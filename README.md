# house 專案說明文件

> 此為好多房平台後台系統，預定網址為 http://backend.houserich.com.tw/

-----------------------
## 版本
v 0.5.0(2016-09-21)
- 專案架構初始化
- 測試基礎開發所需技術：Phalcon\Annotations、Phalcon Dev-Tools建立Models與Mysql schema整合

v 0.6.0(~2016-10-03)
- 完成預設選單配置
- 建立網頁內容表單項目

## 結構注意
- apps/core/controllers/DefaultController.php檔案內可配置
```php
    public function initialize() {
        // 呼叫Phalcon建構子
        parent::initialize();
    }
```

- 配置一個新的模組[content]作法
    1. 複製整個phase目錄改名為新模組放在apps下
    2. 在新模組下的Bootstrap.php內將所有"Phase"取代為"Content"
    3. 修改/public/index.php內於 $application->registerModules 片段附加新模組

```php
$application->registerModules(array(
    'core' => array(
        'className' => 'House\Core\Bootstrap',
        'path' => __DIR__ . '/../apps/core/Bootstrap.php'
    ),
    //附加的新模組
    'content' => array(
        'className' => 'House\Content\Bootstrap',
        'path' => __DIR__ . '/../apps/content/Bootstrap.php'
    ),
    'phase' => array(
        'className' => 'House\Phase\Bootstrap',
        'path' => __DIR__ . '/../apps/phase/Bootstrap.php'
    )
));
```
-----------------------
## 任務進度

### 十月中旬完成至後台登入、會員管理、物件管理(賣房)等項目
- 後台套用kingadmin並根據客戶已規範要求配置選單架構

- 設定共同區塊部分，更新回pkmul

- 調整bower_compoment配置，將jquery / bootstrap部分改為預設載入（手動更新）

- 配置使用者部分model
    - 包含Auth與ACL整合
    - 使用Phalcon/Annotations來搭配ACL作法