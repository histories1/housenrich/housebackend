// 直接自定義dataTable欄位，用來作為附加處理內容。
$.fn.dataTable.ext.feature.push( {
    fnInit: function ( settings ) {
        if( $("#dataTable_Title").length ){
            title = $("#dataTable_Title").text();
        }else{
            title = '資料篩選';
        }
        // return static html
        return '<h3><i class="fa fa-search"></i> '+title+'</h3>';
    },
    cFeature: 'X'
});

$.extend( $.fn.dataTable.defaults, {
    dom : '<"widget"<"widget-header"<"row"<"col-sm-6"X><"col-sm-6 text-right"l>>><"widget-content"t><"widget-footer"<"row"<"col-sm-6"i><"col-sm-6"p>>>>',
    language: {
        "emptyTable"    : "目前沒有任何（匹配的）資料。",
        "sProcessing":   "處理中...",
        "sLengthMenu":   "顯示 _MENU_ 項結果",
        "sZeroRecords":  "沒有匹配結果",
        "sInfo":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
        "sInfoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
        "sInfoFiltered": "(從 _MAX_ 項結果過濾)",
        "sInfoPostFix":  "",
        "sSearch":       "搜索:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "首頁",
            "sPrevious": "上頁",
            "sNext":     "下頁",
            "sLast":     "尾頁"
        }
    },
    pageLength: 20,
    lengthChange: true,
    lengthMenu: [ 20, 50, 100 ],
    bAutoWidth: true,
    scrollCollapse: true,
    pagingType: "full_numbers",
    processing: true,
    serverSide: true,
});

var dTable ;

// 觸發「列單選」「移除」按鈕
$('#dataTable tbody').on( 'click', 'tr', function () {
    var data = dTable.row( $(this) ).data();
    // $(this).css({backgroundColor:'grey'});
    // console.log( data );
});