/*******************************************
 * clickToggle API
 * called by
 * 1. _init_kingadmin_navigation()
 * 2. _init_kingadmin_widget_toggleexpand()
 *
 * 處理點擊觸發與恢復觸發事件
 *******************************************/
(function($){

    $.fn.extend({
        clickToggle: function( f1, f2 ) {
            return this.each( function() {
                var clicked = false;
                $(this).bind('click', function() {
                    if(clicked) {
                        clicked = false;
                        return f2.apply(this, arguments);
                    }
                    clicked = true;
                    return f1.apply(this, arguments);
                });
            });
        },
        pkalert: function(options) {
            var opt = jQuery.extend({
                alerts : ".page-alert",
                display : "#alertInfo",
                msg : "預設訊息。",
                autoclose: true,
                autotimeout : 4000,
            }, options);

            // 直接跳出訊息框
            if( opt.alerts == 'popupAlert' ){
                alert(opt.msg);
                return;
            }

            // 隱藏所有訊息框
            $(opt.alerts).not(this).addClass('hide');
            // 顯示指定訊息框
            $(this).removeClass('hide').text( opt.msg );
            // 判斷自動偵測訊息隱藏
            if( opt.autoclose ){
                setTimeout(function(){
                    $(opt.display).addClass('hide');
                }, opt.autotimeout);
            }
        }
    });

})(jQuery);


/*******************************************
 * KingAdmin預設配置側邊欄選單區塊搭配指定按鈕進行
 * 收合、下拉功能
 *
 * html結構:
 *
 *
 *
 *******************************************/
function _init_kingadmin_navigation() {
    $('.main-menu .js-sub-menu-toggle').off('click').on('click',function(e){
        $li = $(this).parents('li');
        if( !$li.hasClass('active')){
            $li.find('.toggle-icon').removeClass('fa-angle-left').addClass('fa-angle-down');
            $li.addClass('active');
        } else {
            $li.find('.toggle-icon').removeClass('fa-angle-down').addClass('fa-angle-left');
            $li.removeClass('active');
        }
        $li.find('.sub-menu').slideToggle(300);
        e.preventDefault();
    });

    // 防止觸發下面的clickToggle
    $(".main-menu, .sidebar-content").click(function(e){
        e.stopPropagation();
    });
    //設定left-sidebar高度與視窗高度一致
    $('.left-sidebar, .content-wrapper').css('min-height', $(window).innerHeight());
    $('.left-sidebar, .js-toggle-minified').clickToggle(
        function() {
            $('.left-sidebar').addClass('minified');
            $('.content-wrapper').addClass('expanded');

            $('.left-sidebar .sub-menu')
            .css('display', 'none')
            .css('overflow', 'hidden');

            $('.main-menu > li > a > .text').animate({
                    opacity: 0
            }, 200);

            $('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
        },
        function() {
            $('.left-sidebar').removeClass('minified');
            $('.content-wrapper').removeClass('expanded');
            $('.main-menu > li > a > .text').animate({
                opacity: 1
            }, 600);

            $('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
        }
    );
}


/*******************************************
 * widget元件下可收合展開widget視窗功能
 *
 * html結構:
 *
 *
 *
 *******************************************/
function _init_kingadmin_widget_toggleexpand() {
    $('.widget .btn-toggle-expand').clickToggle(
        function(e) {
            e.preventDefault();
            target=$(this).closest('.widget').find('.widget-content');
            if( target.is(':visible') )
                target.slideUp(300);
            else
                target.slideDown(300);
            $(this).find('i.fa-chevron-down').toggleClass('fa-chevron-up').end().find('i.fa-chevron-up').toggleClass('fa-chevron-down');
        },
        function(e) {
            e.preventDefault();
            target=$(this).closest('.widget').find('.widget-content');
            if( target.is(':visible') )
                target.slideUp(300);
            else
                target.slideDown(300);
            $(this).find('i.fa-chevron-up').toggleClass('fa-chevron-down').end().find('i.fa-chevron-down').toggleClass('fa-chevron-up');
        }
    );
}


/*******************************************
 *  CUSTOM TOP MENU
 *******************************************/
function _init_kingadmin_headerblock_custommenu() {
    $(".sub-menu li").draggable({
        cursor: "move",
        cursorAt: { left: 10, top:10 },
        handle: 'i.fa',
        // addClasses: false,
        appendTo: "body",
        containment: "body",
        helper: "clone",
        revert: true,
        start : function(event, ui) {
            $(ui.helper).addClass("menu-item-helper");
        }
    });
    $("#topCustomMenu").droppable({
        accept: ".sub-menu li",
        hoverClass: "drag-active-highlight",
        addClasses: false,
        activate : function(event, ui ){
            $("#topCustomMenu").addClass( "drag-active-highlight" );
        },
        drop : function(event, ui){
            //$( this ).Class( "ui-state-highlight" );
            $item =ui.draggable;
            $item.fadeOut(function() {
                $item.addClass('menu-item')
                     .addClass('pull-left')
                     .appendTo( $("#topCustomMenu") ).fadeIn(function(){});
            });
            $("#topCustomMenu").removeClass( "drag-active-highlight" );

            $.ajax({
                url : '/frontend/index/customTopMenu',
                data : { link: $item.find("a").attr('href'), label: $item.find("span").text() },
                type : 'POST',
                dataType : 'html',
                success : function( resp ){
                    $(resp).insertBefore(".content-breadcrumb");
                }
            });
            // console.log( url+' '+label );
        },
        deactivate : function(event, ui){
            $("#topCustomMenu").removeClass( "drag-active-highlight" );
        }
    });
}

/*******************************************
 * @see https://github.com/davidstutz/bootstrap-multiselect
 * 透過配置html結構與classname方式，直接加載bootstrap-multiselect套件
 * @param bool $with_filter 是否加載下拉選擇內容過濾功能
 *
 * html結構:
 *
 *
 *
 *******************************************/
function _bind_multiselect($with_filter) {
    if( $with_filter ){
        $('.multiselect-filter').multiselect({
            nonSelectedText: '請選擇(可搜尋)',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 200,
            onChange: function(option, checked) {
                if( typeof multiselectfilter_onChange == 'function' ){
                    multiselectfilter_onChange(option, checked);
                }
            }
        });
    }else{
        $('select.multiselect').multiselect({
            nonSelectedText: '請選擇',
            maxHeight: 300,
            onChange: function(option, checked) {
                if( typeof multiselect_onChange == 'function' ){
                    multiselect_onChange(option, checked);
                }
            }
        });
    }
}

/*******************************************
 *  Widget Content Search
/********************************************/
function _bind_widgetsreach() {
    $('.widget-search').next().on({
        click: function(){
            q = $(this).prev().val();
            BLOCK = $( $(this).prev().data('target') );
            if( q == '' ){
                BLOCK.find(".searchable").show();
            }else{
                BLOCK.find(".searchable:visible").hide().filter(":contains('"+q+"')").show();
            }
        }
    });
    if( $('.widget-search-recovery').length ){
        $('.widget-search-recovery').on({
            click: function() {
                $($(this).data('target')).find(".searchable").show();
            }
        });
    }
}
function _bind_widgetfilter() {
    $(".widget-filter").on({
        click: function(){
            target=$(this).parents('.widget-inside').find('.widget-content > div');
            q = $(this).data('filter');
            $(target).find(".searchable").hide().filter(q).show();
        }
    });
}


/*******************************************
 * 頁籤型元件內容區塊由Ajax方式動態加載內容
 *
 * html結構:
 *
 *
 *
 ********************************************/
function _load_tabajax( $nowtab ) {
    var loadurl = $nowtab.attr('href'),
        targ = $nowtab.data('target');
    var callback = $nowtab.data('callback');
    var defaultStatusHtml = '<i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i><span class="">載入中...</span>';

    $.ajax({
        url : loadurl,
        dataType : 'html',
        beforeSend : function(){
            $nowtab.tab('show');
            $(targ).html(defaultStatusHtml);
        },
        success : function( resp ){
            $(targ).html(resp);
        },
        complete :  function(){
            if( typeof window[callback] == "function" ){
                window[callback]();
            }
        }
    });
}


/*******************************************
 * 預設編輯視窗/刪除按鈕觸發動作
/********************************************/
$(document).on("click", ".btn-modal",function(e){
    $url = $(this).data('remote');
    $exclass = $(this).data('mclass');
    var $callback = window[$(this).data('callback')];

    $.ajax({
        url : $url,
        dataType : 'html',
        beforeSend : function(){
            $("#layoutModal").empty();
        },
        success : function( resp ){
            $("#layoutModal").html( resp ).find('.modal-dialog').addClass($exclass);
        },
        complete : function(){
            $("#layoutModal").modal('show');
            if (typeof $callback === "function" ) {
                $callback();
            }
        }
    });
}).on("click", ".btn-delete, .btn-remove",function(e){
    window.location = $(this).data('remote');
// 建檔列表編輯模式切換開關
}).on("click", ".ajax-delete", function(e){
    e.preventDefault();

    if( confirm( $(this).data('dialog') ) ){
        remmoveTarget=$(this).closest( $(this).data('closest') );
        $.ajax({
            url : $(this).data('remote'),
            type:'POST',
            dataType:'json',
            success: function( resp ){
                if( resp.res ){
                    remmoveTarget.remove();
                }
                $(resp).pkalert(resp);
            }
        });
    }
}).on("click", "#enableSwitch",function(e){
    $('.editable').editable('toggleDisabled');
});


/*******************************************
 * 精靈流程元件，流程型表單
 *
/********************************************/
$(document).on('click' ,'.wizard-next', function(){
    $(".wizard-form .help-block").empty().hide();
    $('#flow-wizard').wizard('next');
}).on('click' ,'.wizard-prev', function(){
    $(".wizard-form .help-block").empty().hide();
    $('#flow-wizard').wizard('previous');
});

/*******************************************
 * 參考king-common.js line 114 處理widget focus作法
 * 並附加 line 24 收合側邊選單區塊 js-toggle-minified 作法
 * 處理控制區塊全螢幕
 *******************************************/
$(document).on('click' ,'.btn-fullmode', function(e){
    e.preventDefault();
    $target = $(this).data('target');
    //強制側邊選單收合狀態
    $('.left-sidebar').addClass('minified');
    $('.content-wrapper').addClass('expanded');
    $('.left-sidebar .sub-menu')
    .css('display', 'none')
    .css('overflow', 'hidden');
    $('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
    //強制上方工具列隱藏
    $('.top-bar').addClass('hide');

    $(this).removeClass('btn-fullmode').addClass('btn-normalmode')
           .find('.fa-arrows-alt').addClass('fa-minus-circle text-danger').removeClass('fa-arrows-alt');

    $( $target ).parents('.widget').find('.btn-remove').addClass('link-disabled');
    $( $target ).parents('.widget').addClass('widget-focus-enabled').addClass('widget-fullwindow');
    $('<div id="focus-overlay"></div>').hide().appendTo('body').fadeIn(300);

    //detect dataTable
    if( $("#dataTable").length ){
        $("#dataTable").DataTable().draw();
    }
}).on('click' ,'.btn-normalmode', function(e){
    e.preventDefault();
    $target = $(this).data('target');
    //強制側邊選單收合狀態
    $('.left-sidebar').removeClass('minified');
    $('.content-wrapper').removeClass('expanded');
    $('.left-sidebar .sub-menu')
    .css('display', 'none')
    .css('overflow', 'visible');
    $('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
    //強制上方工具列隱藏
    $('.top-bar').removeClass('hide');

    $(this).addClass('btn-fullmode').removeClass('btn-normalmode')
           .find('.fa-minus-circle').addClass('fa-arrows-alt').removeClass('fa-minus-circle text-danger');

    $theWidget = $( $target ).parents('.widget');
    $($target).find('.fa-arrow-alt').toggleClass('fa-ban');
    $theWidget.find('.btn-remove').removeClass('link-disabled');
    $('body').find('#focus-overlay').fadeOut(function(){
        $(this).remove();
        $theWidget.removeClass('widget-fullwindow').removeClass('widget-focus-enabled');
    });

    //detect dataTable
    if( $("#dataTable").length ){
        $("#dataTable").DataTable().draw();
    }
});

/*******************************************
 * 處理鍵盤操作
 * 1. 偵測Enter事件可以改為TAB作法
 * @see http://stackoverflow.com/questions/2335553/jquery-how-to-catch-enter-key-and-change-event-to-tab
/********************************************/
$(document).on("keydown", "input", function(e) {
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    //add select too
    if(key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        e.preventDefault();
        return false;
    }
}).on("focus", ".numfield", function(e){
    $(this).select();
});


$(function(){
    $('.main-header > em').click(function(){ $('article.alert').show(); });

    /************************
    /*  BOOTSTRAP TOOLTIP
    /************************/
    $('body, [rel=tooltip]').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });
    /*******************************************
     *  BOOTSTRAP POPOVER
     *******************************************/
    $('.helper').popover({
        container: 'body',
        html: true,
        title: '<i class="fa fa-book"></i> Help',
        content: "Help summary goes here. Options can be passed via data attributes <code>data-</code> or JavaScript. Please check <a href='http://getbootstrap.com/javascript/#popovers'>Bootstrap Doc</a>"
    });
    $('.demo-popover1 #popover-title').popover({
        html: true,
        title: '<i class="fa fa-cogs"></i> Popover Title',
        content: 'This popover has title and support HTML content. Quickly implement process-centric networks rather than compelling potentialities. Objectively reinvent competitive technologies after high standards in process improvements. Phosfluorescently cultivate 24/365.'
    });
    $('.demo-popover1 #popover-hover').popover({
        html: true,
        title: '<i class="fa fa-cogs"></i> Popover Title',
        trigger: 'hover',
        content: 'Activate the popover on hover. Objectively enable optimal opportunities without market positioning expertise. Assertively optimize multidisciplinary benefits rather than holistic experiences. Credibly underwhelm real-time paradigms with.'
    });
    $('.demo-popover2 .btn').popover();

    /************************
    /*  BOOTSTRAP ALERT
    /************************/
    $('.alert .close').click( function(e){
        e.preventDefault();
        $(this).parents('.alert').fadeOut(200);
    });

    $('.demo-cancel-click').click(function(){return false;});

    _init_kingadmin_navigation();

    _init_kingadmin_widget_toggleexpand();

    if( $('#flow-wizard').length > 0 && $('.steps li:first').is('.active') ){
        $(".wizard-prev").hide();
    }

    if( $('select.multiselect').length ){
        _bind_multiselect(0);
    }
    if( $('.multiselect-filter').length ){
        _bind_multiselect(1);
    }

    if( $('.widget-search').length ){
        _bind_widgetsreach();
    }
    if( $(".widget-filter").length ){
        _bind_widgetfilter();
    }

    /*******************************************
     *  CUSTOM TOP MENU
     *******************************************/
     // _init_kingadmin_headerblock_custommenu();
});