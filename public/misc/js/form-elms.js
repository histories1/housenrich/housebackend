/*******************************************
 * 處理地址欄位內縣市與行政區切換效果
 * 通用函式
/********************************************/
function _depend_city_withdistrict(citySelect, initial) {
    targetObj = $( citySelect.data('target') );
    city = citySelect.find(":selected").text();

    if( targetObj.length ){
        targetObj.removeClass('hide');
        if( !initial ){
            targetObj.find("option").removeAttr("selected")
        }
        targetObj.find("option").each(function(){
            if( $(this).data('city') != null ){
                if($.trim($(this).data('city'))==$.trim(city)){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            }
        });
    }
}

/*******************************************
 * 處理物件編輯頁面切換用途時效果
 * step1.phtml
/********************************************/
function _depend_userfor(chkval, initial) {
    var datas = ["住宅","店面","辦公","住辦","廠房"];

    if( !initial ){
        $(".usefor-type").find("option").removeAttr("selected");
    }
    if( $.inArray(chkval, datas ) != -1 ){
        $(".usefor-type").attr("disabled",false);
    }else{
        $(".usefor-type").attr("disabled",true);
    }
}

/*******************************************
 * 處理完工日期計算屋齡
/********************************************/
function _date_to_count_houseage() {
    // 日期計算
    var dtp = $(".completion-dtp").datepicker({
        language : 'zh-TW',
        maxViewMode: 4,
        format: 'yyyy-mm-dd'
    })
    // 屋齡計算
    .on('changeDate', function(e) {
        cdate = moment(dtp.datepicker('getDate'));

        n_year = parseInt(moment().format('Y'));
        n_month = parseInt(moment().format('M'));
        b_year = parseInt(cdate.format('Y'));
        b_month = parseInt(cdate.format('M'));

        diff_year=n_year-b_year;
        if( n_month > b_month ){
            diff_month = n_month-b_month;
        }else{
            diff_year -= 1;
            diff_month = 12+n_month-b_month;
        }
        $(".houseage-year").val(diff_year).prop('value',diff_year);
        $(".houseage-month").val(diff_month).prop('value',diff_month);
        // console.log(n_year+' '+n_month);
        // console.log(b_year+' '+b_month);
        // console.log(diff_year+' '+diff_month);
        dtp.datepicker('hide');
    });
}

/*******************************************
 * 處理內文或物件介紹欄位套用基本所見即所得編輯器
/********************************************/
function _wysihtml_description() {
    $('.wysihtml').wysihtml5({
      stylesheets: ["/misc/css/bootstrap3-wysihtml5-color.css"],
      toolbar: {
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": false, //Button which allows you to edit the generated HTML. Default false
        "link": false, //Button to insert a link. Default true
        "image": false, //Button to insert an image. Default true,
        "color": true, //Button to change color of font
        "blockquote": true, //Blockquote
        "size": 'sm' //default: none, other options are xs, sm, lg
      }
    });
}

/*******************************************
 * 處理物件各類面積輸入計算與加總小計計算
/********************************************/
function _ajax_load_htmlblock( loadFormUrl ) {
    // 區塊列表單一紀錄複製
    $(document).on("click", ".btn-ajaxcopy", function(){
        formClass = $(this).data('form');
        appendTo = $(this).data('target');
        selfbtn = $(this);

        $.ajax({
            url : loadFormUrl,
            type: 'POST',
            // itemcount:計算群組元素數量
            data: {form: formClass, itemcount: $(appendTo).find(".area").length },
            dataType: 'json',
            success: function( resp ){
                if( resp.res ){
                    $(appendTo).append( resp.html );
                }else{
                    $(resp.display).pkalert(resp);
                }
            }
        });
    });
}

$(function(){
    moment.locale('zh-tw');

    // 地址表單縣市欄位
    $(document).on("change", ".depend-city", function(){
        _depend_city_withdistrict( $(this) );
    });

    // 物件編輯表單用途/型態欄位
    $(document).on("click", ".depend-radio", function(){
        chkval = $(this).val();
        _depend_userfor( chkval );
    });

    //*******************************************
    /*  DATE PICKER
    /********************************************/
    if( $('.datepicker').length ){
        var dtp = $('.datepicker').datepicker({
            language : 'zh-TW',
            startView: 'month',
            format: 'yyyy-m-d'
            // startDate : moment(new Date()).format('YYYY-MM-DD')
        })
        .on('changeDate', function(e) {
            dtp.datepicker('hide');
        });
        // $('.datepicker').datepicker({
        //     language : 'zh-TW',
        //     startDate : moment(new Date()).format('YYYY-MM-DD')
        // });
    }

    if( $('.daterange').length ){
        $('.daterange').daterangepicker({
            timePicker: true,
            timePickerIncrement: 10,
            format: 'MM/DD/YYYY h:mm A'
        });
    }


    //*******************************************
    /*  Form Ajax Submit
    /********************************************/
    if( $("#ctrlFromBtn").length ){
    $("#ctrlFromBtn").off('click').on('click', function(e){
        if( $(".tab-pane.active").length ){
            var targetForm=$(".tab-pane.active").find("form");
            var validator=targetForm.validate();
        }else{
            var targetForm=$("form");
            var validator=targetForm.validate();
        }

        if( !validator.form() ){
            return ;
        }

        $.ajax({
            url: targetForm.attr('action'),
            type: "post",
            data: targetForm.serialize(),
            dataType: "json",
            beforeSend: function(){
                $("#ctrlFromBtn").hide();
            },
            success: function( resp ){
                $(resp.display).pkalert(resp);

                if( resp.PrimaryId ){
                    $(resp.PrimaryId).val(resp.id).prop('value', resp.id);

                    // 針對物件建檔指定精靈式表單步驟操作
                    if( resp.PrimaryId == "#richitemId" ){
                        $(".wizard-steps").find("li:not(.active)").show();
                        $(".btn-prev, .btn-next").show();
                    }
                }
            },
            complete: function(){
                $("#ctrlFromBtn").show();
            }
        })
    });
    }
});
