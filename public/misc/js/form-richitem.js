/**
 * 集中針對物件建檔表單欄位各項個制化操作項目函式
 * @author San
 * */

/*******************************************
 * 處理物件各類面積輸入計算與加總小計計算
/********************************************/
function _richitem_areas_counter() {
    // 前端單一列表記錄直接移除
    $(document).on('click' ,".btn-remove-self", function(e){
        e.preventDefault();
        $(this).parents('aside').remove();
    // 輸入面積平方公尺計算坪
    }).on('keyup', ".areaM", function(e){
        e.preventDefault();

        var arm=$(this).closest('section.inline-group').find(".areaM");
        var a=$(this).closest('section.inline-group').find(".areaMolecular");
        var b=$(this).closest('section.inline-group').find(".areaDenominator");

        var value = parseFloat($(this).val())*0.3025;

        // console.log(arm);
        // console.log(a);
        // console.log(b);

        if( a.length && b.length ){
            var param=parseFloat(a.val()/b.val());
            if( isNaN(param) ){ return; }
            value = value*param;
        }

        $(this).closest('section.inline-group').find(".area").val(value).prop('value', value);
    }).on('keyup', ".areaMolecular", function(e){
        e.preventDefault();

        var arm=$(this).closest('section.inline-group').find(".areaM");
        var a=$(this);
        var b=$(this).closest('section.inline-group').find(".areaDenominator");

        var param=parseFloat(a.val()/b.val());
        if( isNaN(param) ){ return; }
        var value = (parseFloat(arm.val())*0.3025)*param;

        $(this).closest('section.inline-group').find(".area").val(value).prop('value', value);
    }).on('keyup', ".areaDenominator", function(e){
        e.preventDefault();

        var arm=$(this).closest('section.inline-group').find(".areaM");
        var a=$(this).closest('section.inline-group').find(".areaMolecular");
        var b=$(this);

        var param=parseFloat(a.val()/b.val());
        if( isNaN(param) ){ return; }
        var value = (parseFloat(arm.val())*0.3025)*param;

        $(this).closest('section.inline-group').find(".area").val(value).prop('value', value);
    // 區塊顯示總面積計算
    }).on('click', '.area-count', function(e){
        e.preventDefault();
        // 面積取得區塊
        areas=$($(this).data('areablock'));
        // 總面積元素
        element=$($(this).data('target'))

        // 計算加總
        A = new Array();
        areas.find(".area").map(function(inx, elm){
            if( !isNaN( parseFloat($(elm).val()) ) ){
                A.push( parseFloat($(elm).val()) );
            }
        });
        sum = A.reduce(function(a, b) { return a + b; }, 0);
        if( isNaN(sum) ){
            alert('請檢查是否任一面積欄位有數值否則無法加總計算！');
            sum = 0;
        }

        element.val(sum).prop('value', sum);
    }).on('click', '.btn-ajaxdelete-area', function(e){
        e.preventDefault();
        var URL = $(this).data('remote');
        var list = $(this).closest('section.inline-group');
        $.ajax({
            url: URL,
            type: 'GET',
            dataType: 'json',
            success: function( resp ){
                if( resp.res ){
                    list.remove();
                }else{
                    $(resp.display).pkalert(resp);
                }
            }
        });
    });
}


/*******************************************
 * 處理物件車位產權切換欄位
/********************************************/
function _richitem_parking_property( propertySel ) {
    hidesel = propertySel.find(":selected").data('hide');
    if( hidesel === 'undefined' ){ return; }
    showsel = propertySel.find(":selected").data('show');

    propertySel.parent().find( hidesel ).addClass('hide');
    propertySel.parent().find( showsel ).removeClass('hide');
}

/*******************************************
 * 處理物件房屋、加蓋、車位現況欄位處理
/********************************************/
function _richitem_information_situation( propertySel ) {
    if( propertySel.find(":selected").data('show') ){
        $( propertySel.find(":selected").data('show') ).removeClass('hide');
    }
    if( propertySel.find(":selected").data('hide') ){
        $( propertySel.find(":selected").data('hide') ).addClass('hide');
    }
}

/*******************************************
 * 處理取得加總物件各類總面積數值
 * step2.phtml
/********************************************/
function _richitem_areaall_counter() {

    // 所有總面積計算處理
    $(".count-areaall").click(function(){
        var L, M, A, P, sum, right_sum=0, using_sum=0, sharepart_sum=0;
        // land
        L = new Array();
        $("#AreaLand").find(".area").each(function(inx, elm){
            if( !isNaN( parseFloat($(elm).val()) ) ){
                L.push( parseFloat($(elm).val()) );
            }
        });
        sum = L.reduce(function(a, b) { return a + b; }, 0);
        if( isNaN(sum) ){
            alert('請檢查是否任一面積欄位有數值否則無法加總計算！');
            sum = 0;
        }
        $("#areaLand").val(sum).prop('value', sum);

        // mainbuilding
        M = new Array();
        $("#AreaMainbuilding").find(".area").each(function(inx, elm){
            if( !isNaN( parseFloat($(elm).val()) ) ){
                M.push( parseFloat($(elm).val()) );
            }
        });
        sum = M.reduce(function(a, b) { return a + b; }, 0);
        if( isNaN(sum) ){
            alert('請檢查是否任一面積欄位有數值否則無法加總計算！');
            sum = 0;
        }
        $("#areaMainbuilding").val(sum).prop('value', sum);
        right_sum = sum;
        using_sum = sum;

        // appendbuilding
        A = new Array();
        $("#AreaAppendbuilding").find(".area").each(function(inx, elm){
            if( !isNaN( parseFloat($(elm).val()) ) ){
                A.push( parseFloat($(elm).val()) );
            }
        });
        sum = A.reduce(function(a, b) { return a + b; }, 0);
        if( isNaN(sum) ){
            alert('請檢查是否任一面積欄位有數值否則無法加總計算！');
            sum = 0;
        }
        $("#areaAppendbuilding").val(sum).prop('value', sum);
        right_sum += sum;
        using_sum += sum;

        // Sharepart
        S = new Array();
        $("#AreaSharepart").find(".area").each(function(inx, elm){
            if( !isNaN( parseFloat($(elm).val()) ) ){
                S.push( parseFloat($(elm).val()) );
            }
        });
        sum = S.reduce(function(a, b) { return a + b; }, 0);
        if( isNaN(sum) ){
            alert('請檢查是否任一面積欄位有數值否則無法加總計算！');
            sum = 0;
        }
        $("#areaSharepart").val(sum).prop('value', sum);
        sharepart_sum = sum;
        right_sum += sum;

        // parking
        P = new Array();
        $("#AreaParking").find(".area").each(function(inx, elm){
            if( !isNaN( parseFloat($(elm).val()) ) ){
                P.push( parseFloat($(elm).val()) );
            }
        });
        sum = P.reduce(function(a, b) { return a + b; }, 0);
        if( isNaN(sum) ){
            alert('請檢查是否任一面積欄位有數值否則無法加總計算！');
            sum = 0;
        }
        $("#areaParking").val(sum).prop('value', sum);
        right_sum += sum;

        // 權狀
        $("#areaRight").val(right_sum).prop('value', right_sum);
        // 使用
        $("#areaUsing").val(using_sum).prop('value', using_sum);
        // 公設比
        ratioPublicArea = new Number( (sharepart_sum/right_sum)*100);
        $("#ratioPublicArea").val(ratioPublicArea.toFixed(2)).prop('value', ratioPublicArea.toFixed(2));
    });
}

/*******************************************
 * 處理計算物件單價
 * step2.phtml
/********************************************/
function _richitem_singleprice_counter() {
    // 物件單價計算
    $(".count-price").click(function(){
        var richitem_val, parking_val, right_area, parking_area, priceSingle = 0;
        if( !$("#priceRichitem").val() ){
            alert('請先填入物件總價');
            return ;
        }else{
            richitem_val = parseFloat($("#priceRichitem").val());
        }
        if( !$("#priceParking").val() ){
            parking_val = 0;
        }else{
            parking_val = parseFloat($("#priceParking").val());
        }

        if( !$("#areaRight").val() ){
            alert('請先點選上方取得計算總面積再操作');
            return ;
        }else{
            right_area = parseFloat($("#areaRight").val());
        }

        if( !$("#areaParking").val() ){
            parking_area = 0;
        }else{
            parking_area = parseFloat($("#areaParking").val());
        }

        priceSingle = new Number( (richitem_val-parking_val)/(right_area-parking_area) );
        $("#priceSingle").val(priceSingle.toFixed(2)).prop('value', priceSingle.toFixed(2));
    });
}

/*******************************************
 * 處理計算物件報酬率
 * step2.phtml
/********************************************/
function _richitem_invest_counter() {
    $(".count-invest").click(function(){
        var richitem_val, rent_house, rant_parking, rant_addon, invest=0, investall=0;

        if( !$("#priceRichitem").val() ){
            alert('請先填入物件總價');
            return ;
        }else{
            richitem_val = parseFloat($("#priceRichitem").val());
        }
        if( !$("#houseRent").val() ){
            alert('請確認房屋現況為出租，且已填入目前租金金額');
            return ;
        }else{
            rent_house = parseFloat($("#houseRent").val());
        }

        if( !$("#parkingRent").val() ){
            rent_parking = 0;
        }else{
            rent_parking = parseFloat($("#parkingRent").val());
        }

        if( !$("#addonRent").val() ){
            rent_addon = 0;
        }else{
            rent_addon = parseFloat($("#addonRent").val());
        }

        invest=new Number( ((rent_house+rent_parking)*12/(richitem_val*10000))*100 );
        $("#houseReturnInvest").val(invest.toFixed(2)).prop('value', invest.toFixed(2));
        investall=new Number( ((rent_house+rent_parking+rent_addon)*12/(richitem_val*10000))*100 );
        $("#withaddonReturnInvest").val(investall.toFixed(2)).prop('value', investall.toFixed(2));
    });
}


/*******************************************
 * 處理計算物件報酬率
/********************************************/
function _richitem_tax_counter() {
    $(".count-housetaxcount").click(function(){
        var val_house;

        if( !$("#houseValue").val() ){
            alert('請先填入房屋現值');
            return ;
        }else{
            val_house = parseFloat($("#houseValue").val());
        }
        // 房屋稅（現值*1.2%）
        taxHouse = new Number(val_house*0.012);
        $("#taxHouse").val(taxHouse.toFixed(0)).prop('value', taxHouse.toFixed(0));
        // 契稅
        taxContract = new Number(val_house*0.06);
        $("#taxContract").val(taxHouse.toFixed(0)).prop('value', taxHouse.toFixed(0));
    });

    $(".count-landtaxcount").click(function(){
        var val_land;

        if( !$("#landValue").val() ){
            alert('請先填入物件總價');
            return ;
        }else{
            val_land = parseFloat($("#landValue").val());
        }

        if( !$(".landAmount").val() ){
            alert('請先設定土地總面積');
            return ;
        }else{
            area = parseFloat($(".landAmount").val())*0.3025;
        }

        // 地價稅（現值*0.2%）
        taxLand = new Number(val_land*area*0.002);
        $("#taxLand").val(taxLand.toFixed(0)).prop('value', taxLand.toFixed(2));
    });
}