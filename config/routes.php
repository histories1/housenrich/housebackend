<?php
$router = new \Phalcon\Mvc\Router(FALSE);
$router->removeExtraSlashes(true);

/**
 * Global routes
 */
$router->add('/:module/:controller/:action(/:params)?', array(
    'module' => 1,
    'controller' => 2,
    'action' => 3,
    'param' => 4
));
/**
 * define default route
 * */
$router->add("/(index|dashboard)?", array(
    'namespace' => ucfirst(PPS_APP_PROJECTNAME) . '\Phase\Controllers\\',
    'module'    => 'phase',
    'controller'=> 'default',
    'action'    => 'index'
));

/* set for accessFile */
$file = new \Phalcon\Mvc\Router\Group(
    [
        "module"     => "core",
        "controller" => "file",
    ]
);
$file->setPrefix("/file");
$file->add(
    "/upload",
    ["action" => "upload"]
);
$file->add(
    "/access/{model}/{uuid}",
    ["action" => "access",]
);
$file->add(
    "/delete/{model}/{uuid}/{mode}",
    ["action" => "delete",]
);
$router->mount($file);

$router->handle();
//Get the matched route
// $route = $router->getMatchedRoute();
// echo $router->getModuleName()."<BR/>";
// echo $router->getControllerName()."<BR/>";
// echo $router->getActionName()."<BR/>";
// var_dump($router->getParams());
// var_dump($route);
// die();
return $router;


//These routes simulate real URIs
$testRoutes = array(
    // '/',
    // '/index',
    // '/default',
    // '/index/test/a/b/c/d',
);
//Testing each route
foreach ($testRoutes as $testRoute) {
    //Handle the route
    $router->handle($testRoute);
    echo 'Testing ', $testRoute, '<br>';
    //Check if some route was matched
    if ($router->wasMatched()) {
        echo 'Module: ', $router->getModuleName(), '<br>';
        echo 'Controller: ', $router->getControllerName(), '<br>';
        echo 'Action: ', $router->getActionName(), '<br>';
        echo 'Params: ', implode(", ", $router->getParams()), '<br>';
    } else {
        echo 'The route wasn\'t matched by any route<br>';
    }
    echo '<br>';
}
