<?php

$C = array(
    'personalwork' => array(
        // 配置專案名稱
        'projectname' => 'PPS預設模組系統',
        // 配置專案載入樣板檔使用前綴版型名稱
        'themename' => 'kingadmin',
        // 配置Personalwork預設頁面樣板檔路徑，起始路徑要以apps/views/為主配置相對路徑格式。
        'viewphtml' => '../../../vendor/sanhuang/phalcon-personalwork/libs/Mvc/View/Phtmls/'
    ),
    'database' => array(
        // 'host'        => 'localhost',
        // 'dbname'      => 'house_database',
        'host'        => '127.0.0.1',
        'dbname'      => 'house_database',
        'username'    => 'sanslave',
        'password'    => 'omge0954',
        'adapter'     => 'Mysql',
        'charset'     => 'utf8',
        'options'     => array(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        )
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../apps/frontend/controllers/',
        'modelsDir'      => __DIR__ . '/../apps/models/',
        'pluginDir'      => __DIR__ . '/../apps/plugins/',
        'formDir'        => __DIR__ . '/../apps/forms/',
        'viewsDir'       => __DIR__ . '/../apps/views/',
        'layoutsDir'     => __DIR__ . '/../apps/views/application-layouts/',
        'partialsDir'    => __DIR__ . '/../apps/views/application-partials/',
        'layoutName'     => 'kingadmin',
        'baseUri'        => '/'
    ),
    'misc' => array(
        'headerStyle' => array(
            'misc/bower_component/bootstrap/dist/css/bootstrap.min.css',
            'misc/css/font-awesome.min.css',
            'misc/css/main.css',
            'misc/css/layouts-application.css'
        ),
        'headerScript' => array(
            'misc/bower_component/jquery/dist/jquery.min.js' => [
                'type'      => 'text/javascript',
            ],
            'misc/bower_component/bootstrap/dist/js/bootstrap.min.js' => [
                'type'      => 'text/javascript',
            ]
        ),
        'footerScript' => array(
            'misc/js/application.js'
        ),
    ),
    'google' => array(
        'account'=> 'san.personalwork@gmail.com',
        'apikey_js' => 'AIzaSyBeZh3sDWVRcwpYwiS-q8uJKKhwrLGwjtw',
        'maps_geocoding' => 'https://maps.googleapis.com/maps/api/geocode/'
    ),
    'mail' => array(
        'fromName'  => 'PPS',
        'fromEmail' => 'san@personalwork.tw',
        'smtp'      => array(
                           'server'   => "smtp.gmail.com",
                           'port'     => 465,
                           'security' => 'ssl',
                           'username' => 'san.personalwork@gmail.com',
                           'password' => 'ek4wu3j;3ru4'
                       )
    )
);


if( file_exists(__DIR__.'/application.json') ){
    $sysconf = new \Phalcon\Config\Adapter\Json( __DIR__.'/application.json' );
    $C['sysconfig'] = $sysconf->toArray();
}

return new \Phalcon\Config($C);
