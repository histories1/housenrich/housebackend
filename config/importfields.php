<?php

$F =
array(
   'community' => [
        [
            'match' => 'SUPPLIERID',
            'label' => '原編號',
            'width' => '80px',
            'field' => 'oldsysid',
            'desc' => '從舊系統取得原始配置編號',
            'validate' => '',
        ],
    ],
   'school' => [
        [
            'match' => 'SUPPLIERID',
            'label' => '原編號',
            'width' => '80px',
            'field' => 'oldsysid',
            'desc' => '從舊系統取得原始配置編號',
            'validate' => '',
        ],
    ]
);
