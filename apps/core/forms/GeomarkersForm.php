<?php

namespace House\Core\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class GeomarkersForm extends \Personalwork\Forms\Form
{
	var $html;

	/**
	 * @Comment("地標名稱")
	 */
	private function _Marklabel() {
		$element = new \Personalwork\Forms\Elements\Text("marklabel");
		$element->setLabel("地標名稱")
				->setAttributes(array(
					"class" => "form-control",
					"style"	=> "width:10em;",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
				));
		$element->addValidator(new StringLength([
			"max" => 50
		]));
		return $element;
	}

	/**
	 * @Comment("詳細地址")
	 */
	private function _Markddress() {
		$element = new \Personalwork\Forms\Elements\Text("markaddress");
		$element->setLabel("詳細地址")
				->setAttributes(array(
					"class" => "form-control input-lgw",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
				));
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("經度")
	 */
	private function _Lng() {
		$element = new \Personalwork\Forms\Elements\Numeric("longitude");
		$element->setLabel("經度")
				->setAttributes(array(
					"class" => "form-control",
					"style"	=> "width:8em;",
					"readonly" => "readonly",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
				));
		return $element;
	}

	/**
	 * @Comment("緯度")
	 */
	private function _Lat() {
		$element = new \Personalwork\Forms\Elements\Numeric("latitude");
		$element->setLabel("緯度")
				->setAttributes(array(
					"class" => "form-control",
					"style"	=> "width:8em;",
					"readonly" => "readonly",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
				));
		return $element;
	}


	/**
	 * 產出地圖表單欄位結構
	 * 1. 判斷若有名稱跟地址數值，查詢已設定座標點
	 * */
	public function renderF($opts=array())
	{
		$nondefault = false; $markId = null;
		if( !empty($opts['name']) ){
			$marker = \Houserich\Models\Geomarkers::findFirstByMarklabel($opts['name']);
			if( !$marker ){
				$nondefault = true;
			}else{
				$markId = $marker->geomarkersId;
			}
		}else{
			$nondefault = true;
		}

		$this->html = '';

		$this->html .= '<aside id="GeomarkersForm" class="form-inline" style="position: relative; margin-top:10px;">'.PHP_EOL;
		$this->html .= "\t\t".$this->get('geomarkersId')->setDefault($markId)->render().PHP_EOL;
		$this->html .= "\t\t".$this->get('type')->setDefault($opts['type'])->render().PHP_EOL;
		$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

		$marklabel = $this->_Marklabel();
		$markaddress = $this->_Markddress();
		$lat = $this->_Lat();
		$lng = $this->_Lng();
		if( !$nondefault ){
			$marklabel->setDefault($marker->marklabel);
			$markaddress->setDefault($marker->markaddress);
			$lat->setDefault($marker->latitude);
			$lng->setDefault($marker->longitude);
		}elseif( !empty($opts['address']) ){
			$marklabel->setDefault($opts['name']);
			$markaddress->setDefault($opts['address']);
		}

		if( isset($opts['labelhide']) ){
			$marklabel->setLabel('')->setAttribute('class', 'hide');
		}
		if( isset($opts['addressreadonly']) && $opts['addressreadonly'] == true){
			$markaddress->setAttribute('readonly', 'readonly');
		}

		$this->html .= "\t\t".$marklabel->render().PHP_EOL;
		$this->html .= "\t\t".$markaddress->render().PHP_EOL;
		$this->html .= "\t".'<button id="mapqueryBtn" type="button" class="btn btn-sm btn-warning btn-map-update">更新座標</button>'.PHP_EOL;
		$this->html .= "\t\t".$lng->render().PHP_EOL;
		$this->html .= "\t\t".$lat->render().PHP_EOL;
		$this->html .= "\t".'<button id="mapsaveBtn" type="button" class="btn btn-sm btn-primary btn-map-save">儲存</button>'.PHP_EOL;

		$this->html .= "\t".'</section>'.PHP_EOL;
        $this->html .= "\t".'<hr class="inner-separator">';
        $this->html .= '</aside>'.PHP_EOL;

		return $this->html;
	}


	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("geomarkersId");
		$this->add($id);

		$type = new \Phalcon\Forms\Element\Hidden("type");
		$this->add($type);

		$this->add($this->_Marklabel());
		$this->add($this->_Markddress());
		$this->add($this->_Lng());
		$this->add($this->_Lat());
	}
}
