<?php
/**
 * FileController
 * 整合Bootstrap-Fileinput套件，處理相關後端機制
 * */

namespace House\Core\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class FileController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * 透過header方式直接產生檔案顯示
     * */
    public function accessAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $uuid = $this->dispatcher->getParam('uuid');
        $modelclass = "\\Houserich\\Models\\".$this->dispatcher->getParam('model');
        $cmedia = $modelclass::findFirstByUUID( $uuid );
        if( $cmedia ){
            $ex = explode('-',$uuid);
            // 從專案目錄起算路徑
            $prefix = realpath(PPS_APP_APPSPATH."/../").DIRECTORY_SEPARATOR;
            $randDir = $ex[0];
            $targetDir = "uploads".DIRECTORY_SEPARATOR.$randDir.DIRECTORY_SEPARATOR;

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-type: '.$cmedia->mime);
            header('Content-length: '.$cmedia->size);
            echo file_get_contents($prefix.$targetDir.$cmedia->filepath);
        }
        return fasle;
    }


    /**
     * 刪除檔案
     * 1. 設定狀態碼方式，檔案仍存在
     * 2. 直接刪除掉檔案
     * */
    public function deleteAction()
    {
        $modelclass = $this->dispatcher->getParam('model');
        $uuid = $this->dispatcher->getParam('uuid');
        $mode = ($this->dispatcher->getParam('mode'))?$this->dispatcher->getParam('mode'):1;

        $model = "\\Houserich\\Models\\{$modelclass}";
        $cmedia = $model::findFirstByUUID($uuid);
        if( !$cmedia ){
            echo json_encode(["error" => "該檔案疑似已經移除找不到對應紀錄。"]);
            return false;
        }

        if( $mode == 1 ){
            $cmedia->statecode = 0;
            if( !$cmedia->save() ){
                echo json_encode(["error" => implode(",", $cmedia->getMessages()) ]);
            }else{
                echo json_encode(["res" => "ok"]);
            }
        }elseif( $mode == 2 ){
            // 產生識別碼利用時間戳記取後65535轉16進位+
            $prefix = realpath(PPS_APP_APPSPATH."/../").DIRECTORY_SEPARATOR;
            // 從專案目錄起算路徑
            $randDir = strtoupper(dechex(rand(1,256)));
            $targetDir = "uploads".DIRECTORY_SEPARATOR.$randDir.DIRECTORY_SEPARATOR;

            $ex=explode('-', $uuid);
            if( is_file($prefix.$targetDir.$ex[0].DIRECTORY_SEPARATOR.$cmedia->filepath) ){
                unlink($prefix.$targetDir.$ex[0].DIRECTORY_SEPARATOR.$cmedia->filepath);
                $cmedia->delete();
            }

            echo json_encode(["res" => "ok"]);
        }

        return false;
    }


    /**
     * 專門處理bootstrap-fileinput套件後端上傳功能
     * @param string modelclass
     * */
    public function uploadAction()
    {
        // 產生識別碼利用時間戳記取後65535轉16進位+
        $prefix = realpath(PPS_APP_APPSPATH."/../").DIRECTORY_SEPARATOR;
        // 從專案目錄起算路徑
        $randDir = strtoupper(dechex(rand(1,256)));
        $targetDir = "uploads".DIRECTORY_SEPARATOR.$randDir.DIRECTORY_SEPARATOR;
        // 判斷目錄是否存在建立目錄
        if( !is_dir($prefix.$targetDir) ){
            mkdir($prefix.$targetDir, 0744, true );
        }

        // $modelClass = "\\Houserich\\Models\\{$}"
        foreach ($_FILES as $name => $file) {
            if( $file['error'] == 1 ){
                // logger
                echo json_encode(["error" => "上傳檔案發生錯誤，檔案大小超過系統設定需調整！"]);
                return false;
            }elseif( $file['error'] != 0 ){
                echo json_encode(["error" => "上傳檔案發生不明錯誤，錯誤代碼：".$file['error']]);
                return false;
            }
            $modelClass = "\\Houserich\\Models\\{$name}";
            $media = new $modelClass();
            $media->{$_POST['KeyName']} = intval($_POST['KeyID']);

            // 針對物件處理部分
            if( isset($_POST['type']) ){
                $media->type = $_POST['type'];
            }
            // var_dump($media->toArray());
            // die();

            $media->mime = $file['type'];
            $media->size = $file['size'];

            $fileseg[0] = dechex(date('isH'));
            $fileseg[1] = '-';
            $fileseg[2] = uniqid();

            $originname = str_replace(' ','', $file['name']);
            $media->originname = $originname;
            $ext = explode('.',strtolower($originname));
            $fileseg[3] = '.';
            $fileseg[4] = $ext[count($ext)-1];

            $media->filepath = implode('',$fileseg);

            $tmp[] = $randDir;
            $tmp[] = $fileseg[2];
            $tmp[] = $fileseg[0];
            $tmp[] = strtoupper($fileseg[4]);
            $media->UUID = implode('-',$tmp);

            $findFun = "findBy{$_POST['KeyName']}";
            $row=$modelClass::$findFun($_POST['KeyID']);
            $media->weight = count($row)+1;

            if( !move_uploaded_file($_FILES[$name]["tmp_name"], $prefix.$targetDir.$media->filepath) ){
                echo json_encode(["error" => "上傳檔案發生錯誤，無法複製暫存檔到指定位置。"]);
            }elseif( !$media->save() ){
                echo json_encode(["error" => "紀錄檔案發生錯誤，訊息：".implode(', ',$media->getMessages())]);
            }else{
                $accesspath = $this->url->get('file/access/'.$name.'/'.$media->UUID);
                if( preg_match("/pdf/", $file['type']) ){
                    // $html = "<embed src='{$accesspath}' class='file-preview-image' width='160px' height='160px' type='application/pdf' />";
                    $ftype = 'pdf';
                }else{
                    // $html = "<img src='{$accesspath}' class='file-preview-image' alt='{$media->originname}' title='{$media->originname}' />";
                    $ftype = 'image';
                }

                echo json_encode([
                    'initialPreview' => $accesspath,
                    'initialPreviewConfig' => [
                        'caption' => $media->originname,
                        'type'=> $ftype,
                        'url' => $this->url->get("file/delete/{$name}/{$media->UUID}/2"),
                        'key' => $media->weight,
                        'extra' => ['ID' => $media->mediaId],
                    ],
                    'initialPreviewThumbTags' => [
                    ]
                ]);
            }
        }

        // 檔案路徑相對於project_path設定

        // var_dump($_POST);

        return false;
    }

    /**
     * 處理相關物件群組重新排序處理
     *
     * */
    public function sortingAction()
    {

    }

}
