<?php
/**
 * 匯入建檔機制
 * 1. 匯入資料群組項目
 * 2. 匯入基礎物件欄位
 * */

namespace House\Core\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 *
 *
 * */
class ImportController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * 處理批次匯入機制
     * */
    public function phaseAction()
    {
        // 學區欄位轉換(匯入國小、國中)
        /**
         * 1. 地址轉乘經緯度
         * 2. 原本地址去除前置郵遞區號與多餘字串
         * */
        $districts = \Houserich\Models\Districts::find();
        // $school = \Houserich\Models\School::find();
        foreach ($districts as $item) {
            $prow=\Houserich\Models\Fieldoptions::findFirst(["label=:a: AND fieldname='縣市'",'bind'=>['a'=>$item->city]]);
            if( !$prow ){
                $prow = new \Houserich\Models\Fieldoptions();
                $prow->fieldname = '縣市';
                $prow->label = $item->city;
                $prow->value = $item->city;
                $prow->save();
            }

            $row=\Houserich\Models\Fieldoptions::findFirst(["label=:a: AND parentLabel=:b:",'bind'=>['a'=>$item->district, 'b'=>$item->city]]);
            if( !$row ){
                $row = new \Houserich\Models\Fieldoptions();
                $row->fieldname = '行政區';
                $row->label = $item->district;
                $row->value = $item->district;
                $row->parentLabel = $item->city;
                $row->parentId = $prow->id;
                // var_dump($row->toArray());
                $row->save();
            }

            // $lbs=explode(',', preg_replace(["/\s+/"],[""],$item->address));
            // if( count($lbs) == 2 ){
            //     $item->latitude = $lbs[0];
            //     $item->longitude = $lbs[1];
            //     $item->address = null;
            //     $item->save();
            // }

            // if( !empty($item->address) ){
            //     $item->address = preg_replace(["/^[\d\s\]]+/"],[''],$item->address);
            //     $item->save();
            // }
        }
        // die('done!');
        /**
         * 社區轉換功能
         * 1.
         * */
        // $community = \Houserich\Models\Community::query();
        // $community->columns(["finishDate","tempDate"])->andWhere("tempDate <> ''");
        // $res = $community->execute();
        // foreach ($res as $item) {
        //     $ndate=preg_replace(["/年/","/月/"], ['-',''],$item->tempDate)."-1 00:00:00";
        //     $time=date('Y-m', strtotime($ndate) );
        // }
        // var_dump($res->toArray());

        // foreach ($this->db->listTables() as $tableName) {
        //     $str = "phalcon model $tableName --namespace=Houserich\\Models ; ";
        //     echo $str."<BR/>";
        //     // var_dump($tableName);
        // }
        // die();
    }


    /**
     *
     * */
    public function getImportAction()
    {

    }
}
