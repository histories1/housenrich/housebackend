<?php
/**
 * GisController
 * Google Map後端機制
 * */

namespace House\Core\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class GisController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * 顯示地圖設定經緯度表單
     * */
    public function mapAction()
    {
    }


    /**
     * 處理計算傳入物件對應最近捷運站清單
     * */
    public function distanceAction()
    {
        // act1. 傳入座標點編號\latLng資訊直接計算
        $LAT = $this->request->getPost('latitude');
        $LNG = $this->request->getPost('longitude');

        if( !$LAT || !$LNG ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未取得經緯度數值無法進行計算！"]);
            return false;
        }

        $phql = "SELECT geo.*, M.mrtId, M.statecode FROM ( SELECT geomarkersId,
                    ( 6371 * acos( cos( radians({$LAT}) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$LNG}) ) + sin( radians({$LAT}) ) * sin( radians( latitude ) ) ) ) AS distance,
                        type,
                        marklabel
                 FROM geomarkers G
                 HAVING distance < 1.5 AND type='mrt'
                 ORDER BY distance LIMIT 0 , 4 ) AS geo
                 LEFT JOIN mrt AS M ON geo.marklabel=M.name
                 GROUP BY marklabel;";
        $m = new \Houserich\Models\Geomarkers();
        $marks = new \Phalcon\Mvc\Model\Resultset\Simple(null, $m , $m->getReadConnection()->query($phql));

        // act3. 根據已計算結果設定關聯紀錄表後回傳列表結構
        $modelclass = "\\Houserich\\Models\\".$this->request->getPost('RelModel');
        $field1 = "{$this->request->getPost('FieldPrefix')}Id";
        $field2 = "{$this->request->getPost('FieldPrefix')}GeomarkerId";
        $html = '';
        foreach( $marks->toArray() as $i => $mark){
            if( !($model=$modelclass::findFirst([
                    "{$field1}=:A: AND MrtId=:B:",
                    'bind'=>[
                            'A'=>$this->request->getPost('KeyID', 'int'),
                            'B'=>$mark['mrtId']]
                            ])) ){
                $model = new $modelclass();
            }
            // communityId or richitemId
            $model->$field1 = $this->request->getPost('KeyID', 'int');
            $model->MrtId = $mark['mrtId'];
            $model->distance = floatVal($mark['distance']*1000);
            $statelabel = array("未通車", "已通車");
            $model->mrtStatus = $statelabel[$mark['statecode']];
            $model->$field2 = $this->request->getPost('MarkId', 'int');
            $model->MrtGeomarkerId = $mark['geomarkersId'];

            if( !$model->save() ) {
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"儲存計算捷運距離發生錯誤，訊息：".implode(',',$model->getMessages())]);
                return false;
            }

            $html .= '<tr>
                        <td>'.($i+1).'</td>
                        <td>'.$mark['marklabel'].'</td>
                        <td>'.number_format(($mark['distance']*1000),1).'(m)</td>
                    </tr>';
        }

        echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"已更新最近的捷運站清單。" ,"html"=> $html]);
        return false;
    }


    /**
     * 顯示表單模式直接輸入地點資訊
     * */
    public function formAction()
    {
        $this->view->form = new \House\Core\Forms\GeomarkersForm();
    }


    /**
     * 後端請求座標點
     * */
    public function getmarkerAction()
    {
    }


    /**
     * 寫入geomarkers
     * */
    public function saveAction()
    {
        $item = new \Houserich\Models\Geomarkers();
        $form = new \House\Core\Forms\GeomarkersForm();
        $form->bind($_POST, $item);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "display"=>"#alertDanger", "msg"=>$msg]);
        }else{

            try{
                if( !$item->save() ){
                    $msg = array('寫入座標資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", array_merge($msg, $item->getMessages() )) ]);

                }else{
                    echo json_encode(["res"=>1, "display"=>"#alertSuccess","msg"=>"已更新座標點", "id"=>$item->geomarkersId]);
                }
            } catch (\PDOException $e) {

            } catch (\Exception $e) {
                $msg = array('寫入捷運資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
            }

        }

        return false;
    }
}
