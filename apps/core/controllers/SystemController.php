<?php
/**
 * SystemController
 *
 * */

namespace House\Core\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class SystemController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     *
     * */
    public function indexAction()
    {
        $fb= new \Personalwork\Forms\Builder([
                'DI'        => $this->getDI(),
                'path'      => 'houserich/forms/',
                'inclusive' => ['Richitem_School']
             ]);

        $msg = $fb->setNamespace("House\\Houserich\\Forms")
                  ->run()
                  ->getMessages();
        echo implode('<br/>', $msg);

        return false;
    }


    /**
     * 處理表單欄位清單寫入資料表fieldoptions建檔
     *
     * */
    public function optionsAction()
    {
        $fieldname = "型態";

        // $data = array( "請選擇" => null, "住宅"=>"住宅" ,"店面"=>"店面" ,"辦公"=>"辦公" ,"住辦"=>"住辦" ,"廠房"=>"廠房" ,"車位"=>"車位" ,"土地"=>"土地" ,"其他"=>"其他");
        $data = array(
                       "公寓" => "公寓",
                            "華廈" => "華廈",
                            "大樓" => "大樓",
                            "別墅" => "別墅",
                            "透天" => "透天",
                       );

        foreach ($data as $key => $value) {

            $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$key]]);
            if( !$f ){
                $f = new \Houserich\Models\Fieldoptions();
            }

            if( is_string($value) ){
                $f->fieldname = $fieldname;
                $f->label = $key;
                $f->value = $value;
                $f->save();
            }else{
                $row=\Houserich\Models\Fieldoptions::findFirstByLabel($key);
                if( $row ){
                    $parentId = $row->id;
                    $parentLabel = $row->label;
                }else{
                    $parentId = null;
                    $parentLabel = $key;
                }
                foreach ($value as $skey => $svalue) {
                    $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$skey]]);
                    if( !$f ){
                        $f = new \Houserich\Models\Fieldoptions();
                    }

                    $f->parentId = $parentId;
                    $f->parentLabel = $parentLabel;
                    $f->fieldname = $fieldname;
                    $f->label = $skey;
                    $f->value = $svalue;
                    $f->save();
                }
            }

        }

        echo 'done!';

        return false;
    }
}
