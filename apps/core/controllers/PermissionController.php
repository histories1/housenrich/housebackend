<?php
/**
 * PermissionController
 *
 * */

namespace House\Core\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class PermissionController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"權限管理",pageDesc:"",render:"")
     * */
    public function phaseAction()
    {
        $this->view->pick("default/phase");
    }
}
