<?php
/**
 * ConfigureController
 *
 * */

namespace House\Core\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class ConfigureController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"參數設定",pageDesc:"",render:"")
     * */
    public function phaseAction()
    {
        $this->view->pick("default/phase");
    }
}
