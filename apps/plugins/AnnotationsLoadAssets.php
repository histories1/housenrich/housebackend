<?php
/**
 * 處理在Action執行期間判斷@Jslibrary註釋解析附加Assets項目
 * 透過$di['dispatcher']直接註冊
 *
 * Packages list
 * 1. formvalid
 * 2. datatable
 * 3. gridtable
 * 4.
 */
namespace House\Plugins;

use \Phalcon\Mvc\User\Plugin as UserPlugin;
use \Phalcon\Events\Event;
use \Phalcon\Mvc\Dispatcher;

class AnnotationsLoadAssets extends UserPlugin
{
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $annotations = $this->annotations->getMethod(
            $dispatcher->getControllerClass(),
            $dispatcher->getActiveMethod()
        );

        if( $annotations->has('Jslibrary') ){
            $annotation = $annotations->get("Jslibrary");

            // 直接將@Phtml指定為變數
            $jsHeader = array(); $jsFooter = array(); $css = array();
            foreach ($annotation->getArguments() as $index => $package) {
                // var_dump($package);
                switch($package)
                {
                    case 'richeditor':
                    $jsHeader[] = '/misc/bower_component/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js';

                    $css[] = '/misc/bower_component/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css';

                    case 'formelms':
                    // moment
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/moment.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/moment-zh-tw.js';

                    $jsHeader[] = '/misc/js/plugins/bootstrap-multiselect/bootstrap-multiselect.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/twix.min.js';

                    $jsHeader[] = '/misc/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datepicker/bootstrap-datepicker.zh-TW.js';
                    $jsHeader[] = '/misc/js/plugins/daterangepicker/daterangepicker.js';
                    // 日期選擇器
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js';
                    // 日期時間選擇器
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js';
                    // 時間選擇器
                    $jsHeader[] = '/misc/bower_component/bootstrap-timepicker/js/bootstrap-timepicker.js';
                    // 欄位預設格式處理inputmask
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/inputmask.js';
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/inputmask.date.extensions.js';
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/inputmask.numeric.extensions.js';
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/jquery.inputmask.js';

                    // 預設偵測樣式判斷載入處理
                    $jsFooter[] = '/misc/js/form-elms.js';

                        break;
                    // jquery-validation、jquery-validation-pkextend
                    case 'formvalid':
                    $jsHeader[] = '/misc/bower_component/jquery-validation/dist/jquery.validate.min.js';
                    $jsHeader[] = '/misc/bower_component/jquery-validation/dist/additional-methods.min.js';
                    // 包含判斷頁面表單套用表單驗證
                    $jsFooter[] = '/misc/bower_component/jquery-validation-pkextend/validation-extend.js';

                    $css[] = '/misc/bower_component/jquery-validation-pkextend/validation-extend.css';
                    break;
                    // dataTable
                    case 'datatable':
                    // $css[] = '/misc/bower_component/yadcf/jquery.dataTables.yadcf.css';
                    $css[] = 'https://rawgit.com/vedmack/yadcf/0.8.7/jquery.dataTables.yadcf.css';


                    $jsHeader[] = '/misc/js/plugins/datatable/jquery.dataTables.min.js';
                    $jsHeader[] = '/misc/js/plugins/datatable/exts/dataTables.colVis.bootstrap.js';
                    $jsHeader[] = '/misc/js/plugins/datatable/exts/dataTables.colReorder.min.js';
                    $jsHeader[] = '/misc/js/plugins/datatable/exts/dataTables.tableTools.min.js';
                    $jsHeader[] = '/misc/js/plugins/datatable/dataTables.bootstrap.js';
                    $jsHeader[] = '/misc/bower_component/yadcf/jquery.dataTables.yadcf.js';

                    $jsFooter[] = '/misc/js/pk-datatable.js';
                    break;
                    case 'dtxeditor':
                    $css[] = '/misc/bower_component/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css';
                    $jsHeader[] = '/misc/bower_component/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js';
                    break;
                    case 'dtyadcf_datepicker':
                    // moment
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/moment.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/moment-zh-tw.js';
                    // 單純載入日期選擇器
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js';
                    break;
                    // jGrid
                    case 'jgrid':
                    $jsHeader[] = '/misc/js/plugins/jqgrid/jquery.jqGrid.min.js';
                    $jsHeader[] = '/misc/js/plugins/jqgrid/i18n/grid.locale-en.js';
                    $jsHeader[] = '/misc/js/plugins/jqgrid/jquery.jqGrid.fluid.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js';
                    break;
                    // bootstrap-fileinput
                    case 'inputuplaod':
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/plugins/sortable.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/plugins/purify.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/fileinput.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/locales/zh-TW.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/themes/fa/theme.js';

                    $css[] = '/misc/js/plugins/bootstrap-fileinput/css/fileinput.min.css';
                        break;
                    case 'wizard':
                    $jsHeader[] = '/misc/bower_component/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js';

                    $jsFooter[] = '/misc/js/pk-bootstrapwizard.js';
                    break;
                    // google-maps
                    case 'maps':
                    // $jsHeader[] = '/misc/bower_component/gmap3/dist/gmap3.min.js';
                    $jsFooter[] = 'http://maps.google.com/maps/api/js?callback=_initMap&libraries=places&key='.$this->config->google->apikey_js;
                    $jsFooter[] = '/misc/js/pk-googlemap.js';
                    break;
                }
            }

            if( count($jsHeader) > 0 ){
            foreach($jsHeader as $js){
                $this->assets->collection("headerScript")->addJs($js, true);
            }
            }

            if( count($css) > 0 ){
            foreach($css as $style){
                $this->assets->collection("headerStyle")->addCss($style, true);
            }
            }

            if( count($jsFooter) > 0 ){
            foreach($jsFooter as $js){
                $this->assets->collection("footerScript")->addJs($js, true);
            }
            }
        }
    }
}
