<?php
/**
 * 處理在Action執行期間將@View註釋解析轉存View變數
 * 透過$di['dispatcher']直接註冊
 *
 */
namespace House\Plugins;

use \Phalcon\Mvc\User\Plugin as UserPlugin;
use \Phalcon\Events\Event;
use \Phalcon\Mvc\Dispatcher;

class AnnotationsToPhtml extends UserPlugin
{
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        // get Class Annotations
        // $reflator=$annotations->get( $dispatcher->getControllerClass() );

        $annotations = $this->annotations->getMethod(
            $dispatcher->getControllerClass(),
            $dispatcher->getActiveMethod()
        );

        if( $annotations->has('View') ){
            $annotation = $annotations->get("View");

            // 直接將@Phtml指定為變數
            // foreach ($annotation->getArguments() as $key => $value) {
            //     var_dump($key);
            //     var_dump($value);
            // }
            $this->view->setVars($annotation->getArguments());
        }
    }


    /**
     * 自動根據指定Phtml變數與樣板檔案存在與否來render規則命名樣板檔
     *
     * */
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $path = array();
        $path[] = PPS_APP_APPSPATH . "/views/";
        $namespace = explode("\\", $dispatcher->getNamespaceName());

        // like member/profile-phase.phtml
        if( $this->view->render == 'ruleMCA' ){
            $path[] = strtolower($namespace[count($namespace)-2]."/");
            $path[] = $this->dispatcher->getControllerName().'-'.$this->dispatcher->getActionName();
            $path[] = '.phtml';
            $file = implode("",$path);
            if( file_exists( $file ) ){
                $this->view->pick( strtolower($path[1].$path[2]) );
            }
        // like index/about.phtml
        }elseif( $this->view->render == 'ruleCA' ){
            $path[] = strtolower($this->dispatcher->getControllerName()."/");
            $path[] = strtolower($this->dispatcher->getActionName());
            $path[] = '.phtml';

            $file = implode("",$path);
            if( file_exists( $file ) ){
                $this->view->pick( $path[1].$path[2] );
            }
        // like content/phase-faq.phtml
        }elseif( $this->view->render == 'ruleMAC' ){
            $path[] = strtolower($namespace[count($namespace)-2]."/");
            $path[] = strtolower($this->dispatcher->getActionName().'-'.$this->dispatcher->getControllerName());
            $path[] = '.phtml';
            $file = implode("",$path);
            if( file_exists( $file ) ){
                $this->view->pick( $path[1].$path[2] );
            }
        }
    }
}
