<?php
namespace House\Plugins;

use \Phalcon\Mvc\User\Plugin as UserPlugin;


/**
 * 處理在介面上載入預設多媒體檔案清單作法！
 */
class LoadMedias extends UserPlugin
{
    function __construct($opts=array())
    {
        if( isset($opts['model']) ){
            $this->model = $opts['model'];
        }

        if( isset($opts['name']) ){
            $this->name = $opts['name'];
        }

        if( isset($opts['KeyName']) ){
            $this->fieldname = $opts['KeyName'];
            $this->ID = $opts['KeyID'];
        }

        // RichItem
        if( isset($opts['type']) ){
            $this->type = $opts['type'];
        }
    }


    /**
     * @todo : 修正判斷欄位與編號的部分
     * */
    public function loadpriviews() {
        $data = array();
        // 載入預設檔案作法
        $modelclass=$this->model;

        if( empty($this->ID) ){
            return ;
        }


        if( isset($this->type) ){
            $conf = "statecode=1 and type='{$this->type}' and {$this->fieldname}={$this->ID}";
        }else{
            $conf = "statecode=1 and {$this->fieldname}={$this->ID}";
        }

        $medias = $modelclass::find($conf);

        if( count($medias) > 0 ){
        foreach ($medias as $key => $media) {

            $accesspath = $this->url->get("file/access/{$this->name}/{$media->UUID}");
            $initialPreview[] = $accesspath;
                if( preg_match("/pdf/", $media->mime) ){
                    $ftype = 'pdf';
                }else{
                    $ftype = 'image';
                }
                $initialPreviewConfig[] = '{
                    "caption":"'.$media->originname.'",
                    "type":"'.$ftype.'",
                    "filetype":"'.$media->mime.'",
                    "url":"'.$this->url->get("file/delete/{$this->name}/{$media->UUID}/1").'",
                    "width":"160px",
                    "size":'.$media->size.',
                    "key":'.$media->weight.',
                    "extra":{"ID" : '.$media->mediaId.'}
                }';
        }
        $tmp = implode('","', $initialPreview);
        $data['initialPreview'] = <<< STRING
["{$tmp}"]
STRING;
        $tmp = implode(',', $initialPreviewConfig);
        $data['initialPreviewConfig'] = <<< STRING
[{$tmp}]
STRING;
        }
        return $data;
    }
}
