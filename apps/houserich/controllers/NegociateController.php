<?php
/**
 * NegociateController 議價管理
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class NegociateController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"議價管理",pageDesc:"物件議價流程管理作業。",render:"")
     * */
    public function phaseAction()
    {
        $this->view->pick("default/phase");
    }
}
