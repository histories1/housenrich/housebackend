<?php
/**
 * SchoolController 學區管理機制
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package houserich
 * */
class SchoolController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"學區",pageDesc:"縣市各區域中小學學區管理作業。",render:"ruleCA")
     *
     * @Jslibrary("datatable","dtyadcf_datepicker")
     * */
    public function phaseAction()
    {
        $this->view->datatable = array(
                                    'thead' => array(
                                                    '編號'    => '5%',
                                                    '縣市'    => '15%',
                                                    '行政區' => '15%',
                                                    '學校' => '20%',
                                                    '地址' => '20%',
                                                    '操作'    => '15%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "schoolId",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "addressCity",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "addressDistrict",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "name",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "address",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "func",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '<div class="btn-group">
<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">管理功能 <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu">
    <li class="dropdown-header">操作項目</li>
    <li><a href="{$this->url->get('houserich/school/form/edit-')}'+data+'">編輯</a></li>
    <li><a href="{$this->url->get('houserich/school/delete/')}'+data+'">刪除</a></li>
</ul>
</div>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 2,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_type"   => "text"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('houserich/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
    }


    /**/
    public function loadRecordsAction()
    {
        $phql = "SELECT S.*
                 FROM school S
                 ";
        $S = new \Houserich\Models\School();

        $defaultphql = $phql;
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $S , $S->getReadConnection()->query($defaultphql));

        $withoutfilter = true; $condition=array();
        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                switch ($param['data']) {
                    default:
                    $condition[] = "S.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;
                }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $S , $S->getReadConnection()->query($phql));


        //針對欄位配置顯示數值
        foreach($filters as $row){
            $school = \Houserich\Models\School::findFirst($row->schoolId);
            $record = array();
            $record['schoolId'] = $school->schoolId;
            $record['addressCity'] = $school->addressCity;
            $record['addressDistrict'] = $school->addressDistrict;
            $record['name'] = $school->name;
            $record['address'] = $school->address;
            $record['func'] = $school->schoolId;

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * @View(pageHeader:"學區建檔",pageDesc:"建檔表單",render:"ruleCA")
     *
     * @Jslibrary("richeditor","formelms","formvalid", "maps")
     * */
    public function formAction()
    {
        if( $this->params[0] == 'reset' && $this->session->has('create-schoolId') ){
            $this->session->remove('create-schoolId');
            $this->session->remove('edit-schoolId');
        }

        $school = null;
        if( $this->session->has('create-schoolId') ){
            $school = \Houserich\Models\School::findFirst( $this->session->get('create-schoolId') );
        }elseif( $this->params[0] == 'edit' ){
            $this->session->set('edit-schoolId', $this->params[1]);
            $school = \Houserich\Models\School::findFirst( $this->params[1] );
        }elseif( $this->session->has('edit-schoolId')  ){
            $school = \Houserich\Models\School::findFirst( $this->session->get('edit-schoolId') );
        }
        $this->view->form = new \House\Houserich\Forms\SchoolForm($school);
    }


    /**
     * 學區資料新增與修改
     * */
    public function saveAction()
    {
        $item = new \Houserich\Models\School();
        $form = new \House\Houserich\Forms\SchoolForm();
        $form->bind($_POST, $item);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            // var_dump($msg);
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
        }else{
            try{
                if( !$item->save() ){
                    $msg = array('寫入學區資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "msg"=>implode("\n", array_merge($msg, $item->getMessages() )) ]);
                }else{
                    // schoolId
                    $this->session->set('create-schoolId', $item->schoolId);

                    echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=> "已完成編輯學區紀錄", "PrimaryID"=>"#schoolId", "id"=>$item->schoolId]);
                }
            } catch (\PDOException $e) {
                $msg = array('寫入學區資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "msg"=>implode("\n", $msg)]);
            }

        }

        return false;
    }


    /**
     * 直接刪除作法
     * */
    public function deleteAction()
    {
        if( !intval($this->params[1]) ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未指定刪除記錄編號，無法刪除！"]);
            return false;
        }

        $item = \Houserich\Models\Mrt::findFirst( $this->params[1] );
        if( !$item ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未找到對應學校紀錄，無法刪除！"]);
            return false;
        }elseif( !$item->delete() ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"刪除記錄編號發生錯誤，訊息：".implode(',', $item->getMessages())]);
            return false;
        }else{
            echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"已刪除指定學校紀錄。"]);
            return false;
        }
    }
}
