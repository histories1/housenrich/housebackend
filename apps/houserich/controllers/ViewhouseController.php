<?php
/**
 * ViewhouseController 看屋管理（預約看屋）
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class ViewhouseController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"預約看屋管理",pageDesc:"列舉前台會員提出預約看屋流程紀錄進行管理。",render:"ruleCA")
     *
     * @see 1.配置js套件(dataTable、dataTable表格標頭過濾)
     * @Jslibrary("datatable","dtyadcf_datepicker")
     * */
    public function phaseAction()
    {
        $this->view->datatable = array(
                                    'thead' => array(
                                                    '全選'    => [
                                                        'width'=>'7%',
                                                        'label'=>'<input type="checkbox" class="opt-checkall"name="checkall" />全選'
                                                    ],
                                                    '會員'    => '15%',
                                                    '電話'    => '15%',
                                                    '預約內容' => '33%',
                                                    '時間'    => '15%',
                                                    '操作'    => '15%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "ppId",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '
    <input type="checkbox" id="ppId_'+data+'" name="ppId[]" value="'+data+'" />
    ';
}#
FUNCTION
                                        ],
                                        [
                                            "data" => "peopleName",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "peopleAccount",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "title",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "setTimeFt",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "func",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '<div class="btn-group">
<a class="btn btn-primary btn-sm btn-modal"
   href="javascript:;"
   data-remote="{$this->url->get('houserich/viewhouse/load-detail/')}'+data+'">詳情
</a>
</div>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 2,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_type"   => "text"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('houserich/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
    }


    /**
     * @see 2.配置ajax載入列表方式
     * */
    public function loadRecordsAction()
    {
        $phql = "SELECT PR.*
                 FROM people_reserve PR
                 LEFT JOIN people_information PI ON PR.PeopleId=PI.PeopleId
                 ";
        $PR = new \Houserich\Models\PeopleReserve();

        $defaultphql = $phql;
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $PR , $PR->getReadConnection()->query($defaultphql));

        $withoutfilter = true; $condition=array();
        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                switch ($param['data']) {
                    case "":
                    $condition[] = "PR.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;

                    default:
                    $condition[] = "PR.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;
                }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $PR , $PR->getReadConnection()->query($phql));

        //針對欄位配置顯示數值
        foreach($filters as $row){
            // $crawldata = \Houserich\Models\Crawldata::findFirst($row->CrawdataId);
            $peopleinfo = \Houserich\Models\PeopleInformation::findFirst($row->PeopleId);
            $record = $row->toArray();
            $record['peopleName'] = $peopleinfo->fullName;
            $record['peopleAccount'] = $peopleinfo->account;
            $record['setTimeFt'] = date('Y/m/d H:i:s',$row->setTime);
            $record['func'] = $row->ppId;

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * 配置查看詳情載入modal顯示詳細物件
     * 不使用layout只顯示action view作法
     * */
    public function loadDetailAction()
    {
        $row = \Houserich\Models\PeopleReserve::findFirst( $this->params[0] );
        $this->view->crawldata = \Houserich\Models\Crawldata::findFirst($row->CrawdataId);

        $this->view->setRenderLevel( \Phalcon\Mvc\View::LEVEL_ACTION_VIEW );
        $this->view->render("viewhouse", "load-detail");
        // $this->view->pick(["viewhouse/load-detail"]);
    }
}
