<?php
/**
 * TradeController 交易議價管理
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package houserich
 * */
class TradeController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"交易議價管理",pageDesc:"交易議價管理跟議價管理？",render:"ruleMAC")
     * */
    public function phaseAction()
    {
        $this->view->pick("default/phase");
    }


    /**
     * @View(pageHeader:"保證金管理",pageDesc:"保證金與虛擬帳號應該是隸屬於會員之下，解釋當初配置構想？",render:"ruleMAC")
     * */
    public function accountAction()
    {
        $this->view->pick("default/phase");
    }

    /**
     * @View(pageHeader:"成交管理",pageDesc:"除了成交以外的交易呢？",render:"ruleMAC")
     * */
    public function tempAction()
    {
        $this->view->pick("default/phase");
    }
}
