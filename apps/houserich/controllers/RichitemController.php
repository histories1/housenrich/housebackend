<?php
/**
 * RichitemController 物件管理
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package houserich
 * */
class RichitemController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }

    /**
     * @View(pageHeader:"物件列表",pageDesc:"顯示物件項目並由此開始建立物件內容。",render:"ruleCA")
     *
     * @Jslibrary("datatable")
     * */
    public function phaseAction()
    {
        $this->view->datatable = array(
                                    'thead' => array(
                                                    '編號'    => '5%',
                                                    '案件編號' => '15%',
                                                    '物件名稱' => '15%',
                                                    '用途'    => '10%',
                                                    '型態'    => '10%',
                                                    '地址'    => '20%',
                                                    '操作'    => '10%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "richitemId",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "richitemNo",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "title",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "usefor",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "type",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "address",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "func",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '<div class="btn-group">
<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">管理功能 <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu">
    <li class="dropdown-header">操作項目</li>
    <li><a href="{$this->url->get('houserich/richitem/form/edit-')}'+data+'">編輯</a></li>
    <li><a href="{$this->url->get('houserich/richitem/delete/')}'+data+'">刪除</a></li>
</ul>
</div>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 1,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 2,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 4,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 5,
                                            "filter_type"   => "text"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('houserich/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
        // $this->view->pick('richitem/phase');
    }


    /**/
    public function loadRecordsAction()
    {
        $phql = "SELECT R.*
                 FROM richitem R
                 ";
        $R = new \Houserich\Models\Richitem();

        $defaultphql = $phql;
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $R , $R->getReadConnection()->query($defaultphql));

        $withoutfilter = true; $condition=array();
        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                // switch ($param['data']) {
                //     case 'showname':
                //     $condition[] = "s.showname LIKE '%{$param['search']['value']}%'";
                //     break;
                //     case 'typeLabel':
                //     $code=$s->getTypecode($param['search']['value']);
                //     $condition[] = "s.type = '{$code}'";
                //     break;
                //     case 'twFormat':
                //     if( ($timestamp=strtotime($param['search']['value'])) !== false ){
                //         $condition[] = "ss1.setTime>={$timestamp}";
                //     }
                //     break;
                //     case 'employeeName':
                //     $condition[] = "EmployeeId IN (SELECT employeeId FROM people WHERE name LIKE '%{$param['search']['value']}%')";
                //     break;
                //     case 'laststateLabel':
                //     $code = $ps->getStateLabelcode($param['search']['value']);
                //     $condition[] = "ss.statecode = '{$code}'";
                //     break;
                // }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                // case 'twFormat':
                // $order[] = "date {$order['dir']}";
                // break;
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $R , $R->getReadConnection()->query($phql));


        //針對欄位配置顯示數值
        foreach($filters as $row){
            $richitem = \Houserich\Models\Richitem::findFirst($row->richitemId);
            $record = array();
            $record['richitemId'] = $richitem->richitemId;
            $record['richitemNo'] = $richitem->richitemNo;
            $record['title'] = $richitem->title;
            $record['usefor'] = $richitem->usefor;
            $record['type'] = $richitem->type;
            $record['address'] = $richitem->address;
            $record['func'] = $richitem->richitemId;

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * @View(pageHeader:"物件新增與編輯表單流程",pageDesc:"紅色框框代表必填欄位，物件主表欄位建立完成後可直接切換第二延伸欄位表單與第三上傳檔案表單，延伸欄位表單除第一區塊外，其他區塊預設收合，點選右上箭頭可展開。",render:"ruleCA")
     *
     * @Jslibrary("richeditor","formelms","formvalid","inputuplaod","wizard", "maps")
     * */
    public function formAction()
    {
        if( $this->params[0] == 'reset' ){
            $this->session->remove('create-richitemId');
            $this->session->remove('create-richitemNo');
            $this->session->remove('edit-richitemId');
            $this->session->remove('richitemNo');
        }

        // 處理載入社區資料內容
        if( $this->params[0] == 'rechoice' ){
            $this->session->remove('richitem-community');
        }elseif( $this->params[0] == 'edit' ){
            $this->session->set('richitem-community', 'edit');
        }

        if( $_POST['do'] == 'yes'){
            $c = \Houserich\Models\Community::findFirst( $_POST['communityId']);
            $this->session->set('richitem-community', $c->toArray());
        }elseif( $_POST['do'] == 'no'){
            $this->session->set('richitem-community', $_POST['do']);
        }

        $this->assets->collection("headerScript")->addJs('misc/js/form-richitem.js');

        $this->view->formsteps = array(
                                    array(
                                        1,
                                        "物件基本欄位",
                                    ),
                                    array(
                                        2,
                                        "物件延伸欄位",
                                    ),
                                    array(
                                        3,
                                        "設定學區與捷運",
                                        "配置物件所屬國中國小與最近捷運清單。"
                                    ),
                                    array(
                                        4,
                                        "上傳文件照片檔案",
                                        "配置物件各項文件與照片檔案。"
                                    ),
                                 );

        $richitem = null;
        if( $this->session->has('create-richitemId') ){
            $richitem = \Houserich\Models\Richitem::findFirst( $this->session->get('create-richitemId') );
            $d = $richitem->toArray();
            $this->session->set('richitemNo', $d['richitemNo'] );
        }elseif( $this->params[0] == 'edit' ){
            $this->session->remove('create-richitemId');
            $richitem = \Houserich\Models\Richitem::findFirst( $this->params[1] );
            $d = $richitem->toArray();
            $this->session->set('edit-richitemId', $this->params[1]);
            $this->session->set('richitemNo', $d['richitemNo'] );
        }elseif( $this->session->has('edit-richitemId')  ){
            $richitem = \Houserich\Models\Richitem::findFirst( $this->session->get('edit-richitemId') );
        }

        /**
         * 載入編輯模式其他表單
         * */
        // 主建物表單
        $this->view->formM = new \House\Houserich\Forms\RichitemAreaMainbuildingForm();
        // 土地表單
        $this->view->formL = new \House\Houserich\Forms\RichitemAreaLandForm();
        // 附屬建物
        $this->view->formA = new \House\Houserich\Forms\RichitemAreaAppendbuildingForm();
        // 共有部分
        $this->view->formS = new \House\Houserich\Forms\RichitemAreaSharepartForm();
        // 車位
        $this->view->formP = new \House\Houserich\Forms\RichitemAreaParkingForm();
        // 物件延伸欄位
        $info = ( $richitem->RichitemInformation ) ? $richitem->RichitemInformation : null;
        $this->view->formI = new \House\Houserich\Forms\RichitemInformationForm($info);

        // 所有權人
        $this->view->formO = new \House\Houserich\Forms\RichitemOwnerForm();
        // 看屋時間
        $this->view->formV = new \House\Houserich\Forms\RichitemViewhouseForm();

        $this->view->form = new \House\Houserich\Forms\RichitemForm($richitem);
    }


    /**
     * 載入部分表單區塊
     * 主建物、土地、附屬建物、車位等多項類型欄位群組
     * */
    public function loadFormAction()
    {
        if( !$this->request->getPost('form') ){
            echo json_encode(array("res"=>0, "display"=>"#alertDanger", "msg"=>"未取得正確表單類別！無法完成操作。"));
            return false;
        }

        $class="\\House\\Houserich\\Forms\\".ucfirst($this->request->getPost('form'));
        $form = new $class();

        /**
         * 針對redio/checkbox群組元素必須將產出的元素id唯一化
         * 透過每次計算數值方式傳遞變更id值
         * */
        if( $this->request->getPost('itemcount') &&
            method_exists($form, 'setGroupElementCounts') ){
            $form->setGroupElementCounts( $this->request->getPost('itemcount') );
        }

        echo json_encode(["res"=>1, "html"=> $form->renderCreate(1)]);
        return false;
    }


    /**
     * 處理物件主表建檔與編輯
     * */
    public function saveAction()
    {
        $item = new \Houserich\Models\Richitem();
        $form = new \House\Houserich\Forms\RichitemForm();
        $form->bind($_POST, $item);

        if( !empty($_POST['publicFacility']) ){
        $_POST['publicFacility'] = implode(",",$_POST['publicFacility']);
        }

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
        }else{
            try{
                if( !$item->save() ){
                    $msg = array('寫入物件資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "msg"=>implode("\n", array_merge($msg, $item->getMessages() )), "display"=>"#alertWarning"]);
                }else{
                    // richitemId
                    $this->session->set('create-richitemId', $item->richitemId);
                    // richitemNo
                    $this->session->set('richitemNo', $item->richitemNo);

                    $d = ["res"=>1, "id"=>$item->richitemId, "no"=>$item->richitemNo];
                    if( !empty($_POST['richitemId']) ){
                        $d["msg"]="完成更新[{$item->richitemNo}]物件欄位。";
                        $d["display"]="#alertSuccess";
                        $d["PrimaryId"]="#richitemId";
                    }else{
                        $d["msg"]="已完成新增[{$item->richitemNo}]物件基本欄位。";
                        $d["display"]="#alertSuccess";
                        $d["PrimaryId"]="#richitemId";
                    }
                    echo json_encode($d);
                }
            } catch (\PDOException $e) {
                $msg = array('寫入物件資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "msg"=>implode("\n", $msg), "alerts"=>"popupAlert"]);
            }

        }

        return false;
    }


    /**
     * 處理物件延伸欄位各類新增/修改/刪除等動作
     * */
    public function itemExtendsAction()
    {
        switch($this->params[0])
        {
            // create & update
            case 'save':
                // 重複送出！
                if( !empty($this->REPOST) ){
                    echo json_encode(array("res"=>0, "alerts"=>"popupAlert", "msg"=>'系統已處理過本次表單，請勿直接再次。'));
                    return false;
                }

                if( $this->request->isPost() ) {

                    $modelClass = "\\Houserich\\Models\\{$this->params[1]}";
                    $formClass = "\\House\\Houserich\\Forms\\{$this->params[1]}Form";
                    $form = new $formClass();

                    // 取得primary key name
                    $primaryKey = $this->modelsMetadata->getIdentityField(new $modelClass());
                    // 取得欄位清單
                    $columnsWithoutPrimary = $this->modelsMetadata->getNonPrimaryKeyAttributes(new $modelClass());
                    // 取得必填欄位清單
                    $columnsRequired = $this->modelsMetadata->getNotNullAttributes(new $modelClass());

// var_dump($primaryKey);
// var_dump($columnsWithoutPrimary);
// var_dump($columnsRequired);
// var_dump($_POST);
// die();
                    foreach ($_POST[$primaryKey] as $i => $value) {
                        $data = array();
                        foreach( $columnsWithoutPrimary as $fname){
                            $data[$fname] = $_POST[$fname][$i];
                        }

                        // 判斷若已有主鍵則使用編輯模式
                        if( !empty($value) ){
                            $model = $modelClass::findFirst($value);
                        }else{
                            $model = new $modelClass();
                        }
                        // 透過表單類別綁定$_POST與對應欄位
                        $form->bind($data, $model);

                        if( !$model->save() ){

                            // 判斷若設定必填欄位、主鍵欄位都沒有內容情況下仍忽略直接繼續處理！
                            if( $value ){
                                $msgs = array('寫入物件資料發生錯誤，請根據以下訊息修正後再次送出：');
                                $msg = implode("\n", array_merge($msgs, $model->getMessages() ));
                                echo json_encode(array("res"=>0, "alerts"=>"popupAlert", "msg"=>$msg));
                                return false;
                            }
                            foreach ($columnsRequired as $column) {
                                if( !empty($_POST[$column][$i]) ){
                                $msgs = array('寫入物件資料發生錯誤，請根據以下訊息修正後再次送出：');
                                $msg = implode("\n", array_merge($msgs, $model->getMessages() ));
                                echo json_encode(array("res"=>0, "alerts"=>"popupAlert", "msg"=>$msg));
                                return false;
                                }
                            }

                        }

                    }
                    echo json_encode(array("res"=>1, "alerts"=>"popupAlert", "msg"=>"已完成本次物件延伸欄位建檔/更新作業\n請注意！本次新增的紀錄，若要刪除、修改必須重新整理畫面。"));
                }else{
                    echo json_encode(array("res"=>0, "alerts"=>"popupAlert", "msg"=>'功能發生錯誤，請使用表單傳遞方式處理。'));
                }
                break;
            case 'saveSchool':
                // 重複送出！
                if( !empty($this->REPOST) ){
                    echo json_encode(array("res"=>0, "alerts"=>"popupAlert", "msg"=>'系統已處理過本次表單，請勿直接再次。'));
                    return false;
                }

                if( $this->request->isPost() ) {

                    $modelClass = "\\Houserich\\Models\\{$this->params[1]}";
                    $formClass = "\\House\\Houserich\\Forms\\{$this->params[1]}Form";
                    $form = new $formClass();

                    $model = $modelClass::findFirst(["RichitemId=?0 AND SchoolId=?1", 'bind'=>[$_POST['RichitemId'], $_POST['SchoolId']]]);
                    if( !$model ){
                        $model = new $modelClass();
                    }
                    $form->bind($_POST, $model);

                    if( !$model->save() ){
                        $msgs = array('設定物件學區發生錯誤，請根據以下訊息修正後再次送出：');
                        $msg = implode("\n", array_merge($msgs, $model->getMessages() ));
                        echo json_encode(array("res"=>0, "alerts"=>"popupAlert", "msg"=>$msg));
                        return false;
                    }else{
                        echo json_encode(array("res"=>1, "display"=>"#alertSuccess", "msg"=>"已完成物件學區設定作業"));
                    }
                }
                break;
            case 'delete':
                // model class
                $mClass = "\\Houserich\\Models\\{$this->params[1]}";
                // ID
                $item = $mClass::findFirst( $this->params[2] );
                if( $item && $item->delete() ) {
                    echo json_encode(["res"=>1, "alerts"=>"popupAlert", "msg"=>"已成功刪除該筆主建物面積紀錄。"]);
                }elseif( !$item ){
                    echo json_encode(["res"=>1, "alerts"=>"popupAlert", "msg"=>"無法對應該筆主建物面積紀錄，刪除失敗，建議重新整理頁面後嘗試。"]);
                }elseif( !$item->delete() ){
                    echo json_encode(["res"=>1, "alerts"=>"popupAlert", "msg"=>"刪除發生錯誤，訊息:".implode(",",$item->getMessages())."。"]);
                }
                break;
        }

        return false;
    }


    /**
     * 處理物件資訊新增/修改/刪除等動作
     * */
    public function saveInfoAction(){

        $model = new \Houserich\Models\RichitemInformation();
        $form = new \House\Houserich\Forms\RichitemInformationForm();
        $form->bind($_POST, $model);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "alerts"=> "popupAlert", "msg"=>implode("\n", $msg)]);
        }else{
            try{
                if( !$model->save() ){
                    $msg = array('寫入物件延伸資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "msg"=>implode("\n", array_merge($msg, $item->getMessages() )), "alerts"=>"popupAlert"]);
                }else{

                    $d = ["res"=>1];
                    if( !empty($_POST['richitemId']) ){
                        $d["msg"]="完成更新物件延伸欄位。";
                        $d["alerts"]="popupAlert";
                    }else{
                        $d["msg"]="已完成新增物件基本欄位。";
                        $d["alerts"]="popupAlert";
                    }
                    echo json_encode($d);
                }
            } catch (\PDOException $e) {
                $msg = array('寫入物件資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "msg"=>implode("\n", $msg), "alerts"=>"popupAlert"]);
            }

        }

        return false;
    }


    /**
     * 直接刪除作法
     * */
    public function deleteAction()
    {
        if( !intval($this->params[1]) ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未指定刪除記錄編號，無法刪除！"]);
            return false;
        }

        $item = \Houserich\Models\Mrt::findFirst( $this->params[1] );
        if( !$item ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未找到對應物件紀錄，無法刪除！"]);
            return false;
        }elseif( !$item->delete() ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"刪除記錄編號發生錯誤，訊息：".implode(',', $item->getMessages())]);
            return false;
        }else{
            echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"已刪除指定物件紀錄。"]);
            return false;
        }
    }
}
