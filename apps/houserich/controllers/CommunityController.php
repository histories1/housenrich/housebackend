<?php
/**
 * CommunityController 社區大樓管理機制
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;
// cUrl in Phalcon
use Phalcon\Http\Client\Request;
/**
 * @package houserich
 * */
class CommunityController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }

    /**
     * @View(pageHeader:"上傳功能",pageDesc:"社區大樓管理作業。",render:"ruleCA")
     *
     * @Jslibrary("inputuplaod")
     * */
    public function photoAction()
    {
        $this->view->form = new \House\Houserich\Forms\CommunityMediaForm();
    }


    /**
     * @View(pageHeader:"社區大樓",pageDesc:"社區大樓管理作業。",render:"ruleCA")
     *
     * @Jslibrary("datatable","dtyadcf_datepicker")
     * */
    public function phaseAction()
    {
        $this->view->datatable = array(
                                    'thead' => array(
                                                    '編號'    => '5%',
                                                    '社區名稱' => '15%',
                                                    '建設公司' => '15%',
                                                    '地址'    => '25%',
                                                    '完工日'  => '10%',
                                                    '屋齡'    => '9%',
                                                    '總層數'   => '12%',
                                                    '路寬'    => '12%',
                                                    '操作'    => '15%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "communityId",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "name",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "company",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "address",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "completion",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "houseAge",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "floorAll",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "rootWidth",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "func",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '<div class="btn-group">
<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">管理功能 <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu">
    <li class="dropdown-header">操作項目</li>
    <li><a href="{$this->url->get('houserich/community/form/edit-')}'+data+'">編輯</a></li>
    <li><a href="{$this->url->get('houserich/community/delete/')}'+data+'">刪除</a></li>
</ul>
</div>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 1,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 2,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 4,
                                            "filter_type"   => "date",
                                            "datepicker_type"=> "bootstrap-datetimepicker",
                                            "date_format"   => "Y-MM-DD"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('houserich/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
    }


    /**/
    public function loadRecordsAction()
    {
        $phql = "SELECT C.*
                 FROM community C
                 ";
        $C = new \Houserich\Models\Community();

        $defaultphql = $phql;
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $C , $C->getReadConnection()->query($defaultphql));

        $withoutfilter = true; $condition=array();
        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                switch ($param['data']) {
                    case 'name':
                    case 'company':
                    case 'address':
                    $condition[] = "C.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;
                    case 'completion':
                    $condition[] = "C.completion = '{$param['search']['value']}'";
                    break;
                }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                case 'houseAge':
                $ordset[] = "houseAgeYear {$order['dir']}";
                break;
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $C , $C->getReadConnection()->query($phql));


        //針對欄位配置顯示數值
        foreach($filters as $row){
            $community = \Houserich\Models\Community::findFirst($row->communityId);
            $record = array();
            $record['communityId'] = $community->communityId;
            $record['name'] = $community->name;
            $record['company'] = $community->company;
            $record['address'] = $community->address;
            $record['completion'] = $community->completion;
            $record['houseAge'] = $community->houseAgeYear.'年'.$community->houseAgeMonth.'月';
            $record['floorAll'] = "地上{$community->floorAbove}層地下{$community->floorBelow}層";
            $record['rootWidth'] = $community->roadWidth;
            $record['func'] = $community->communityId;

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * @View(pageHeader:"社區大樓建檔",pageDesc:"建檔表單",render:"ruleCA")
     *
     * @Jslibrary("richeditor","formelms","formvalid", "inputuplaod", "maps")
     * */
    public function formAction()
    {
        if( $this->params[0] == 'reset' && $this->session->has('create-communityId') ){
            $this->session->remove('create-communityId');
            $this->session->remove('edit-communityId');
        }

        $community = null;
        if( $this->session->has('create-communityId') ){
            $community = \Houserich\Models\Community::findFirst( $this->session->get('create-communityId') );
        }elseif( $this->params[0] == 'edit' ){
            $this->session->remove('create-communityId');
            $this->session->set('edit-communityId', $this->params[1]);
            $community = \Houserich\Models\Community::findFirst( $this->params[1] );
        }elseif( $this->session->has('edit-communityId')  ){
            $community = \Houserich\Models\Community::findFirst( $this->session->get('edit-communityId') );
        }
        $this->view->form = new \House\Houserich\Forms\CommunityForm($community);
    }


    /**
     * 社區大樓資料新增與修改
     * */
    public function saveAction()
    {
        $item = new \Houserich\Models\Community();
        $form = new \House\Houserich\Forms\CommunityForm();
        $form->bind($_POST, $item);

        $_POST['publicFacility'] = implode(",",$_POST['publicFacility']);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
        }else{
            try{
                if( !$item->save() ){
                    $msg = array('寫入社區大樓資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", array_merge($msg, $item->getMessages() )) ]);
                }else{
                    // communityId
                    $this->session->set('create-communityId', $item->communityId);

                    echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=> "已完成編輯社區大樓紀錄", "PrimaryID"=>"#communityId", "id"=>$item->communityId]);
                }
            } catch (\PDOException $e) {
                $msg = array('寫入社區大樓資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
            }

        }

        return false;
    }


    /**
     * 直接刪除作法
     * */
    public function deleteAction()
    {
        if( !intval($this->params[1]) ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未指定刪除記錄編號，無法刪除！"]);
            return false;
        }

        $item = \Houserich\Models\Mrt::findFirst( $this->params[1] );
        if( !$item ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未找到對應社區大樓紀錄，無法刪除！"]);
            return false;
        }elseif( !$item->delete() ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"刪除記錄編號發生錯誤，訊息：".implode(',', $item->getMessages())]);
            return false;
        }else{
            echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"已刪除指定社區大樓紀錄。"]);
            return false;
        }
    }
}
