<?php
/**
 * MrtController 捷運站管理機制
 *
 * */

namespace House\Houserich\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package houserich
 * */
class MrtController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"捷運站",pageDesc:"捷運站管理作業。",render:"ruleCA")
     *
     * @Jslibrary("datatable")
     * */
    public function phaseAction()
    {

        $this->view->datatable = array(
                                    'thead' => array(
                                                    '編號'    => '5%',
                                                    '縣市'    => '25%',
                                                    '行政區'   => '10%',
                                                    '站名'    => '15%',
                                                    '路線名稱' => '15%',
                                                    '出入口名稱'=> '9%',
                                                    '通車狀態' => '12%',
                                                    '操作'    => '10%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "mrtId",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "city",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "district",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "name",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "lineLabel",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "exitName",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "statecodeLabel",
                                            "sortable" =>  false ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "func",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '<div class="btn-group">
<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">管理功能 <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu">
    <li class="dropdown-header">操作項目</li>
    <li><a href="{$this->url->get('houserich/mrt/form/edit-')}'+data+'">編輯</a></li>
    <li><a href="{$this->url->get('houserich/mrt/delete/')}'+data+'">刪除</a></li>
</ul>
</div>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 1,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 2,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 4,
                                            "filter_type"   => "text"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('houserich/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
    }


    /**/
    public function loadRecordsAction()
    {
        $phql = "SELECT M.*
                 FROM mrt M
                 ";
        $M = new \Houserich\Models\Mrt();

        $defaultphql = $phql;
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $M , $M->getReadConnection()->query($defaultphql));

        $withoutfilter = true; $condition=array();
        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                // switch ($param['data']) {
                //     case 'showname':
                //     $condition[] = "s.showname LIKE '%{$param['search']['value']}%'";
                //     break;
                //     case 'typeLabel':
                //     $code=$s->getTypecode($param['search']['value']);
                //     $condition[] = "s.type = '{$code}'";
                //     break;
                //     case 'twFormat':
                //     if( ($timestamp=strtotime($param['search']['value'])) !== false ){
                //         $condition[] = "ss1.setTime>={$timestamp}";
                //     }
                //     break;
                //     case 'employeeName':
                //     $condition[] = "EmployeeId IN (SELECT employeeId FROM people WHERE name LIKE '%{$param['search']['value']}%')";
                //     break;
                //     case 'laststateLabel':
                //     $code = $ps->getStateLabelcode($param['search']['value']);
                //     $condition[] = "ss.statecode = '{$code}'";
                //     break;
                // }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                // case 'twFormat':
                // $order[] = "date {$order['dir']}";
                // break;
                // case 'typeLabel':
                // $order[] = "type {$order['dir']}";
                // break;
                // case 'employeeName':
                // $order[] = "EmployeeId {$order['dir']}";
                // break;
                // case 'laststateLabel':
                // $order[] = "ss.statecode {$order['dir']}";
                // break;
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $M , $M->getReadConnection()->query($phql));


        //針對欄位配置顯示數值
        foreach($filters as $row){
            $mrt = \Houserich\Models\Mrt::findFirst($row->mrtId);
            $record = array();
            $record['mrtId'] = $mrt->mrtId;
            $record['city'] = $mrt->city;
            $record['district'] = $mrt->district;
            $record['name'] = $mrt->name;
            $record['lineLabel'] = $mrt->lineLabel;
            $record['exitName'] = $mrt->exitName;
            $record['statecodeLabel'] = $mrt->getStatecodeLabel();
            $record['func'] = $mrt->mrtId;

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * @View(pageHeader:"捷運站建檔",pageDesc:"建檔表單",render:"ruleCA")
     *
     * @Jslibrary("formelms","formvalid", "maps")
     * */
    public function formAction()
    {
        if( $this->params[0] == 'reset' && $this->session->has('create-mrtId') ){
            $this->session->remove('create-mrtId');
            $this->session->remove('edit-mrtId');
        }

        $mrt = null;
        if( $this->params[0] == 'edit' ){
            $this->session->set('edit-mrtId', $this->params[1]);
            $mrt = \Houserich\Models\Mrt::findFirst( $this->params[1] );
        }elseif( $this->session->has('create-mrtId') ){
            $mrt = \Houserich\Models\Mrt::findFirst( $this->session->get('create-mrtId') );
        }elseif( $this->session->has('edit-mrtId')  ){
            $this->session->remove('create-mrtId');
            $mrt = \Houserich\Models\Mrt::findFirst( $this->session->get('edit-mrtId') );
        }
        $this->view->form = new \House\Houserich\Forms\MrtForm($mrt);
    }


    /**
     * 捷運站資料新增與修改
     * */
    public function saveAction()
    {
        $item = new \Houserich\Models\Mrt();
        $form = new \House\Houserich\Forms\MrtForm();
        $form->bind($_POST, $item);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
        }else{

            try{
                if( !$item->save() ){
                    $msg = array('寫入捷運資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "msg"=>implode("\n", array_merge($msg, $item->getMessages() )) ]);
                }else{
                    // mrtId
                    $this->session->set('create-mrtId', $item->mrtId);

                    echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=> "已完成編輯捷運站紀錄", "PrimaryID"=>"#mrtId", "id"=>$item->mrtId]);
                }
            } catch (\PDOException $e) {

            } catch (\Exception $e) {
                $msg = array('寫入捷運資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
            }

        }

        return false;
    }

    /**
     * 直接刪除作法
     * */
    public function deleteAction()
    {
        if( !intval($this->params[1]) ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未指定刪除記錄編號，無法刪除！"]);
            return false;
        }

        $item = \Houserich\Models\Mrt::findFirst( $this->params[1] );
        if( !$item ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"未找到對應捷運站紀錄，無法刪除！"]);
            return false;
        }elseif( !$item->delete() ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"刪除記錄編號發生錯誤，訊息：".implode(',', $item->getMessages())]);
            return false;
        }else{
            echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"已刪除指定捷運站紀錄。"]);
            return false;
        }
    }
}
