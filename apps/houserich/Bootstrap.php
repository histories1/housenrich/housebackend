<?php
namespace House\Houserich;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Bootstrap implements ModuleDefinitionInterface
{
    /**
     * Registers the module auto-loader
     */
    public function registerAutoloaders(\Phalcon\DiInterface $di = null)
    {
        if( $di->has('autoloader') ){
            $loader = $di->get('autoloader');
        }else{
            $loader = new Loader();
        }

        $namespace = $loader->getNamespaces();
        //module's controller / form /
        $loader->registerNamespaces(array_merge($namespace, array(
            'House\Houserich\Controllers'  => __DIR__ .'/controllers/',
            'House\Houserich\Forms'        => __DIR__ .'/forms/',
            'House\Core\Forms'             => __DIR__ .'/../core/forms/',
        )));

        $loader->register();
    }


    /**
     * Registers the module-only services
     *
     * @param Phalcon\DI $di
     */
    public function registerServices(\Phalcon\DiInterface $di = null)
    {
        if( $di->has('dispatcher') ){
            $dispatcher = $di->get('dispatcher');
        }else{
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
        }
        $dispatcher->setDefaultNamespace('\House\Houserich\Controllers');

        /**
         * Update the view component
         */
        $v = $di->get('view');
        $path = $v->getViewsDir();
        // $path[] = __DIR__ . '/views/';
        $v->setViewsDir( $path );
        $di->setShared('view' , $v);
    }
}
