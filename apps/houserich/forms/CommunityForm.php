<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class CommunityForm extends \Personalwork\Forms\Form
{
	// 公共設施 from Model\Fieldoptions
	var $publicFacility;

	/**
	 * @Comment("社區名稱")
	 */
	private function _Name() {
		$element = new \Personalwork\Forms\Elements\Text("name");
		$element->setLabel("社區名稱");
		$element->setAttributes(array(
					"class" => "form-control",
					"required" => "required"
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"社區名稱欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("建設公司")
	 */
	private function _Company() {
		$element = new \Personalwork\Forms\Elements\Text("company");
		$element->setLabel("建設公司");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("公共設施")
	 */
	private function _Publicfacility() {
		$element = new \House\Forms\ElmCheckboxInline("publicFacility");

		$publicFacility = \Houserich\Models\Fieldoptions::findByFieldname("公共設施");
		$items = array();
		foreach ($publicFacility->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		// 修改時配置預設
		if( $this->getEntity() ){
			$element->setDefault( explode(",",$this->getEntity()->publicFacility) );
		}

		$element->setLabel("公共設施")
				->setAttributes(array(
					"name"	=> "publicFacility[]",
					"class" => "valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"checkbox-class" => "checkbox checkbox-inline",
					"format" => "Checks"
				));
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"公共設施欄位必須至少設定一項。"
		]));
		return $element;
	}

	/**
	 * @Comment("建材")
	 */
	private function _Meterials() {
		$element = new \House\Forms\ElmRadioInline("meterials");

		$meterials = \Houserich\Models\Fieldoptions::findByFieldname("建材");
		$items = array();
		foreach ($meterials->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		$element->setLabel("建材")
				->setAttributes(array(
					"class" => "valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"radio-class" => "radio radio-inline",
					"format" => "Radios"
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}


	/**
	 * @Comment("地址")
	 */
	private function _Address() {
		$element = new \Personalwork\Forms\Elements\Text("address");
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}


	/**
	 * @Comment("地址(縣市)")
	 */
	private function _Addresscity() {
		$element = new \Personalwork\Forms\Elements\Select("addressCity");
		$element->setAttributes(array(
					"class" => "form-control depend-city",
					"data-target" => "#addressDistrict",
				));
		$this->address_city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($this->address_city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(行政區)")
	 */
	private function _Addressdistrict() {
		$element = new \Personalwork\Forms\Elements\Select("addressDistrict");
		$element->setAttributes(array(
					"class" => "hide form-control",
				));

		$this->address_district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($this->address_district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(路)")
	 */
	private function _Addressroad() {
		$element = new \Personalwork\Forms\Elements\Text("addressRoad");
		$element->setAttributes(array(
					"class" => "form-control input-lgw",
					"required" => "required",
					"placeholder"=> "路/段/道(含段)名"
				));
		$element->addValidator(new StringLength([
			"max" => 20,
			"message"=>"地址路名欄位長度超過15字元限制。"
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"地址路名欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(巷)")
	 */
	private function _Addresslane() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressLane");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("巷");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
			"notRequired" => true,
		    "message"=>"地址巷欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(弄)")
	 */
	private function _Addressalley() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressAlley");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("弄");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
			"notRequired" => true,
		    "message"=>"地址弄欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號)")
	 */
	private function _Addressno() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNo");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("號");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
			"notRequired" => true,
		    "message"=>"地址號欄位必須為數值格式。"
		]));
		return $element;
	}


	/**
	 * @Comment("完成日期")
	 */
	private function _Completion() {
		$element = new \Personalwork\Forms\Elements\Text("completion");
		$element->setLabel("完成日期")
				->setAttributes(array(
					"class" => "form-control input-smw completion-dtp",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "control-label"
				));
		$element->addValidator(new PresenceOf([
			"message"=>"完成日期欄位必須設定。"
		]));
		return $element;
	}


	/**
	 * @Comment("屋齡(年)")
	 */
	private function _Houseageyear() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseAgeYear");
		$element->setLabel("屋齡");
		$element->setAttributes(array(
					"class" => "form-control input-smw houseage-year",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(年)"
				));
		return $element;
	}

	/**
	 * @Comment("屋齡(月)")
	 */
	private function _Houseagemonth() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseAgeMonth");
		$element->setAttributes(array(
					"class" => "form-control input-xsw houseage-month",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(月)"
				));
		return $element;
	}

	/**
	 * @Comment("總層數（地上）")
	 */
	private function _Floorabove() {
		$element = new \Personalwork\Forms\Elements\Numeric("floorAbove");
		$element->setLabel("總層數");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(地上)"
				));
		return $element;
	}

	/**
	 * @Comment("總層數（地下）")
	 */
	private function _Floorbelow() {
		$element = new \Personalwork\Forms\Elements\Numeric("floorBelow");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(地下)"
				));
		return $element;
	}

	/**
	 * @Comment("路寬(m)")
	 */
	private function _Roadwidth() {
		$element = new \Personalwork\Forms\Elements\Numeric("roadWidth");
		$element->setLabel("路寬");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(m)"
				));
		return $element;
	}

	/**
	 * @Comment("電梯數量")
	 */
	private function _Numelevators() {
		$element = new \Personalwork\Forms\Elements\Numeric("numElevators");
		$element->setLabel("電梯數量")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("1樓現況")
	 */
	private function _Onefloor() {
		$element = new \Personalwork\Forms\Elements\Select("onefloor");
		$element->setLabel("1樓現況");
		$element->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$this->onefloor = \Houserich\Models\Fieldoptions::findByFieldname("1F現況");
		$items = array(''=>'請選擇');
		foreach($this->onefloor as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		return $element;
	}

	/**
	 * @Comment("基地面積")
	 */
	private function _Basearrea() {
		$element = new \Personalwork\Forms\Elements\Numeric("baseArea");
		$element->setLabel("基地面積")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(m<sup>2</sup>)"
				));
		return $element;
	}

	/**
	 * @Comment("棟數")
	 */
	private function _Numbuildings() {
		$element = new \Personalwork\Forms\Elements\Numeric("numBuildings");
		$element->setLabel("")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "棟數"
				));
		return $element;
	}

	/**
	 * @Comment("戶數")
	 */
	private function _Numhousehold() {
		$element = new \Personalwork\Forms\Elements\Numeric("numHousehold");
		$element->setLabel("")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "戶數"
				));
		return $element;
	}

	/**
	 * @Comment("簡介")
	 */
	private function _Description() {
		$element = new \Personalwork\Forms\Elements\TextArea("description");
		$element->setLabel("簡介");
		$element->setAttributes(array(
					"class" => "form-control wysihtml",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}


	/**
	 * 針對公共設施欄位在驗證前先轉換成字串！
	 * */
	public function beforeValidation()
    {
        // $_POST['publicFacility'] = implode(",",$_POST['publicFacility']);
        // return true;
        // $this->get('publicFacility')->setDefault( implode(",",$_POST['publicFacility']) );
    }


	/**
	 * 針對公共設施欄位在驗證後恢復成陣列！
	 * */
    public function afterValidation()
    {
        $_POST['publicFacility'] = explode(",",$_POST['publicFacility']);
    }


	public function initialize() {
		// 直接附加communityId
		$id = new \Phalcon\Forms\Element\Hidden("communityId");
		// $id->setDefault(1);
		$this->add($id);

		$this->add($this->_Name());
		$this->add($this->_Company());
		$this->add($this->_Publicfacility());
		$this->add($this->_Meterials());
		$this->add($this->_Address());
		$this->add($this->_Addresscity());
		$this->add($this->_Addressdistrict());
		$this->add($this->_Addressroad());
		$this->add($this->_Addresslane());
		$this->add($this->_Addressalley());
		$this->add($this->_Addressno());
		// $this->add($this->_Addressnoex());
		// $this->add($this->_Addressfloor());
		// $this->add($this->_Addressfloorex());
		$this->add($this->_Completion());
		$this->add($this->_Houseageyear());
		$this->add($this->_Houseagemonth());
		$this->add($this->_Floorabove());
		$this->add($this->_Floorbelow());
		$this->add($this->_Roadwidth());
		$this->add($this->_Numelevators());
		$this->add($this->_Onefloor());
		$this->add($this->_Basearrea());
		$this->add($this->_Numbuildings());
		$this->add($this->_Numhousehold());
		$this->add($this->_Description());
	}
}
