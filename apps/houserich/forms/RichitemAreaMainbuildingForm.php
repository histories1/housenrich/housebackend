<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemAreaMainbuildingForm extends \Personalwork\Forms\Form
{
	var $group_element_count = 0;

	// 編輯列表列數量
	var $exist_elements = 0;

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _Richitemid() {
		$element = new \Phalcon\Forms\Element\Hidden("RichitemId");
		// 直接配置物件編號
		$element->setDefault( $this->session->get('edit-richitemId') );
		// $element->setDefault( 1 );
		$element->setAttributes(["name" => "RichitemId[{$this->group_element_count}]"]);

		return $element;
	}

	/**
	 * @Comment("層次")
	 */
	private function _Layer() {
		$element = new \Personalwork\Forms\Elements\Text("layer");
		$element->setLabel("層次")
				->setAttributes(array(
					"class" => "form-control input-xsw",
					"name"	=> "layer[{$this->group_element_count}]"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("面積(平方公尺)")
	 */
	private function _Aream() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaM");
		$element->setLabel("面積")
				->setAttributes(array(
					"class" => "form-control input-smw areaM",
					"name"	=> "areaM[{$this->group_element_count}]",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					"postfix-label"=> "(m<sup>2</sup>)",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("面積(坪)")
	 */
	private function _Area() {
		$element = new \Personalwork\Forms\Elements\Numeric("area");
		$element->setAttributes(array(
					"class" => "form-control input-smw area",
					"name"	=> "area[{$this->group_element_count}]",
					"readonly" => "readonly",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					"postfix-label" => "(坪)",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}


	/**
	 * 根據對應Model處理編輯欄位預先顯示作法！
	 * */
	public function renderEdits() {
		$RichitemId = $this->_Richitemid()->getValue();
		$items = \Houserich\Models\RichitemAreaMainbuilding::findByRichitemId( $RichitemId );
		if( count($items) == 0 ){ return ; }

		$this->html = null;
		foreach ($items as $c => $main) {

			// 設定
			$this->setGroupElementCounts($c);

			$this->html .= '<aside style="position: relative;">'.PHP_EOL;

			$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

			$e = $this->get('rmbaId')
					  ->setDefault($main->rmbaId)
					  ->setAttribute('name', "rmbaId[{$this->group_element_count}]");
			$this->html .= "\t\t".$e->render().PHP_EOL;

			$this->html .= "\t\t".$this->_Richitemid()->setDefault($main->RichitemId)->render().PHP_EOL;

			$this->html .= "\t\t".$this->_Layer()->setDefault($main->layer)->render().PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Aream()->setDefault($main->areaM)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Area()->setDefault($main->area)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;

	        $this->html .= "\t".'</section>'.PHP_EOL;
	        $this->html .= "\t".'<hr class="inner-separator">';

	        $this->html .= '<button type="button" data-remote="'.$this->getDI()->get('url')->get('houserich/richitem/itemExtends/delete-RichitemAreaMainbuilding-'.$main->rmbaId).'" data-ID="'.$main->rmbaId.'" class="btn btn-danger btn-ajaxdelete-area">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}

		$this->exist_elements = $c+1;

		return $this->html;
	}


	public function renderCreate($withRemove=false) {
		$this->html = null;

		// 預設產生編輯列表時配置列數
		if( $this->exist_elements ){
			// 若列表已有紀錄，為了表單驗證失敗，故不需顯示預設新增表單！
			return ;
			$this->setGroupElementCounts($this->exist_elements);
		}

		if( $withRemove ){
			$this->html .= '<aside style="position: relative;">'.PHP_EOL;
		}

		$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

		$this->html .= "\t\t".$this->get('rmbaId')
								   ->setAttribute('name', "rmbaId[{$this->group_element_count}]")
								   ->setDefault(null)
								   ->render().PHP_EOL;
		$this->html .= "\t\t".$this->_Richitemid()->render().PHP_EOL;

		$this->html .= "\t\t".$this->_Layer()->render().PHP_EOL;

		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Aream()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;

		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Area()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;

        $this->html .= "\t".'</section>'.PHP_EOL;
        $this->html .= "\t".'<hr class="inner-separator">';

        if( $withRemove ){
			$this->html .= '<button type="button" class="btn btn-danger btn-remove-self">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}

		$this->exist_elements = $c+1;

        return $this->html;
	}

	public function setGroupElementCounts($count) {
		$this->group_element_count = $count;
	}

	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("rmbaId");
		$id->setAttribute('name', "rmbaId[{$this->group_element_count}]");
		$this->add($id);

		$this->add($this->_Richitemid());
		$this->add($this->_Layer());
		$this->add($this->_Aream());
		$this->add($this->_Area());
	}
}
