<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class MrtForm extends \Personalwork\Forms\Form
{

	/**
	 * @Comment("站名")
	 */
	private function _Name() {
		$element = new \Personalwork\Forms\Elements\Text("name");
		$element->setLabel("站名");
		$element->setAttributes(array(
					"class" => "form-control",
					"required" => "required"
				));
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"站名欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("站名別名")
	 */
	private function _Namealias() {
		$element = new \Personalwork\Forms\Elements\Text("nameAlias");
		$element->setLabel("站名別名");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("路線名稱")
	 */
	private function _Linelabel() {
		$element = new \Personalwork\Forms\Elements\Text("lineLabel");
		$element->setLabel("路線名稱");
		$element->setAttributes(array(
					"class" => "form-control",
					"required" => "required"
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"路線名稱欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("路線代碼")
	 */
	private function _Lineid() {
		$element = new \Personalwork\Forms\Elements\Text("lineId");
		$element->setLabel("路線代碼");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("縣市")
	 */
	private function _City() {
		$element = new \Personalwork\Forms\Elements\Select("city");
		$element->setLabel("縣市");
		$element->setAttributes(array(
					"class" => "form-control depend-city",
					"data-target" => "#district",
					"required" => "required"
				));
		$this->address_city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($this->address_city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"縣市欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("行政區")
	 */
	private function _District() {
		$element = new \Personalwork\Forms\Elements\Select("district");
		$element->setLabel("行政區");
		$element->setAttributes(array(
					"class" => "hide form-control",
					"required" => "required"
				));
		$this->address_district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($this->address_district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"行政區欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("出入口名稱")
	 */
	private function _Exitname() {
		$element = new \Personalwork\Forms\Elements\Text("exitName");
		$element->setLabel("出入口名稱");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("出入口編號")
	 */
	private function _Exitno() {
		$element = new \Personalwork\Forms\Elements\Numeric("exitNo");
		$element->setLabel("出入口編號");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		return $element;
	}

	/**
	 * @Comment("通車狀態")
	 */
	private function _Statecode() {
		$element = new \Personalwork\Forms\Elements\Select("statecode");
		$element->setLabel("通車狀態")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setOptions(array(
						"未通車", "已通車"
					));
		return $element;
	}

	public function initialize() {
		// 直接附加mrtId
		$id = new \Phalcon\Forms\Element\Hidden("mrtId");
		// $id->setDefault(1);
		$this->add($id);

		$this->add($this->_Name());
		$this->add($this->_Namealias());
		$this->add($this->_Linelabel());
		$this->add($this->_Lineid());
		$this->add($this->_City());
		$this->add($this->_District());
		$this->add($this->_Exitname());
		$this->add($this->_Exitno());
		$this->add($this->_Statecode());
	}
}
