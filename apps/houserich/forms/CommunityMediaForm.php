<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class CommunityMediaForm extends \Personalwork\Forms\Form
{

	/**
	 * @Comment("關聯社區大樓編號")
	 */
	private function _Communityid() {
		$element = new \Personalwork\Forms\Elements\Select("CommunityId");
		$element->setLabel("關聯社區大樓編號");
		$element->setOptions([]);
		return $element;
	}

	/**
	 * @Comment("媒體類型")
	 */
	private function _Mime() {
		$element = new \Personalwork\Forms\Elements\Text("mime");
		$element->setLabel("媒體類型");
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("檔案儲存路徑")
	 */
	private function _Filepath() {
		$element = new \Personalwork\Forms\Elements\Text("filepath");
		$element->setLabel("檔案儲存路徑");
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		return $element;
	}

	/**
	 * @Comment("網址路徑")
	 */
	private function _Urlpath() {
		$element = new \Personalwork\Forms\Elements\Text("urlpath");
		$element->setLabel("網址路徑");
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		return $element;
	}

	/**
	 * @Comment("標題")
	 */
	private function _Title() {
		$element = new \Personalwork\Forms\Elements\Text("title");
		$element->setLabel("標題");
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("縮圖路徑")
	 */
	private function _Thumbnail() {
		$element = new \Personalwork\Forms\Elements\Text("thumbnail");
		$element->setLabel("縮圖路徑");
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		return $element;
	}

	/**
	 * @Comment("建檔時間")
	 */
	private function _Settime() {
		$element = new \Personalwork\Forms\Elements\Numeric("setTime");
		$element->setLabel("建檔時間");
		return $element;
	}

	/**
	 * @Comment("狀態碼")
	 */
	private function _Statecode() {
		$element = new \Phalcon\Forms\Element\Check("statecode");
		$element->setLabel("狀態碼");
		return $element;
	}

	public function initialize() {
		$this->add($this->_Communityid());
		$this->add($this->_Mime());
		$this->add($this->_Filepath());
		$this->add($this->_Urlpath());
		$this->add($this->_Title());
		$this->add($this->_Thumbnail());
		$this->add($this->_Settime());
		$this->add($this->_Statecode());
	}
}
