<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class SchoolForm extends \Personalwork\Forms\Form
{
	// 縣市 from Model\Fieldoptions
	var $address_city;

	// 行政區 from Model\Fieldoptions
	var $address_district;

	/**
	 * @Comment("學校")
	 */
	private function _Name() {
		$element = new \Personalwork\Forms\Elements\Text("name");
		$element->setLabel("學校名稱");
		$element->setAttributes(array(
					"class" => "form-control input-lgw",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "control-label"
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"學校欄位必須填寫。"
		]));
		return $element;
	}


	/**
	 * @Comment("完整地址")
	 */
	private function _Address() {
		$element = new \Personalwork\Forms\Elements\Text("address");
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("地址(縣市)")
	 */
	private function _Addresscity() {
		$element = new \Personalwork\Forms\Elements\Select("addressCity");
		$element->setAttributes(array(
					"class" => "form-control depend-city",
					"data-target" => "#addressDistrict",
				));
		$this->address_city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($this->address_city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(行政區)")
	 */
	private function _Addressdistrict() {
		$element = new \Personalwork\Forms\Elements\Select("addressDistrict");
		$element->setAttributes(array(
					"class" => "hide form-control",
				));

		$this->address_district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($this->address_district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(路)")
	 */
	private function _Addressroad() {
		$element = new \Personalwork\Forms\Elements\Text("addressRoad");
		$element->setAttributes(array(
					"class" => "form-control input-lgw",
					"placeholder"=> "路/段/道(含段)名"
				));
		$element->addValidator(new StringLength([
			"max" => 20,
			"message"=>"地址路名欄位長度超過15字元限制。"
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"地址路名欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(巷)")
	 */
	private function _Addresslane() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressLane");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("巷");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址巷欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(弄)")
	 */
	private function _Addressalley() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressAlley");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("弄");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址弄欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號)")
	 */
	private function _Addressno() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNo");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("號");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號之)")
	 */
	private function _Addressnoex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNoEx");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓)")
	 */
	private function _Addressfloor() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloor");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("樓");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓之)")
	 */
	private function _Addressfloorex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloorEx");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("經度")
	 */
	private function _Latitude() {
		$element = new \Personalwork\Forms\Elements\Text("latitude");
		$element->setLabel("經度")
				->setAttributes(array(
					"class" => "form-control",
					"readonly" => true,
				));
		return $element;
	}

	/**
	 * @Comment("緯度")
	 */
	private function _Longitude() {
		$element = new \Personalwork\Forms\Elements\Text("longitude");
		$element->setLabel("緯度")
				->setAttributes(array(
					"class" => "form-control",
					"readonly" => true,
				));
		return $element;
	}

	public function initialize() {
		// 直接附加schoolId
		$id = new \Phalcon\Forms\Element\Hidden("schoolId");
		// $id->setDefault(1);
		$this->add($id);

		$this->add($this->_Name());
		$this->add($this->_Address());
		$this->add($this->_Addresscity());
		$this->add($this->_Addressdistrict());
		$this->add($this->_Addressroad());
		$this->add($this->_Addresslane());
		$this->add($this->_Addressalley());
		$this->add($this->_Addressno());
		// $this->add($this->_Addressnoex());
		// $this->add($this->_Addressfloor());
		// $this->add($this->_Addressfloorex());
		$this->add($this->_Latitude());
		$this->add($this->_Longitude());
	}
}
