<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemInformationForm extends \Personalwork\Forms\Form
{

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _Richitemid() {
		$element = new \Phalcon\Forms\Element\Hidden("RichitemId");
		// 直接配置物件編號
		$element->setDefault( $this->session->get('edit-richitemId') );
		return $element;
	}

	/**
	 * @Comment("土地總面積(坪)")
	 */
	private function _Arealand() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaLand");
		$element->setLabel("土地")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("主建物總面積(坪)")
	 */
	private function _Areamainbuilding() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaMainbuilding");
		$element->setLabel("主建物")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("附屬建物總面積(坪)")
	 */
	private function _Areaappendbuilding() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaAppendbuilding");
		$element->setLabel("附屬建物")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("共有部分總面積(坪)")
	 */
	private function _Areasharepart() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaSharepart");
		$element->setLabel("共有部分")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("車位總面積(坪)")
	 */
	private function _Areaparking() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaParking");
		$element->setLabel("車位")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("權狀總面積(坪)")
	 */
	private function _Arearight() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaRight");
		$element->setLabel("權狀")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("使用總面積(坪)")
	 */
	private function _Areausing() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaUsing");
		$element->setLabel("使用")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
				));
		return $element;
	}

	/**
	 * @Comment("公設比(%)")
	 */
	private function _Ratiopublicarea() {
		$element = new \Personalwork\Forms\Elements\Numeric("ratioPublicArea");
		$element->setLabel("公設比")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "%",
				));
		return $element;
	}

	/**
	 * @Comment("總價(萬元)")
	 */
	private function _Pricerichitem() {
		$element = new \Personalwork\Forms\Elements\Numeric("priceRichitem");
		$element->setLabel("物件總價")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(萬元)",
				));
		return $element;
	}

	/**
	 * @Comment("車位總價(萬元)")
	 */
	private function _Priceparking() {
		$element = new \Personalwork\Forms\Elements\Numeric("priceParking");
		$element->setLabel("車位總價")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(萬元)",
				));
		return $element;
	}

	/**
	 * @Comment("單價(萬元/坪)")
	 */
	private function _Pricesingle() {
		$element = new \Personalwork\Forms\Elements\Numeric("priceSingle");
		$element->setLabel("單價")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(萬元/坪)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋格局(房)")
	 */
	private function _Houseroom() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseRoom");
		$element->setLabel("房屋格局")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(房)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋格局(廳)")
	 */
	private function _Househall() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseHall");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(廳)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋格局(衛)")
	 */
	private function _Housetoilt() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseToilt");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(衛)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋格局(室)")
	 */
	private function _Houserooms() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseRoomS");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(室)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋格局(陽台)")
	 */
	private function _Housebalcony() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseBalcony");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(陽台)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋現況")
	 */
	private function _Housesituation() {
		$element = new \Personalwork\Forms\Elements\Select("houseSituation");
		$element->setLabel("房屋現況")
				->setAttributes(array(
					"class" => "form-control situation",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$opts = array(
					[
						"label"=>"請選擇",
						"value"=>'',
					],
					[
						"label"=>"自住",
						"value"=>'自住',
						"data-hide"=>".rentblock-house",
						"data-show"=>""
					],
					[
						"label"=>"空屋",
						"value"=>'空屋',
						"data-hide"=>".rentblock-house",
						"data-show"=>""
					],
					[
						"label"=>"出租",
						"value"=>'出租',
						"data-hide"=>"",
						"data-show"=>".rentblock-house"
					],
				);
		$element->setOptions($opts);

		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("房屋租金(元)")
	 */
	private function _Houserent() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseRent");
		$element->setLabel("租金")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)"
				));
		return $element;
	}

	/**
	 * @Comment("房屋租約到期日")
	 */
	private function _Houserentenddate() {
		$element = new \Personalwork\Forms\Elements\Date("houseRentEnddate");
		$element->setLabel("租約到期日")
				->setAttributes(array(
					"class" => "form-control datepicker input-lgw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("加蓋格局(房)")
	 */
	private function _Addonroom() {
		$element = new \Personalwork\Forms\Elements\Numeric("addonRoom");
		$element->setLabel("加蓋格局")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(房)",
				));
		return $element;
	}

	/**
	 * @Comment("加蓋格局(廳)")
	 */
	private function _Addonhall() {
		$element = new \Personalwork\Forms\Elements\Numeric("addonHall");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(廳)",
				));
		return $element;
	}

	/**
	 * @Comment("加蓋格局(衛)")
	 */
	private function _Addontoilt() {
		$element = new \Personalwork\Forms\Elements\Numeric("addonToilt");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(衛)",
				));
		return $element;
	}

	/**
	 * @Comment("加蓋格局(室)")
	 */
	private function _Addonrooms() {
		$element = new \Personalwork\Forms\Elements\Numeric("addonRoomS");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(室)",
				));
		return $element;
	}

	/**
	 * @Comment("加蓋格局(陽台)")
	 */
	private function _Addonbalcony() {
		$element = new \Personalwork\Forms\Elements\Numeric("addonBalcony");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(陽台)",
				));
		return $element;
	}

	/**
	 * @Comment("加蓋現況")
	 */
	private function _Addonsituation() {
		$element = new \Personalwork\Forms\Elements\Select("addonSituation");
		$element->setLabel("加蓋現況")
				->setAttributes(array(
					"class" => "form-control situation",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$opts = array(
					[
						"label"=>"請選擇",
						"value"=>'',
					],
					[
						"label"=>"自住",
						"value"=>'自住',
						"data-hide"=>".rentblock-addon",
						"data-show"=>""
					],
					[
						"label"=>"空屋",
						"value"=>'空屋',
						"data-hide"=>".rentblock-addon",
						"data-show"=>""
					],
					[
						"label"=>"出租",
						"value"=>'出租',
						"data-hide"=>"",
						"data-show"=>".rentblock-addon"
					],
				);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("加蓋租金(元)")
	 */
	private function _Addonrent() {
		$element = new \Personalwork\Forms\Elements\Numeric("addonRent");
		$element->setLabel("租金(元)")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)"
				));
		return $element;
	}

	/**
	 * @Comment("加蓋租約到期日")
	 */
	private function _Addonrentenddate() {
		$element = new \Personalwork\Forms\Elements\Date("addonRentEnddate");
		$element->setLabel("租約到期日")
				->setAttributes(array(
					"class" => "form-control datepicker input-lgw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("地下室格局(房)")
	 */
	private function _Basementroom() {
		$element = new \Personalwork\Forms\Elements\Numeric("basementRoom");
		$element->setLabel("地下室格局")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(房)",
				));
		return $element;
	}

	/**
	 * @Comment("地下室格局(廳)")
	 */
	private function _Basementhall() {
		$element = new \Personalwork\Forms\Elements\Numeric("basementHall");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(廳)",
				));
		return $element;
	}

	/**
	 * @Comment("地下室格局(衛)")
	 */
	private function _Basementtoilt() {
		$element = new \Personalwork\Forms\Elements\Numeric("basementToilt");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(衛)",
				));
		return $element;
	}

	/**
	 * @Comment("地下室格局(室)")
	 */
	private function _Basementrooms() {
		$element = new \Personalwork\Forms\Elements\Numeric("basementRoomS");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(室)",
				));
		return $element;
	}

	/**
	 * @Comment("地下室格局(陽台)")
	 */
	private function _Basementbalcony() {
		$element = new \Personalwork\Forms\Elements\Numeric("basementBalcony");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(陽台)",
				));
		return $element;
	}

	/**
	 * @Comment("車位現況")
	 */
	private function _Parkingsituation() {
		$element = new \Personalwork\Forms\Elements\Select("parkingSituation");
		$element->setLabel("車位現況")
				->setAttributes(array(
					"class" => "form-control situation",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$opts = array(
					[
						"label"=>"請選擇",
						"value"=>'',
					],
					[
						"label"=>"自住",
						"value"=>'自住',
						"data-hide"=>".rentblock-parking",
						"data-show"=>""
					],
					[
						"label"=>"空屋",
						"value"=>'空屋',
						"data-hide"=>".rentblock-parking",
						"data-show"=>""
					],
					[
						"label"=>"出租",
						"value"=>'出租',
						"data-hide"=>"",
						"data-show"=>".rentblock-parking"
					],
				);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("車位租金(元)")
	 */
	private function _Parkingrent() {
		$element = new \Personalwork\Forms\Elements\Numeric("parkingRent");
		$element->setLabel("租金")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(元)"
				));;
		return $element;
	}

	/**
	 * @Comment("車位租約到期日")
	 */
	private function _Parkingrentenddate() {
		$element = new \Personalwork\Forms\Elements\Date("parkingRentEnddate");
		$element->setLabel("租約到期日")
				->setAttributes(array(
					"class" => "form-control datepicker input-lgw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));;
		return $element;
	}

	/**
	 * @Comment("房屋投資報酬率(%)")
	 */
	private function _Housereturninvest() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseReturnInvest");
		$element->setLabel("房屋投資報酬率")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(%)",
				));
		return $element;
	}

	/**
	 * @Comment("含加蓋投資報酬率(%)")
	 */
	private function _Withaddonreturninvest() {
		$element = new \Personalwork\Forms\Elements\Numeric("withaddonReturnInvest");
		$element->setLabel("含加蓋投資報酬率")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(%)",
				));
		return $element;
	}

	/**
	 * @Comment("採光")
	 */
	private function _Lighting() {
		$element = new \Personalwork\Forms\Elements\Select("lighting");
		$element->setLabel("採光")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$opts = array(
					""=>"請選擇",
					"1"=>"1",
					"2"=>"2",
					"3"=>"3",
					"4"=>"4"
				);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("廚房瓦斯")
	 */
	private function _Gaskitchen() {
		$element = new \Personalwork\Forms\Elements\Select("gasKitchen");
		$element->setLabel("廚房瓦斯")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));

		$opts = array(
					""=>"請選擇",
					"天然氣" => "天然氣",
					"桶裝瓦斯" => "桶裝瓦斯",
					"電熱" => "電熱",
					);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("浴室瓦斯")
	 */
	private function _Gasbathroom() {
		$element = new \Personalwork\Forms\Elements\Select("gasBathroom");
		$element->setLabel("浴室瓦斯")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));

		$opts = array(
					""=>"請選擇",
					"天然氣" => "天然氣",
					"桶裝瓦斯" => "桶裝瓦斯",
					"電熱" => "電熱",
					);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("管理費(元)")
	 */
	private function _Managefee() {
		$element = new \Personalwork\Forms\Elements\Numeric("manageFee");
		$element->setLabel("管理費")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)",
				));
		return $element;
	}

	/**
	 * @Comment("其他")
	 */
	private function _Others() {
		$element = new \House\Forms\ElmCheckboxInline("others");

		$others = \Houserich\Models\Fieldoptions::findByFieldname("延伸其他");
		$items = array();
		foreach ($others->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}

		$element->setLabel("其他")
				->setAttributes(array(
					"class" => "form-control",
					"items" => $items
				))
				->setUserOptions(array(
					"checkbox-class" => "checkbox checkbox-inline",
					"format" => "Checks"
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("大樓朝向")
	 */
	private function _Facetobuilding() {
		$element = new \Personalwork\Forms\Elements\Select("facetoBuilding");

		$others = \Houserich\Models\Fieldoptions::findByFieldname("大樓朝向");
		$items = array();
		foreach ($others->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}

		$element->setLabel("大樓朝向")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				))
				->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("大門朝向")
	 */
	private function _Facetodoor() {
		$element = new \Personalwork\Forms\Elements\Select("facetoDoor");

		$others = \Houserich\Models\Fieldoptions::findByFieldname("大門朝向");
		$items = array();
		foreach ($others->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}

		$element->setLabel("大門朝向")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				))
				->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("客廳窗朝向")
	 */
	private function _Facetowindow() {
		$element = new \Personalwork\Forms\Elements\Select("facetoWindow");

		$others = \Houserich\Models\Fieldoptions::findByFieldname("客廳窗朝向");
		$items = array();
		foreach ($others->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}

		$element->setLabel("客廳窗朝向")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				))
				->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("房屋現值(元)")
	 */
	private function _Housevalue() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseValue");
		$element->setLabel("房屋現值")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)",
				));
		return $element;
	}

	/**
	 * @Comment("房屋稅(元)")
	 */
	private function _Taxhouse() {
		$element = new \Personalwork\Forms\Elements\Numeric("taxHouse");
		$element->setLabel("房屋稅")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)",
				));
		return $element;
	}

	/**
	 * @Comment("申報地價(元)")
	 */
	private function _Landvalue() {
		$element = new \Personalwork\Forms\Elements\Numeric("landValue");
		$element->setLabel("申報地價")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元/m<sup>2</sup>)",
				));
		return $element;
	}

	/**
	 * @Comment("地價稅(元)")
	 */
	private function _Taxland() {
		$element = new \Personalwork\Forms\Elements\Numeric("taxLand");
		$element->setLabel("地價稅")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)",
				));
		return $element;
	}

	/**
	 * @Comment("土地增值稅(元)")
	 */
	private function _Taxlandadd() {
		$element = new \Personalwork\Forms\Elements\Numeric("taxLandAdd");
		$element->setLabel("土地增值稅")
				->setAttributes(array(
					"class" => "form-control input-smw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)",
				));
		return $element;
	}

	/**
	 * @Comment("契稅")
	 */
	private function _Taxcontract() {
		$element = new \Personalwork\Forms\Elements\Numeric("taxContract");
		$element->setLabel("契稅")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(元)",
				));
		return $element;
	}

	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("riId");
		$this->add($id);

		$this->add($this->_Richitemid());
		$this->add($this->_Arealand());
		$this->add($this->_Areamainbuilding());
		$this->add($this->_Areaappendbuilding());
		$this->add($this->_Areasharepart());
		$this->add($this->_Areaparking());
		$this->add($this->_Arearight());
		$this->add($this->_Areausing());
		$this->add($this->_Ratiopublicarea());
		$this->add($this->_Pricerichitem());
		$this->add($this->_Priceparking());
		$this->add($this->_Pricesingle());
		$this->add($this->_Housetoilt());
		$this->add($this->_Househall());
		$this->add($this->_Houseroom());
		$this->add($this->_Houserooms());
		$this->add($this->_Housebalcony());
		$this->add($this->_Addonroom());
		$this->add($this->_Addonhall());
		$this->add($this->_Addontoilt());
		$this->add($this->_Addonrooms());
		$this->add($this->_Addonbalcony());
		$this->add($this->_Basementroom());
		$this->add($this->_Basementhall());
		$this->add($this->_Basementtoilt());
		$this->add($this->_Basementrooms());
		$this->add($this->_Basementbalcony());
		$this->add($this->_Lighting());
		$this->add($this->_Gaskitchen());
		$this->add($this->_Gasbathroom());
		$this->add($this->_Managefee());
		$this->add($this->_Others());
		$this->add($this->_Housesituation());
		$this->add($this->_Houserent());
		$this->add($this->_Houserentenddate());
		$this->add($this->_Addonsituation());
		$this->add($this->_Addonrent());
		$this->add($this->_Addonrentenddate());
		$this->add($this->_Parkingsituation());
		$this->add($this->_Parkingrent());
		$this->add($this->_Parkingrentenddate());
		$this->add($this->_Housereturninvest());
		$this->add($this->_Withaddonreturninvest());
		$this->add($this->_Facetobuilding());
		$this->add($this->_Facetodoor());
		$this->add($this->_Facetowindow());
		$this->add($this->_Housevalue());
		$this->add($this->_Taxhouse());
		$this->add($this->_Landvalue());
		$this->add($this->_Taxland());
		$this->add($this->_Taxlandadd());
		$this->add($this->_Taxcontract());
	}
}
