<?php

namespace House\Houserich\Forms;

use Phalcon\Tag as Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Numericality;

class RichitemForm extends \Personalwork\Forms\Form
{

	// 用途 from Model\Fieldoptions
	var $item_usefor;

	// 型態 from Model\Fieldoptions
	var $item_type;

	// 縣市 from Model\Fieldoptions
	var $address_city;

	// 行政區 from Model\Fieldoptions
	var $address_district;

	// 1F現況 from Model\Fieldoptions
	var $onefloor;

	// 法定用途 from Model\Fieldoptions
	var $statutory_usefor;

	/**
	 * @Comment("案件編號")
	 *
	 */
	private function _Richitemno() {
		$element = new \Phalcon\Forms\Element\Hidden("richitemNo");
		$element->setLabel("案件編號");

		if( $this->getDI()->get('session')->get('richitemNo') ){
			$no = $this->getDI()->get('session')->get('richitemNo');
		}else{
			$no = date('ymd').'H'.substr(time(), 3);
		}
		$element->setDefault( $no );
		$element->addValidator(new StringLength([
			"max" => 16,
			"message"=>"案件編號欄位長度超過16字元限制。"
		]));
		return $element;
	}

	/**
	 * @Comment("標題")
	 */
	private function _Title() {
		$element = new \Personalwork\Forms\Elements\Text("title");
		$element->setLabel("標題")
				->setAttributes(array(
					"class" => "form-control",
					"required" => 1
				));
		$element->addValidator(new StringLength([
			"max" => 60,
			"message"=>"標題欄位長度超過60字元限制。"
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"物件標題欄位必須填寫。"
		]));

		return $element;
	}

	/**
	 * @Comment("用途")
	 */
	private function _Usefor() {
		$element = new \House\Forms\ElmRadioInline("usefor");

		$usefor = \Houserich\Models\Fieldoptions::findByFieldname("用途");
		$items = array();
		foreach ($usefor->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		$element->setLabel('用途')
				->setAttributes(array(
					"class" => "depend-radio valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"radio-class" => "radio radio-inline",
					"format" => "Radios",
					"depend-other-class" => "form-control input-smw"
				));

		return $element;
	}

	/**
	 * @Comment("型態")
	 */
	private function _Type() {
		$element = new \Personalwork\Forms\Elements\Select("type");
		$element->setLabel("型態")
				->setAttributes(array(
					"class" => "form-control usefor-type",
					"disabled" => "true",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$this->item_type = \Houserich\Models\Fieldoptions::findByFieldname("型態");
		$items = array(''=>'請選擇');
		foreach($this->item_type as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("完整地址")
	 */
	private function _Address() {
		$element = new \Personalwork\Forms\Elements\Text("address");
		// $element->setLabel("Address");
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("地址(縣市)")
	 */
	private function _Addresscity() {
		$element = new \Personalwork\Forms\Elements\Select("addressCity");
		$element->setAttributes(array(
					"class" => "form-control depend-city",
					"data-target" => "#addressDistrict",
				));
		$this->address_city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($this->address_city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(行政區)")
	 */
	private function _Addressdistrict() {
		$element = new \Personalwork\Forms\Elements\Select("addressDistrict");
		$element->setAttributes(array(
					"class" => "hide form-control",
				));

		$this->address_district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($this->address_district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(路)")
	 */
	private function _Addressroad() {
		$element = new \Personalwork\Forms\Elements\Text("addressRoad");
		$element->setAttributes(array(
					"class" => "form-control input-lgw",
					"placeholder"=> "路/段/道(含段)名"
				));
		$element->addValidator(new StringLength([
			"max" => 20,
			"message"=>"地址路名欄位長度超過15字元限制。"
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"地址路名欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(巷)")
	 */
	private function _Addresslane() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressLane");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("巷");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址巷欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(弄)")
	 */
	private function _Addressalley() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressAlley");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("弄");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址弄欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號)")
	 */
	private function _Addressno() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNo");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("號");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號之)")
	 */
	private function _Addressnoex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNoEx");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓)")
	 */
	private function _Addressfloor() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloor");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("樓");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓之)")
	 */
	private function _Addressfloorex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloorEx");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("委託人姓名")
	 */
	private function _Clientname() {
		$element = new \Personalwork\Forms\Elements\Text("clientName");
		$element->setLabel("委託人姓名");
		$element->setAttributes(array(
					"class" => "form-control",
					"required" => "required"
				))
				->setUserOptions(array(
					"format"=> "kingadmin"
				));
		$element->addValidator(new StringLength([
			"max" => 30,
			"message"=>"委託人姓名欄位長度超過15字元限制。"
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"委託人姓名欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("委託人電話")
	 */
	private function _Clientphone() {
		$element = new \Personalwork\Forms\Elements\Text("clientPhone");
		$element->setLabel("委託人電話");
		$element->setAttributes(array(
					"class" => "form-control valid-phonenum",
					"required" => "required"
				));
		$element->addValidator(new StringLength([
			"max" => 15,
			"message"=>"委託人電話欄位長度超過15字元限制。"
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"委託人電話欄位必須填寫。"
		]));
		$element->addValidator(new Regex([
		     // 'pattern' => '/^[0][1-9]{1,3}[-][0-9]{6,8}[#,0-9]{0,4}$/',
			'pattern' => '/^(\(?0\d{1,2}\)?)?\-?\d{3,4}\-?\d{4}([#|\*]?\d+)?$/',
		     'message' => "委託人電話欄位格式錯誤"
		 ]));
		return $element;
	}

	/**
	 * @Comment("委託人身分證字號")
	 */
	private function _Clientid() {
		$element = new \Personalwork\Forms\Elements\Text("clientID");
		$element->setLabel("委託人身分證字號");
		$element->setAttributes(array(
					"class" => "form-control",
					"data-inputmask" => "'mask': 'a999999999'"
				));
		$element->addFilter('emptytonull');
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		$element->addValidator(new Regex([
		     'pattern' => '/^[A-Z]{1}[0-9]{9}$/',
		     'message' => "委託人身分證字號欄位格式錯誤"
		]));
		return $element;
	}

	/**
	 * @Comment("委託人生日")
	 */
	private function _Clientbirthday() {
		$element = new \Personalwork\Forms\Elements\Text("clientBirthday");
		$element->setLabel("委託人生日");
		$element->setAttributes(array(
					"class" => "form-control",
					"data-inputmask" => "'alias': 'yyyy-mm-dd'"
				));
		$element->addFilter('emptytonull');
		$element->addValidator(new Regex([
		     'pattern' => '/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/',
		     'message' => "委託人生日欄位格式錯誤"
		]));
		return $element;
	}


	/**
	 * @Comment("關聯社區大樓編號")
	 */
	private function _Communityid() {
		$element = new \Phalcon\Forms\Element\Hidden("CommunityId");
		return $element;
	}

	/**
	 * @Comment("社區名稱")
	 */
	private function _Communityname() {
		$element = new \Personalwork\Forms\Elements\Text("CommunityName");
		$element->setLabel("社區名稱");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("建設公司")
	 */
	private function _Company() {
		$element = new \Personalwork\Forms\Elements\Text("company");
		$element->setLabel("建設公司");
		$element->setAttributes(array(
					"class" => "form-control",
				));
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("公共設施")
	 */
	private function _Publicfacility() {
		$element = new \House\Forms\ElmCheckboxInline("publicFacility");

		$publicFacility = \Houserich\Models\Fieldoptions::findByFieldname("公共設施");
		$items = array();
		foreach ($publicFacility->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		// 修改時配置預設
		if( $this->getEntity() ){
			$element->setDefault( explode(",",$this->getEntity()->publicFacility) );
		}

		$element->setLabel("公共設施")
				->setAttributes(array(
					"name"	=> "publicFacility[]",
					// "class" => "valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"checkbox-class" => "checkbox checkbox-inline",
					"format" => "Checks"
				));
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		// $element->addValidator(new PresenceOf([
		// 	"message"=>"公共設施欄位必須至少設定一項。"
		// ]));
		return $element;
	}

	/**
	 * @Comment("建材")
	 */
	private function _Meterials() {
		$element = new \House\Forms\ElmRadioInline("meterials");

		$meterials = \Houserich\Models\Fieldoptions::findByFieldname("建材");
		$items = array();
		foreach ($meterials->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		$element->setLabel("建材")
				->setAttributes(array(
					// "class" => "valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"label-class" => "radio radio-inline",
					"format" => "Radios"
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("完成日期")
	 */
	private function _Completion() {
		$element = new \Personalwork\Forms\Elements\Text("completion");
		$element->setLabel("完成日期")
				->setAttributes(array(
					"class" => "form-control input-smw completion-dtp",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "control-label"
				));
		$element->addValidator(new PresenceOf([
			"message"=>"完成日期欄位必須設定。"
		]));
		return $element;
	}

	/**
	 * @Comment("屋齡(年)")
	 */
	private function _Houseageyear() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseAgeYear");
		$element->setLabel("屋齡");
		$element->setAttributes(array(
					"class" => "form-control input-smw houseage-year",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(年)"
				));
		return $element;
	}

	/**
	 * @Comment("屋齡(月)")
	 */
	private function _Houseagemonth() {
		$element = new \Personalwork\Forms\Elements\Numeric("houseAgeMonth");
		$element->setAttributes(array(
					"class" => "form-control input-xsw houseage-month",
					"readonly" => "readonly"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(月)"
				));
		return $element;
	}

	/**
	 * @Comment("總層數（地上）")
	 */
	private function _Floorabove() {
		$element = new \Personalwork\Forms\Elements\Numeric("floorAbove");
		$element->setLabel("總層數");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(地上)"
				));
		return $element;
	}

	/**
	 * @Comment("總層數（地下）")
	 */
	private function _Floorbelow() {
		$element = new \Personalwork\Forms\Elements\Numeric("floorBelow");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(地下)"
				));
		return $element;
	}

	/**
	 * @Comment("路寬(m)")
	 */
	private function _Roadwidth() {
		$element = new \Personalwork\Forms\Elements\Numeric("roadWidth");
		$element->setLabel("路寬");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(m)"
				));
		return $element;
	}

	/**
	 * @Comment("電梯數量")
	 */
	private function _Numelevators() {
		$element = new \Personalwork\Forms\Elements\Numeric("numElevators");
		$element->setLabel("電梯數量")
				->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("1樓現況")
	 */
	private function _Onefloor() {
		$element = new \Personalwork\Forms\Elements\Select("onefloor");
		$element->setLabel("1樓現況");
		$element->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$this->onefloor = \Houserich\Models\Fieldoptions::findByFieldname("1F現況");
		$items = array(''=>'請選擇');
		foreach($this->onefloor as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		return $element;
	}

	/**
	 * @Comment("法定用途")
	 */
	private function _Useforstatutory() {
		$element = new \House\Forms\ElmRadioInline("useforStatutory");
		$element->setLabel("法定用途");

		$useforStatutory = \Houserich\Models\Fieldoptions::findByFieldname("法定用途");
		$items = array();
		foreach ($useforStatutory->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		$element->setAttributes(array(
					"class" => "valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"radio-class" => "radio radio-inline",
					"format" => "Radios",
					"depend-other-class" => "form-control input-smw"
				));

		$element->addValidator(new StringLength([
			"max" => 10
		]));

		return $element;
	}

	/**
	 * @Comment("出售層數")
	 */
	private function _Salefloor() {
		$element = new \Personalwork\Forms\Elements\Text("saleFloor");
		$element->setLabel("出售層數");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("該層戶數")
	 */
	private function _Salehouseholdnum() {
		$element = new \Personalwork\Forms\Elements\Numeric("saleHouseholdNum");
		$element->setLabel("該層戶數");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("該層高度(m)")
	 */
	private function _Salefloorhigh() {
		$element = new \Personalwork\Forms\Elements\Numeric("saleFloorHigh");
		$element->setLabel("該層高度(m)");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("物件介紹(內文)")
	 */
	private function _Description() {
		$element = new \Personalwork\Forms\Elements\TextArea("description");
		$element->setLabel("內文");
		$element->setAttributes(array(
					"class" => "form-control wysihtml",
				));
		$element->addValidator(new PresenceOf([
			"message"=>"內文欄位必須填寫。"
		]));
		return $element;
	}


	/**
	 * 1. 直接根據每次重新整理產生時間戳記進行編號設定
	 * 2. 格式; 2位年月日H7碼流水號
	 * */
	public function renderRichitemno(){
		//指定物件編號
		$element = $this->get('richitemNo');
		$html  = '<b class="form-control-static text-success">'.$element->getDefault().'</b>';
		$html .= $element->render();

		return $html;
	}


	/**
	 * 針對公共設施欄位在驗證後恢復成陣列！
	 * */
    public function afterValidation()
    {
        $_POST['publicFacility'] = explode(",",$_POST['publicFacility']);
    }


    /**
     * 判斷是否從社區大樓載入部分欄位值做為預設值
     * */
    private function _load_communitys() {
    	if( $this->getDI()->get('session')->has('richitem-community') ){
    		$c = $this->getDI()->get('session')->get('richitem-community');
    		if( !is_array($c) ){ return; }

    		foreach ($c as $key => $value) {
    			switch ($key) {
					case 'baseArea':
					case 'numHousehold':
					case 'numBuildings':
						break;
    				case 'communityId':
    				$this->get('CommunityId')->setDefault($value);
    					break;
    				case 'name':
    				$this->get('CommunityName')->setDefault($value);
    					break;
    				default:
    				$this->get($key)->setDefault($value);
    					break;
    			}
    		}
    	}
    }


	public function initialize()
	{
		// 直接附加richitemId
		$id = new \Phalcon\Forms\Element\Hidden("richitemId");
		// $id->setDefault(1);
		$this->add($id);

		$this->add($this->_Richitemno());
		$this->add($this->_Title());
		$this->add($this->_Usefor());
		$this->add($this->_Type());
		$this->add($this->_Clientname());
		$this->add($this->_Clientphone());
		$this->add($this->_Clientid());
		$this->add($this->_Clientbirthday());
		$this->add($this->_Address());
		$this->add($this->_Addresscity());
		$this->add($this->_Addressdistrict());
		$this->add($this->_Addressroad());
		$this->add($this->_Addresslane());
		$this->add($this->_Addressalley());
		$this->add($this->_Addressno());
		$this->add($this->_Addressnoex());
		$this->add($this->_Addressfloor());
		$this->add($this->_Addressfloorex());
		$this->add($this->_Communityid());
		$this->add($this->_Communityname());
		$this->add($this->_Company());
		$this->add($this->_Publicfacility());
		$this->add($this->_Meterials());
		$this->add($this->_Completion());
		$this->add($this->_Houseageyear());
		$this->add($this->_Houseagemonth());
		$this->add($this->_Floorabove());
		$this->add($this->_Floorbelow());
		$this->add($this->_Roadwidth());
		$this->add($this->_Numelevators());
		$this->add($this->_Onefloor());
		$this->add($this->_Useforstatutory());
		$this->add($this->_Salefloor());
		$this->add($this->_Salehouseholdnum());
		$this->add($this->_Salefloorhigh());
		$this->add($this->_Description());

		$this->_load_communitys();
	}
}

