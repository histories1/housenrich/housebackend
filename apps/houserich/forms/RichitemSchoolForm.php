<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemSchoolForm extends \Personalwork\Forms\Form
{
	var $html = '';

	var $richitem;

	var $level;

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _RichitemId() {
		$element = new \Phalcon\Forms\Element\Hidden("RichitemId");
		$element->setDefault($this->richitem->richitemId);
		return $element;
	}

	/**
	 * @Comment("學區(校名)")
	 */
	private function _SchoolId() {
		$element = new \Personalwork\Forms\Elements\Select("SchoolId");
		$element->setLabel("設定鄰近{$this->level}")
				->setAttributes(array(
					"class" => "form-control multiselect-filter valid-checkreq",
				))
				->setUserOptions(array(
					"label-class" => "control-label"
				));

		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("級別(國中/國小)")
	 */
	private function _Level() {
		$element = new \Phalcon\Forms\Element\Hidden("level");
		$element->setLabel("級別(國中/國小)")
				->setDefault($this->level);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("是否為預設學區")
	 */
	private function _Default() {
		$element = new \Phalcon\Forms\Element\Hidden("default");
		$element->setDefault(1);
		return $element;
	}



	public function renderF()
	{
		$this->html = '';
		$this->html .= $this->get('rsId')->render().PHP_EOL;
		$this->html .= $this->_RichitemId()->render().PHP_EOL;
		$this->html .= $this->_Level()->render().PHP_EOL;
		$this->html .= $this->_Default()->render().PHP_EOL;

		$element = $this->_SchoolId();
		$schools = \Houserich\Models\School::find("name like '%{$this->level}%' AND addressCity='{$this->richitem->addressCity}'");
		$opt = array(''=>'請選擇');
		foreach($schools as $school) {
			$opt[$school->schoolId] = $school->name;
		}
		if( $this->getEntity() ){
			$element->setDefault( $this->getEntity()->SchoolId );
		}
		$element->setOptions($opt);

		$this->html .= $element->render().PHP_EOL;
		$this->html .= "\t".'<button type="button" class="btn btn-primary btn-ajaxsubmit">設定</button>'.PHP_EOL;

		return $this->html;
	}


	public function setRichitem($richitem){
		$this->richitem = $richitem;
		return $this;
	}

	public function setLevel($level){
		$this->level = $level;
		return $this;
	}


	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("rsId");
		$this->add($id);

		$this->add($this->_RichitemId());
		$this->add($this->_SchoolId());
		$this->add($this->_Level());
		$this->add($this->_Default());
	}
}
