<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemAreaParkingForm extends \Personalwork\Forms\Form
{
	var $group_element_count = 0;

	// 編輯列表列數量
	var $exist_elements = 0;

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _Richitemid() {
		$element = new \Phalcon\Forms\Element\Hidden("RichitemId");
		// 直接配置物件編號
		$element->setDefault( $this->session->get('edit-richitemId') );
		// $element->setDefault( 1 );
		$element->setAttributes(["name" => "RichitemId[{$this->group_element_count}]"]);

		return $element;
	}

	/**
	 * @Comment("車位樓層")
	 */
	private function _Floor() {
		$element = new \Personalwork\Forms\Elements\Text("floor");
		$element->setLabel("車位樓層")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"name"	=> "floor[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("車位位置")
	 */
	private function _Position() {
		$element = new \Personalwork\Forms\Elements\Text("position");
		$element->setLabel("車位位置")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"name"	=> "position[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("產權")
	 */
	private function _Property() {
		$element = new \Personalwork\Forms\Elements\Select("property");
		$element->setLabel("產權")
				->setAttributes(array(
					"class" => "form-control property-select",
					"name"	=> "property[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));

		$opts = array(
					[
						"label"=>"請選擇",
						"value"=>'',
					],
					[
						"label"=>"無",
						"value"=>'無',
						"data-hide"=>".propertyYLaw-block,.propertyYRight-block",
						"data-show"=>".propertyN-block"
					],
					[
						"label"=>"有",
						"value"=>'有',
						"data-hide"=>".propertyN-block",
						"data-show"=>".propertyYLaw-block,.propertyYRight-block"
					],
				);
		$element->setOptions($opts);

		$element->addValidator(new StringLength([
			"max" => 2
		]));
		return $element;
	}

	/**
	 * @Comment("產權無")
	 */
	private function _Propertyn() {
		$element = new \House\Forms\ElmRadioInline("propertyN");

		$propertyN = \Houserich\Models\Fieldoptions::findByFieldname("產權（無）");
		$items = array();
		foreach ($propertyN->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}

		$element->setAttributes(array(
					"name"	=> "propertyN[{$this->group_element_count}]",
					"items" => $items,
				))
				->setUserOptions(array(
					"grid-class" => 'properties propertyN-block hide',
					"radio-class" => "radio radio-inline",
					"format" => "Radios",
					"depend-other-class" => "form-control input-smw",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("產權有(法律)")
	 */
	private function _Propertyylaw() {
		$element = new \House\Forms\ElmRadioInline("propertyYLaw");

		$propertyYLaw = \Houserich\Models\Fieldoptions::findByFieldname("產權（有）法律");
		$items = array();
		foreach ($propertyYLaw->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		$element->setLabel("法律")
				->setAttributes(array(
					"name"	=> "propertyYLaw[{$this->group_element_count}]",
					"items" => $items
				))
				->setUserOptions(array(
					"grid-class" => 'properties propertyYLaw-block hide',
					"label-class" => "control-label sub-lebel",
					"radio-class" => "radio radio-inline",
					"format" => "Radios",
					"depend-other-class" => "form-control input-smw",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("產權有(權狀)")
	 */
	private function _Propertyyright() {
		$element = new \House\Forms\ElmRadioInline("propertyYRight");

		$propertyYRight = \Houserich\Models\Fieldoptions::findByFieldname("產權（有）權狀");
		$items = array();
		foreach ($propertyYRight->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}

		$element->setLabel("權狀")
				->setAttributes(array(
					"name"	=> "propertyYRight[{$this->group_element_count}]",
					"items" => $items
				))
				->setUserOptions(array(
					"grid-class" => 'properties propertyYRight-block hide',
					"label-class" => "control-label sub-lebel",
					"radio-class" => "radio radio-inline",
					"format" => "Radios",
					"depend-other-class" => "form-control input-smw",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("種類")
	 */
	private function _Type() {
		$element = new \Personalwork\Forms\Elements\Select("type");
		$element->setLabel("種類");

		$type = \Houserich\Models\Fieldoptions::findByFieldname("車位種類");
		$items = array();
		foreach ($type->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}

		$element->setAttributes(array(
					"class" => "form-control",
					"name"	=> "type[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				))
				->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 5
		]));

		return $element;
	}

	/**
	 * @Comment("長(cm)")
	 */
	private function _Long() {
		$element = new \Personalwork\Forms\Elements\Numeric("long");
		$element->setLabel("長(cm)");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
					"name"	=> "long[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("寬(cm)")
	 */
	private function _Width() {
		$element = new \Personalwork\Forms\Elements\Numeric("width");
		$element->setLabel("寬(cm)");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
					"name"	=> "width[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("高(cm)")
	 */
	private function _Height() {
		$element = new \Personalwork\Forms\Elements\Numeric("height");
		$element->setLabel("高(cm)");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
					"name"	=> "height[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("載重(kg)")
	 */
	private function _Weight() {
		$element = new \Personalwork\Forms\Elements\Numeric("weight");
		$element->setLabel("載重(kg)");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
					"name"	=> "weight[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("面積(平方公尺)")
	 */
	private function _Aream() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaM");
		$element->setLabel("面積")
				->setAttributes(array(
					"class" => "form-control input-smw areaM",
					"name"	=> "areaM[{$this->group_element_count}]",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					"postfix-label"=> "(m<sup>2</sup>)",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("面積(坪)")
	 */
	private function _Area() {
		$element = new \Personalwork\Forms\Elements\Numeric("area");
		$element->setAttributes(array(
					"class" => "form-control input-smw area",
					"name"	=> "area[{$this->group_element_count}]",
					"readonly" => "readonly",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					"postfix-label" => "(坪)",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("權力範圍(分子)")
	 */
	private function _Scopeauthoritymolecular() {
		$element = new \Personalwork\Forms\Elements\Numeric("scopeAuthorityMolecular");
		$element->setLabel("權力範圍")
				->setAttributes(array(
					"class" => "form-control input-xsw areaMolecular",
					"name"	=> "scopeAuthorityMolecular[{$this->group_element_count}]",
				))->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("權力範圍(分母)")
	 */
	private function _Scopeauthoritydenominator() {
		$element = new \Personalwork\Forms\Elements\Numeric("scopeAuthorityDenominator");
		$element->setLabel("／")
				->setAttributes(array(
					"class" => "form-control input-smw areaDenominator",
					"name"	=> "scopeAuthorityDenominator[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}


	/**
	 * 根據對應Model處理編輯欄位預先顯示作法！
	 * */
	public function renderEdits() {
		$RichitemId = $this->_Richitemid()->getValue();
		$items = \Houserich\Models\RichitemAreaParking::findByRichitemId( $RichitemId );
		if( count($items) == 0 ){ return ; }

		$this->html = null;
		foreach ($items as $c => $main) {

			// 設定
			$this->setGroupElementCounts($c);

			$this->html .= '<aside style="position: relative;">'.PHP_EOL;

			$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

			$this->html .= "\t\t".$this->get('riparkingId')->setDefault($main->riparkingId)->render().PHP_EOL;
			$this->html .= "\t\t".$this->_Richitemid()->setDefault($main->RichitemId)->render().PHP_EOL;

			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Type()->setDefault($main->type)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Floor()->setDefault($main->floor)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Position()->setDefault($main->position)->render().
			PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'</section>'.PHP_EOL;

			$this->html .= "\t".'<BR/><section class="form-group inline-group">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Property()->setDefault($main->property)->render().PHP_EOL;

			$this->html .= "\t<div class='properties propertyN-block hide'>".PHP_EOL;
			$this->html .= "\t\t".$this->_Propertyn()->setDefault($main->propertyN)->render().PHP_EOL;
			$this->html .= "\t</div>".PHP_EOL;
			$this->html .= "\t<div class='properties propertyYLaw-block hide'>".PHP_EOL;
			// $this->html .= "\t\t<span class='control-label'>法律</span>".PHP_EOL;
			$this->html .= "\t\t".$this->_Propertyylaw()->setDefault($main->propertyYLaw)->render().PHP_EOL;
			$this->html .= "\t</div>".PHP_EOL;
			$this->html .= "\t<div class='properties propertyYRight-block hide'>".PHP_EOL;
			// $this->html .= "\t\t<span class='control-label'>權狀</span>".PHP_EOL;
			$this->html .= "\t\t".$this->_Propertyyright()->setDefault($main->propertyYRight)->render().PHP_EOL;
			$this->html .= "\t</div>".PHP_EOL;
			$this->html .= "\t".'</section>'.PHP_EOL;

			$this->html .= "\t".'<BR/><section class="form-group inline-group">'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Long()->setDefault($main->long)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Width()->setDefault($main->width)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Height()->setDefault($main->height)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t\t".$this->_Weight()->setDefault($main->weight)->render().PHP_EOL;
			$this->html .= "\t".'</section>'.PHP_EOL;

			$this->html .= "\t".'<BR/><section class="form-group inline-group">'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Aream()->setDefault($main->areaM)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t\t".$this->_Scopeauthoritymolecular()->setDefault($main->scopeAuthorityMolecular)->render().$this->_Scopeauthoritydenominator()->setDefault($main->scopeAuthorityDenominator)->render().PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Area()->setDefault($main->area)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
	        $this->html .= "\t".'</section>'.PHP_EOL;

	        $this->html .= "\t".'<hr class="inner-separator">';

	        $this->html .= '<button type="button" data-remote="'.$this->getDI()->get('url')->get('houserich/richitem/itemExtends/delete-RichitemAreaParking-'.$main->riparkingId).'" data-ID="'.$main->riparkingId.'" class="btn btn-danger btn-ajaxdelete-area">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}

		$this->exist_elements = $c+1;

		return $this->html;
	}


	public function renderCreate($withRemove=false) {
		$this->html = null;

		// 預設產生編輯列表時配置列數
		if( $this->exist_elements ){
			// 若列表已有紀錄，為了表單驗證失敗，故不需顯示預設新增表單！
			return ;
			$this->setGroupElementCounts($this->exist_elements);
		}

		if( $withRemove ){
			$this->html .= '<aside style="position: relative;">'.PHP_EOL;
		}

		$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

		$this->html .= "\t\t".$this->get('riparkingId')
									->setAttribute('name', "riparkingId[{$this->group_element_count}]")
								    ->setDefault(null)
								    ->render().PHP_EOL;
		$this->html .= "\t\t".$this->_Richitemid()->render().PHP_EOL;

		$this->html .= "\t\t".$this->_Type()->render().PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Floor()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Position()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t".'</section>'.PHP_EOL;

		$this->html .= "\t".'<BR/><section class="form-group inline-group">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Property()->render().PHP_EOL;

		$this->html .= $this->renderGroups(["propertyN"],
                        					"\\House\\Forms\\Decorators\\GridElement");

		$this->html .= $this->renderGroups(["propertyYLaw"],
                        					"\\House\\Forms\\Decorators\\GridElement");

		$this->html .= $this->renderGroups(["propertyYRight"],
                        					"\\House\\Forms\\Decorators\\GridElement");

		$this->html .= "\t".'</section>'.PHP_EOL;

		$this->html .= "\t".'<BR/><section class="form-group inline-group">'.PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Long()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Width()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Height()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t\t".$this->_Weight()->render().PHP_EOL;
		$this->html .= "\t".'</section>'.PHP_EOL;

		$this->html .= "\t".'<BR/><section class="form-group inline-group">'.PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Aream()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t\t".$this->_Scopeauthoritymolecular()->render().$this->_Scopeauthoritydenominator()->render().PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Area()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
        $this->html .= "\t".'</section>'.PHP_EOL;
        $this->html .= "\t".'<hr class="inner-separator">';

        if( $withRemove ){
			$this->html .= '<button type="button" class="btn btn-danger btn-remove-self">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}


        return $this->html;
	}

	public function setGroupElementCounts($count) {
		$this->group_element_count = $count;
	}

	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("riparkingId");
		$id->setAttribute('name', "riparkingId[{$this->group_element_count}]");
		$this->add($id);

		$this->add($this->_Richitemid());
		$this->add($this->_Floor());
		$this->add($this->_Position());
		$this->add($this->_Property());
		$this->add($this->_Propertyn());
		$this->add($this->_Propertyylaw());
		$this->add($this->_Propertyyright());
		$this->add($this->_Type());
		$this->add($this->_Long());
		$this->add($this->_Width());
		$this->add($this->_Height());
		$this->add($this->_Weight());
		$this->add($this->_Aream());
		$this->add($this->_Area());
		$this->add($this->_Scopeauthoritymolecular());
		$this->add($this->_Scopeauthoritydenominator());
	}
}
