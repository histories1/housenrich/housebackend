<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemViewhouseForm extends \Personalwork\Forms\Form
{
	var $group_element_count = 0;

	// 編輯列表列數量
	var $exist_elements = 0;

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _Richitemid() {
		$element = new \Phalcon\Forms\Element\Hidden("RichitemId");
		// 直接配置物件編號
		$element->setDefault( $this->session->get('edit-richitemId') );
		// $element->setDefault( 1 );
		$element->setAttributes(["name" => "RichitemId[{$this->group_element_count}]"]);

		return $element;
	}

	/**
	 * @Comment("可看屋日")
	 */
	private function _Bookweekday() {
		$element = new \Personalwork\Forms\Elements\Select("bookweekday");
		$element->setLabel("可看屋日")
				->setAttributes(array(
					"class" => "form-control",
					"name"	=> "bookweekday[{$this->group_element_count}]",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));
		$opts = array(
					"" => "請選擇",
					"週一" => "週一",
					"週二" => "週二",
					"週三" => "週三",
					"週四" => "週四",
					"週五" => "週五",
					"週六" => "週六",
					"週日" => "週日",
				);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 2
		]));
		return $element;
	}

	/**
	 * @Comment("可看屋時間")
	 */
	private function _Booktime() {
		$element = new \Personalwork\Forms\Elements\Select("booktime");
		$element->setLabel("可看屋時段")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"name"	=> "booktime[{$this->group_element_count}]",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));
		$opts = array(
					"" => "請選擇",
					"上午" => "上午",
					"下午" => "下午",
					"晚上" => "晚上",
				);
		$element->setOptions($opts);
		$element->addValidator(new StringLength([
			"max" => 2
		]));
		return $element;
	}

	/**
	 * @Comment("看屋見面地點")
	 */
	private function _Meetplace() {
		$element = new \Personalwork\Forms\Elements\Text("meetplace");
		$element->setLabel("看屋見面地點")
				->setAttributes(array(
					"class" => "form-control",
					"name"	=> "meetplace[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("鑰匙存放位置")
	 */
	private function _Keyat() {
		$element = new \Personalwork\Forms\Elements\Select("keyat");
		$element->setLabel("鑰匙存放位置")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"name"	=> "keyat[{$this->group_element_count}]",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "form-label",
					'groupelements-count' => $this->group_element_count
				));

		$keyat = \Houserich\Models\Fieldoptions::findByFieldname("鑰匙存放處");
		foreach ($keyat->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}


	/**
	 * 根據對應Model處理編輯欄位預先顯示作法！
	 * */
	public function renderEdits() {
		$RichitemId = $this->_Richitemid()->getValue();
		$items = \Houserich\Models\RichitemViewhouse::findByRichitemId( $RichitemId );
		if( count($items) == 0 ){ return ; }

		$this->html = null;
		foreach ($items as $c => $main) {

			// 設定
			$this->setGroupElementCounts($c);

			$this->html .= '<aside style="position: relative;">'.PHP_EOL;

			$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

			$e = $this->get('riviewhouseId')
					  ->setDefault($main->riviewhouseId)
					  ->setAttribute('name', "riviewhouseId[{$this->group_element_count}]");
			$this->html .= "\t\t".$e->render().PHP_EOL;

			$this->html .= "\t\t".$this->_Richitemid()->setDefault($main->RichitemId)->render().PHP_EOL;

			$this->html .= "\t\t".$this->_Bookweekday()->setDefault($main->bookweekday)
								   ->render().PHP_EOL;
			$this->html .= "\t\t".$this->_Booktime()->setDefault($main->booktime)->render().PHP_EOL;
			$this->html .= "\t\t".$this->_Meetplace()->setDefault($main->meetplace)
								   ->render().PHP_EOL;
			$this->html .= "\t\t".$this->_Keyat()->setDefault($main->keyat)->render().PHP_EOL;

	        $this->html .= "\t".'</section>'.PHP_EOL;
	        $this->html .= "\t".'<hr class="inner-separator">';

	        $this->html .= '<button type="button" data-remote="'.$this->getDI()->get('url')->get('houserich/richitem/itemExtends/delete-RichitemViewhouse-'.$main->riviewhouseId).'" data-ID="'.$main->riviewhouseId.'" class="btn btn-danger btn-ajaxdelete-area">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}

		$this->exist_elements = $c+1;

		return $this->html;
	}


	public function renderCreate($withRemove=false) {
		$this->html = null;

		// 預設產生編輯列表時配置列數
		if( $this->exist_elements ){
			// 若列表已有紀錄，為了表單驗證失敗，故不需顯示預設新增表單！
			return ;
			$this->setGroupElementCounts($this->exist_elements);
		}

		if( $withRemove ){
			$this->html .= '<aside style="position: relative;">'.PHP_EOL;
		}

		$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

		$this->html .= "\t\t".$this->get('riviewhouseId')
									->setAttribute('name', "riviewhouseId[{$this->group_element_count}]")
								    ->setDefault(null)
								    ->render().PHP_EOL;
		$this->html .= "\t\t".$this->_Richitemid()->render().PHP_EOL;

		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Bookweekday()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Booktime()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;

		$this->html .= "\t\t".$this->_Meetplace()->render().PHP_EOL;
		$this->html .= "\t\t".$this->_Keyat()->render().PHP_EOL;

        $this->html .= "\t".'</section>'.PHP_EOL;
        $this->html .= "\t".'<hr class="inner-separator">';

        if( $withRemove ){
			$this->html .= '<button type="button" class="btn btn-danger btn-remove-self">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}


        return $this->html;
	}

	public function setGroupElementCounts($count) {
		$this->group_element_count = $count;
	}

	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("riviewhouseId");
		$id->setAttribute('name', "riviewhouseId[{$this->group_element_count}]");
		$this->add($id);

		$this->add($this->_Richitemid());
		$this->add($this->_Bookweekday());
		$this->add($this->_Booktime());
		$this->add($this->_Meetplace());
		$this->add($this->_Keyat());
	}
}
