<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemMediaForm extends \Personalwork\Forms\Form
{

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _Richitemid() {
		$element = new \Personalwork\Forms\Elements\Select("RichitemId");
		$element->setLabel("關聯物件編號");
		$element->setOptions([]);
		return $element;
	}

	/**
	 * @Comment("媒體檔類型:1照片集 2格局圖 3預設圖 4. 影本文件 5. 附件檔案")
	 */
	private function _Type() {
		$element = new \Phalcon\Forms\Element\Check("type");
		$element->setLabel("媒體檔類型:1照片集 2格局圖 3預設圖 4. 影本文件 5. 附件檔案");
		return $element;
	}

	/**
	 * @Comment("檔案格式")
	 */
	private function _Mime() {
		$element = new \Personalwork\Forms\Elements\Text("mime");
		$element->setLabel("檔案格式");
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("完整路徑")
	 */
	private function _Fullpath() {
		$element = new \Personalwork\Forms\Elements\Text("fullpath");
		$element->setLabel("完整路徑");
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		return $element;
	}

	/**
	 * @Comment("網址路徑")
	 */
	private function _Urlpath() {
		$element = new \Personalwork\Forms\Elements\Text("urlpath");
		$element->setLabel("網址路徑");
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		return $element;
	}

	/**
	 * @Comment("排序")
	 */
	private function _Weight() {
		$element = new \Personalwork\Forms\Elements\Numeric("weight");
		$element->setLabel("排序");
		return $element;
	}

	/**
	 * @Comment("標題")
	 */
	private function _Title() {
		$element = new \Personalwork\Forms\Elements\Text("title");
		$element->setLabel("標題");
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("縮圖路徑")
	 */
	private function _Thumbnail() {
		$element = new \Personalwork\Forms\Elements\Text("thumbnail");
		$element->setLabel("縮圖路徑");
		$element->addValidator(new StringLength([
			"max" => 255
		]));
		return $element;
	}

	public function initialize() {
		$this->add($this->_Richitemid());
		$this->add($this->_Type());
		$this->add($this->_Mime());
		$this->add($this->_Fullpath());
		$this->add($this->_Urlpath());
		$this->add($this->_Weight());
		$this->add($this->_Title());
		$this->add($this->_Thumbnail());
	}
}
