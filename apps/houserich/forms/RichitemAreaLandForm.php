<?php

namespace House\Houserich\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class RichitemAreaLandForm extends \Personalwork\Forms\Form
{
	var $group_element_count = 0;

	// 編輯列表列數量
	var $exist_elements = 0;

	/**
	 * @Comment("關聯物件編號")
	 */
	private function _Richitemid() {
		$element = new \Phalcon\Forms\Element\Hidden("RichitemId");
		// 直接配置物件編號
		$element->setDefault( $this->session->get('edit-richitemId') );
		// $element->setDefault( 1 );
		$element->setAttributes(["name" => "RichitemId[{$this->group_element_count}]"]);
		return $element;
	}

	/**
	 * @Comment("使用分區")
	 */
	private function _Landvaation() {
		$element = new \House\Forms\ElmRadioInline("landVaation");

		$r=\Houserich\Models\Richitem::findFirst($this->get('RichitemId')->getValue());

		$landVaation = \Houserich\Models\Fieldoptions::find(["fieldname='土地使用分區' AND parentLabel=:city:", "bind"=>["city"=>$r->addressCity]]);
		$items = array();
		foreach ($landVaation->toArray() as $item) {
			if( $item['label'] != '其他' ){
				$items[$item['label']]=$item['value'];
			}
		}
		$items['其他']='其他';

		$element->setLabel("使用分區")
				->setAttributes(array(
					"name"	=> "landVaation[{$this->group_element_count}]",
					"class" => "valid-checkreq",
					"items" => $items
				))
				->setUserOptions(array(
					"label-class" => "radio radio-inline",
					"format" => "Radios",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("現況")
	 */
	private function _Situation() {
		$element = new \Personalwork\Forms\Elements\Select("situation");
		$element->setLabel("現況");
		$element->setOptions(array(
					"租用" => "租用",
					"佔用" => "佔用",
					"自用" => "自用"
				))
				->setAttributes(array(
					"class" => "form-control",
					"name"	=> "situation[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("面積(平方公尺)")
	 */
	private function _Aream() {
		$element = new \Personalwork\Forms\Elements\Numeric("areaM");
		$element->setLabel("面積")
				->setAttributes(array(
					"class" => "form-control input-smw areaM",
					"name"	=> "areaM[{$this->group_element_count}]",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(m<sup>2</sup>)",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("面積(坪)")
	 */
	private function _Area() {
		$element = new \Personalwork\Forms\Elements\Numeric("area");
		$element->setAttributes(array(
					"class" => "form-control input-smw area",
					"name"	=> "area[{$this->group_element_count}]",
					"readonly" => "readonly",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label" => "(坪)",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("權力範圍(分子)")
	 */
	private function _Scopeauthoritymolecular() {
		$element = new \Personalwork\Forms\Elements\Numeric("scopeAuthorityMolecular");
		$element->setLabel("權力範圍")
				->setAttributes(array(
					"class" => "form-control input-xsw areaMolecular",
					"name"	=> "scopeAuthorityMolecular[{$this->group_element_count}]",
				))->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}

	/**
	 * @Comment("權力範圍(分母)")
	 */
	private function _Scopeauthoritydenominator() {
		$element = new \Personalwork\Forms\Elements\Numeric("scopeAuthorityDenominator");
		$element->setLabel("／")
				->setAttributes(array(
					"class" => "form-control input-smw areaDenominator",
					"name"	=> "scopeAuthorityDenominator[{$this->group_element_count}]",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					'groupelements-count' => $this->group_element_count
				));
		return $element;
	}


	/**
	 * 根據對應Model處理編輯欄位預先顯示作法！
	 * */
	public function renderEdits() {
		$RichitemId = $this->_Richitemid()->getValue();
		$items = \Houserich\Models\RichitemAreaLand::findByRichitemId( $RichitemId );
		if( count($items) == 0 ){ return ; }

		$this->html = null;
		foreach ($items as $c => $main) {

			// 設定
			$this->setGroupElementCounts($c);

			$this->html .= '<aside style="position: relative;">'.PHP_EOL;

			$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;

			$e = $this->get('rlaId')
					  ->setDefault($main->rlaId)
					  ->setAttribute('name', "rlaId[{$this->group_element_count}]");
			$this->html .= "\t\t".$e->render().PHP_EOL;

			$this->html .= "\t\t".$this->_Richitemid()->setDefault($main->RichitemId)->render().PHP_EOL;

			$this->html .= "\t\t".$this->_Landvaation()->setDefault($main->landVaation)->render().PHP_EOL;
			$this->html .= "\t\t".$this->_Situation()->setDefault($main->situation)->render().PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Aream()->setDefault($main->areaM)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;
			$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
			$this->html .= "\t\t".$this->_Area()->setDefault($main->area)->render().PHP_EOL;
			$this->html .= "\t".'</span>'.PHP_EOL;

			$this->html .= "\t\t".$this->_Scopeauthoritymolecular()->setDefault($main->scopeAuthorityMolecular)->render().$this->_Scopeauthoritydenominator()->setDefault($main->scopeAuthorityDenominator)->render().PHP_EOL;

	        $this->html .= "\t".'</section>'.PHP_EOL;
	        $this->html .= "\t".'<hr class="inner-separator">';

	        $this->html .= '<button type="button" data-remote="'.$this->getDI()->get('url')->get('houserich/richitem/itemExtends/delete-RichitemAreaLand-'.$main->rlaId).'" data-ID="'.$main->rlaId.'" class="btn btn-danger btn-ajaxdelete-area">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}

		$this->exist_elements = $c+1;

		return $this->html;
	}


	public function renderCreate($withRemove=false) {
		$this->html = null;

		// 預設產生編輯列表時配置列數
		if( $this->exist_elements ){
			// 若列表已有紀錄，為了表單驗證失敗，故不需顯示預設新增表單！
			return ;
			$this->setGroupElementCounts($this->exist_elements);
		}

		if( $withRemove ){
			$this->html .= '<aside style="position: relative;">'.PHP_EOL;
		}

		$this->html .= "\t".'<section class="form-group inline-group">'.PHP_EOL;
		$this->html .= "\t\t".$this->get('rlaId')
								   ->setAttribute('name', "rlaId[{$this->group_element_count}]")
								   ->setDefault(null)
								   ->render().PHP_EOL;
		$this->html .= "\t\t".$this->_Richitemid()->render().PHP_EOL;

		// 將區塊群組數量設定到元素類別以便render取用！
		$this->html .= "\t\t".$this->_Landvaation()->render().PHP_EOL;
		$this->html .= "\t".'</section>'.PHP_EOL;

		$this->html .= "\t".'<section class="form-group inline-group" style="margin-top:5px;">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Situation()->render().PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Aream()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;

		$this->html .= "\t\t".$this->_Scopeauthoritymolecular()->render().$this->_Scopeauthoritydenominator()->render().PHP_EOL;
		$this->html .= "\t".'<span class="element-inlineblock">'.PHP_EOL;
		$this->html .= "\t\t".$this->_Area()->render().PHP_EOL;
		$this->html .= "\t".'</span>'.PHP_EOL;
        $this->html .= "\t".'</section>'.PHP_EOL;
        $this->html .= "\t".'<hr class="inner-separator">';

        if( $withRemove ){
			$this->html .= '<button type="button" class="btn btn-danger btn-remove-self">刪除</button>'.PHP_EOL;
			$this->html .= '</aside>'.PHP_EOL;
		}


        return $this->html;
	}

	public function setGroupElementCounts($count) {
		$this->group_element_count = $count;
	}

	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("rlaId");
		$id->setAttribute('name', "rlaId[{$this->group_element_count}]");
		$this->add($id);

		$this->add($this->_Richitemid());
		$this->add($this->_Landvaation());
		$this->add($this->_Situation());
		$this->add($this->_Aream());
		$this->add($this->_Area());
		$this->add($this->_Scopeauthoritymolecular());
		$this->add($this->_Scopeauthoritydenominator());
	}
}
