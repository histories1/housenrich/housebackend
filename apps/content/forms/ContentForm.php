<?php

namespace House\Content\Forms;

use Personalwork\Forms\Form;

/**
 * 預設建立內容表單
 */
class ContentForm extends Form
{

    /**
     *
     */
    public function initialize()
    {

        $this   ->setAction( $this->url->get("") )
                ->setEnctype('multipart/form-data')
                ->setMethod('post')
                ->setCssClass('form-horizontal')
                ->setCssId( '' );


        $this->add(
            new \Phalcon\Forms\Element\Text("title", [
                    'label' => '物件標題',
                    'class' => 'form-control',
                ])
            );


        $this->add(new \Personalwork\Forms\Elements\Button('send', array('class' => 'btn btn-lg color-step-edit', 'value'=>'測試')));
    }
}