<?php
/**
 * PromoteController 行銷活動管理
 *
 * */

namespace House\Content\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class PromoteController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"行銷活動",pageDesc:"根據定案行銷活動配置新增欄位。",render:"ruleMAC")
     * */
    public function phaseAction()
    {

    }
}
