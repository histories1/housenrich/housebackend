<?php
/**
 * FaqController 了解好多房管理
 *
 * */

namespace House\Content\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class FaqController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"了解好多房",pageDesc:"預設載入各項問與答列表，可切換固定群組分類按鈕，不附帶查詢功能。",render:"ruleMAC")
     * */
    public function phaseAction()
    {
    }
}
