<?php
/**
 * NewsController 房市管理
 *
 * */

namespace House\Content\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class NewsController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"房市新聞",pageDesc:"預設載入房市新聞列表。",render:"ruleMAC")
     * */
    public function phaseAction()
    {
    }
}
