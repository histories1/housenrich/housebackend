<?php
/**
 * PageController 靜態頁面建立介面
 * 1. 首頁 portal
 * 2. 關於我們 aboutus
 * 3. 服務條款 service
 * 4. 隱私權政策 policy
 *
 * */

namespace House\Content\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @module content
 *
 * @buildresource("hello", "world", 1, 2, 3, false, true)
 *
 * @checkPrivileges(
    resource:Page,
    route="/content/page/*",
    permission={
        allow:[role1, role2]
    }
   )
 * */
class PageController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @checkPrivileges(
        resource:PageBuilder,
        route:"/content/page/builder",
        permission={
            deny:[role2]
        }
       )
     *
     * @View(pageHeader:"首頁下半部編輯器",pageDesc:"載入contentBuilder顯示操作介面。",render:"")
     *
     * @param string 頁面類型[portal|aboutus|service|policy]
     *
     * @param string 內容類型[]
     * */
    public function builderAction()
    {
        // 根據類型載入特定phtml
        $this->view->pick("content/contentbuilder/".$this->params[0]);
        if( !empty($this->params[1]) ){
            $this->view->active = $this->params[1];
            switch($this->params[1])
            {
                case 'aboutus':
                $this->view->pageHeader = '關於我們';
                break;
                case 'policy':
                $this->view->pageHeader = '隱私權政策';
                // $this->view->pageDesc = '';
                break;
                case 'service':
                $this->view->pageHeader = '服務條款';
                // $this->view->pageDesc = '';
                break;
            }
        }

    }



}
