<?php

namespace House\Forms;

use Phalcon\Tag as Tag;

/**
 * 配置好多房專屬表單樣式
 */
class ElmCheckboxInline extends \Personalwork\Forms\Elements\RadioGroup
{

    private function _renderOther($checked=null){

        if( !empty($this->getAttribute('name')) ){
            // 從form class取得群組區塊新增數量
            $count = $this->getUserOption('groupelements-count');

            $otherId = $this->getName().'OTHERS'.$count;
            $name = $this->getAttribute('name');
            $otherName = $this->getName()."others[{$count}]";
        }else{
            $otherId = $this->getName().'OTHERS';
            $name = $this->getName();
            $otherName = $this->getName().'others';
        }

        $html = "\t\t".Tag::tagHtml( 'label',
                                     array(
                                        'for' => $otherId,
                                        'class' => $this->getUserOption('checkbox-class')
                                     ), FALSE, TRUE, TRUE);

        $html .= "\t\t\t".'<input type="checkbox" id="'.$otherId.'" name="'.$name.'" class="'.$this->getAttribute('class').'" value="其他" />';
        $html .= "\t\t\t".'<input type="text" class="'.$this->getUserOption('depend-other-class').'" name="'.$otherName.'"  placeholder="其他" />';
        $html .= "\t\t".Tag::tagHtmlClose('label').PHP_EOL;
        return $html;
    }

    /**
     * 產出可附加其他欄位結構
     * <label for="" class="radio radio-inline">
     *      <input id="" name="" value="" class="" type="radio" />
     * </label>
     * */
    public function renderChecks() {
         // top label
        if( !empty($this->getLabel()) ){
        $this->html .= "\t".Tag::tagHtml( 'label',
                                          array(
                                            'class' => $this->getUserOption('label-class')
                                          ), FALSE, TRUE, TRUE);
        $this->html .= $this->getLabel();
        $this->html .= "\t".Tag::tagHtmlClose('label').PHP_EOL;
        }

        if( count($this->getValue()) == 1 and $this->getValue()[0] == ''){
            $pettern = $this->getValue();
        }elseif( strpos($this->getValue(),',') ){
            $pettern = explode(',', $this->getValue());
        }else{
            $pettern = $this->getValue();
        }

        // 從form class取得群組區塊新增數量
        $count = $this->getUserOption('groupelements-count');
        $attr = array();
        foreach ($this->getAttribute('items') as $label => $value) {
            // 針對多選項欄位處理
            if( is_array($pettern) ){
                $checked = (in_array($value, $pettern)) ? ' checked' : null;
            }elseif( is_string($pettern) ){
                $checked = ( $value==$this->getValue() ) ? ' checked' : null;
            }else{
                $checked = null;
            }

            if( $label == '其他' ){
                $this->html .= $this->_renderOther($checked);
                continue;
            }

            // 調整radio屬性
            $attr['type'] = 'checkbox';
            $attr['id'] = $this->getName().$label.$count;
            $attr['class'] = $this->getAttribute('class');
            if( !empty($this->getAttribute('name')) ){
                $attr['name'] = $this->getAttribute('name');
            }else{
                $attr['name'] = $this->getName();
            }
            $attr['value'] = $value;
            $attr['checked'] = $checked;

            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $this->getName().$label,
                                                    'class' => $this->getUserOption('checkbox-class')
                                                ), FALSE, TRUE, TRUE).PHP_EOL;
            // input:checkbox
            $this->html .= "\t\t\t".Tag::tagHtml( 'input', $attr, FALSE, TRUE, TRUE). $label;

            $this->html .= "\t\t".Tag::tagHtmlClose('label').PHP_EOL;
        }

        return $this->html;
    }
}

?>
