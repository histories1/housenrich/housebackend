<?php
namespace House\People;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Bootstrap implements ModuleDefinitionInterface
{
    /**
     * Registers the module auto-loader
     */
    public function registerAutoloaders(\Phalcon\DiInterface $di = null)
    {
        if( $di->has('autoloader') ){
            $loader = $di->get('autoloader');
        }else{
            $loader = new Loader();
        }

        $namespace = $loader->getNamespaces();
        //module's controller / form /
        $loader->registerNamespaces(array_merge($namespace, array(
            'House\People\Controllers'  => __DIR__ .'/controllers/',
            'House\People\Forms'        => __DIR__ .'/forms/',
        )));

        $loader->register();
    }


    /**
     * Registers the module-only services
     *
     * @param Phalcon\DI $di
     */
    public function registerServices(\Phalcon\DiInterface $di = null)
    {
        if( $di->has('dispatcher') ){
            $dispatcher = $di->get('dispatcher');
        }else{
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
        }
        $dispatcher->setDefaultNamespace('\House\People\Controllers');

        /**
         * Update the view component
         */
        $v = $di->get('view');
        $path = $v->getViewsDir();
        // $path[] = __DIR__ . '/views/';
        $v->setViewsDir( $path );
        $di->setShared('view' , $v);
    }
}
