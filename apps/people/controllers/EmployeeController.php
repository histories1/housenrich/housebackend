<?php
/**
 * EmployeeController 員工管理
 *
 * */

namespace House\People\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class EmployeeController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"員工管理",pageDesc:"類似會員管理介面。",render:"")
     * */
    public function phaseAction()
    {
        $this->view->pick("default/phase");
    }
}
