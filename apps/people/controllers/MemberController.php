<?php
/**
 * MemberController 會員管理
 *
 * */

namespace House\People\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package houserich
 * */
class MemberController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"會員列表",pageDesc:"註冊會員管理作業。",render:"ruleCA")
     *
     * @Jslibrary("datatable","dtyadcf_datepicker")
     * */
    public function phaseAction()
    {
        $this->view->datatable = array(
                                    'thead' => array(
                                                    '編號'    => '5%',
                                                    '會員帳號' => '10%',
                                                    '帳號類型' => '10%',
                                                    '會員全名' => '10%',
                                                    '門號'    => '10%',
                                                    '帳號狀態'   => '10%',
                                                    '註冊時間'    => '15%',
                                                    '操作'    => '10%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "PeopleId",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "account",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "typeLabel",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "fullName",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "cellphone",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "StatecodeLabel",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "initTime",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "PeopleId",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    if( full.lastStatecode < 2 ){
        appendFunc = '<li><a href="{$this->url->get('people/member/setstate/deny-')}'+data+'" class="text-warning">停權</a></li>';
    }else if( full.lastStatecode == 2 ){
        appendFunc = '<li><a href="{$this->url->get('people/member/setstate/allow-')}'+data+'" class="text-success">復權</a></li>';
    }else{
        appendFunc = '';
    }
    return '<div class="btn-group">
<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">管理功能 <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu">
    <li class="dropdown-header">操作項目</li>
    <li><a href="{$this->url->get('people/member/form/edit-')}'+data+'" class="text-primary">編輯</a></li>'+appendFunc+
    '<li><a href="{$this->url->get('people/member/setstate/remove-')}'+data+'" class="text-warning">移除</a></li>
    <li><a href="javascript:void(0);" class="text-danger ajax-delete" data-dialog="刪除後無法恢復記錄，確定要操作？" data-closest="tr" data-remote="{$this->url->get('people/member/delete/')}'+data+'">刪除</a></li>
</ul>
</div>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 1,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 2,
                                            "filter_default_label" => "選擇類型",
                                            "data" => [
                                                "本地",
                                                "Facebook",
                                                "Yahoo",
                                            ]
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 4,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 6,
                                            "filter_type"   => "date",
                                            "datepicker_type"=> "bootstrap-datetimepicker",
                                            "date_format"   => "Y-MM-DD"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('people/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
    }


    /**/
    public function loadRecordsAction()
    {
        $phql = "SELECT PI.*, P.cellphone, P.initTime
                 FROM people_information PI
                 INNER JOIN people P ON PI.PeopleId=P.peopleId
                 ";
        $P = new \Houserich\Models\PeopleInformation();

        $defaultphql = $phql;
        $withoutfilter = true; $condition=array();
        $condition[] = "PI.lastStatecode <> 3";
        if( count($condition) > 0 ){
            $defaultphql .= " WHERE ".implode(" AND ",$condition);
        }
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $P , $P->getReadConnection()->query($defaultphql));

        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                switch ($param['data']) {
                    case 'typeLabel':
                    $p = new \Houserich\Models\People();
                    $type=$p->getType($param['search']['value']);
                    $condition[] = "P.type = '{$type}'";
                    break;
                    case 'fullName':
                    case 'account':
                    $condition[] = "PI.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;
                    case 'cellphone':
                    $condition[] = "P.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;
                    case 'initTime':
                    $time = strtotime($param['search']['value']);
                    $condition[] = "P.{$param['data']} >= {$time}";
                    break;
                }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $P , $P->getReadConnection()->query($phql));

        //針對欄位配置顯示數值
        $s = new \Houserich\Models\PeopleStatus();
        foreach($filters as $row){
            $people = \Houserich\Models\PeopleInformation::findFirst($row->PeopleId);
            $record = array();
            $record['PeopleId'] = $people->PeopleId;
            $record['account'] = $people->account;
            $record['type'] = $people->People->type;
            $record['typeLabel'] = $people->People->getTypeLabel();
            $record['fullName'] = $people->fullName;
            $record['cellphone'] = $people->People->cellphone;
            $record['lastStatecode'] = $people->lastStatecode;
            $record['StatecodeLabel'] = $s->getStatecodeLabel($people->lastStatecode);
            $record['initTime'] = date('Y-m-d H:i:s',$people->People->initTime);
            // $record['func'] = $people->People;

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * @View(pageHeader:"會員建檔",pageDesc:"建檔表單",render:"ruleCA")
     *
     * @Jslibrary("richeditor","formelms","formvalid", "inputuplaod")
     * */
    public function formAction()
    {
        if( $this->params[0] == 'reset' ){
            $this->session->remove('create-PeopleId');
            $this->session->remove('edit-PeopleId');
        }

        $people = null;
        if( $this->session->has('create-PeopleId') ){
            $people = \Houserich\Models\PeopleInformation::findFirst( $this->session->get('create-PeopleId') );
        }elseif( $this->params[0] == 'edit' ){
            $this->session->remove('create-PeopleId');
            $this->session->set('edit-PeopleId', $this->params[1]);
            $people = \Houserich\Models\PeopleInformation::findFirst( $this->params[1] );
        }elseif( $this->session->has('edit-PeopleId')  ){
            $people = \Houserich\Models\PeopleInformation::findFirst( $this->session->get('edit-PeopleId') );
        }
        $this->view->form = new \House\People\Forms\PeopleInformationForm($people);
    }


    /**
     * 會員資料新增與修改
     * */
    public function saveAction()
    {
        $item = new \Houserich\Models\PeopleInformation();
        $form = new \House\People\Forms\PeopleInformationForm();
        $form->bind($_POST, $item);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
        }else{
            try{
                // 新建本地會員帳號！
                if( isset($_POST['type']) ){
                    $people = new \Houserich\Models\people();
                    $people->type = $_POST['type'];
                    $people->cellphone = $_POST['cellphone'];
                    $people->vertifyCellphone = 0;
                    if( !$people->save() ){
                        $msg = array('會員資料建檔發生錯誤，請根據以下訊息修正後再次送出：');
                        echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", array_merge($msg, $people->getMessages() )) ]);
                        return false;
                    }
                }


                if( !$item->save() ){
                    $msg = array('寫入會員資料發生錯誤，請根據以下訊息修正後再次送出：');
                    echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", array_merge($msg, $item->getMessages() )) ]);
                }else{
                    echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"已完成更新會員資料", "id"=>$item->PeopleId]);
                }
            } catch (\PDOException $e) {
                $msg = array('寫入會員資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
            }

        }

        return false;
    }


    /**
     * 處理直接完全刪除會員記錄
     * */
    public function deleteAction()
    {
        if( !$this->request->isAjax() ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"操作發生錯誤無法處理，請重新操作一次。"]);
            return false;
        }

        $people=\Houserich\Models\People::findFirst($this->params['0']);
        if( !$people ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"操作發生錯誤，訊息:".implode(',',$people->getMessages())]);
        }elseif( !$people->delete() ){
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"操作發生錯誤，訊息:".implode(',',$people->getMessages())]);
        }else{
            echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=>"操作發生錯誤無法處理，請重新操作一次。"]);
        }

        return false;
    }


    /**
     * 變更會員狀態
     * */
    public function setstateAction()
    {
        if( count($this->params) != 2 ){
            $this->flashSession->warning("操作發生錯誤無法處理，請重新操作一次。");
            return $this->response->redirect('people/member/phase');
        }

        switch( $this->params[0] )
        {
            case 'deny':
                $newstate= \Houserich\Models\PeopleStatus::STAT_SUSPEND;
                $state=\Houserich\Models\People::findFirst($this->params[1])->getPeopleStatus(["order"=>"setTime DESC"])->getFirst();
                $comments=$state->statecode;

            break;
            case 'allow':
                // 取回前次設定最新狀態！
                $state=\Houserich\Models\People::findFirst($this->params[1])->getPeopleStatus(["order"=>"setTime DESC"])->getFirst();
                $newstate= intval($state->comments);
                $comments='恢復前次權限';
            break;
            case 'remove':
                $newstate= \Houserich\Models\PeopleStatus::STAT_REMOVED;
                $state=\Houserich\Models\People::findFirst($this->params[1])->getPeopleStatus(["order"=>"setTime DESC"])->getFirst();
                $comments=$state->statecode;
            break;
        }

        $status = new \Houserich\Models\PeopleStatus();
        $status->PeopleId = $this->params[1];
        $status->statecode = $newstate;
        $status->comments = $comments;
        if( !$status->save() ){
            $this->flashSession->warning("變更會員狀態發生錯誤，訊息:".implode(',',$status->getMessages()));
        }else{
            $PI = $status->People->PeopleInformation;
            $PI->lastStatecode = $newstate;
            if( !$PI->save() ){
                $this->flashSession->warning("變更會員狀態發生錯誤，訊息:".implode(',',$PI->getMessages()));
            }else{
                $this->flashSession->success("已成功變更會員狀態");
            }
        }
        return $this->response->redirect('people/member/phase');
    }
}
