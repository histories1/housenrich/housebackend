<?php
/**
 * SalerController 賣房申請
 *
 * */

namespace House\People\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @package content
 * */
class SalerController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"賣房申請",pageDesc:"檢視來自前端會員/非會員提出我要賣房表單資訊。",render:"ruleMCA")
     * */
    public function phaseAction()
    {
        $this->view->datatable = array(
                                    'thead' => array(
                                                    '編號'    => '8%',
                                                    '姓名' => '17%',
                                                    '手機' => '17%',
                                                    '物件類型' => '13%',
                                                    '售價'    => '13%',
                                                    '提出時間'    => '20%',
                                                    '操作'    => '12%'
                                               ),
                                    'columns' => json_encode([
                                        [
                                            "data" => "salerId",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "fullName",
                                            "sortable" =>  true ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "cellphone",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "type",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "price",
                                            "sortable" =>  false ,
                                            "searchable" => true
                                        ],
                                        [
                                            "data" => "setTime",
                                            "sortable" =>  true ,
                                            "searchable" => false
                                        ],
                                        [
                                            "data" => "SalerId",
                                            "sortable" =>  false ,
                                            "searchable" => false,
                                            "render" => <<< FUNCTION
#function(data, type, full, meta){
    return '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" aria-expanded="false">完整資訊</button>';
}#
FUNCTION
                                        ]
                                    ]),
                                    'yadcf' => json_encode([
                                        [
                                            "column_number" => 1,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 3,
                                            "filter_default_label" => "選擇類型",
                                            "data" => [
                                                "房屋",
                                                "其他",
                                            ]
                                        ],
                                        [
                                            "column_number" => 4,
                                            "filter_type"   => "text"
                                        ],
                                        [
                                            "column_number" => 5,
                                            "filter_type"   => "date",
                                            "datepicker_type"=> "bootstrap-datetimepicker",
                                            "date_format"   => "Y-MM-DD"
                                        ],
                                    ]),
                                    // start from application-partials
                                    'script' => 'scripts-DataTable',
                                    'ajaxpath' => $this->url->get('saler/'.strtolower( $this->dispatcher->getControllerName() ).'/load-records')
                                   );
    }


    /**/
    public function loadRecordsAction()
    {
        $phql = "SELECT * FROM saler S";
        $S = new \Houserich\Models\Saler();

        $defaultphql = $phql;
        $withoutfilter = true; $condition=array();
        $default = new \Phalcon\Mvc\Model\Resultset\Simple(null, $S , $S->getReadConnection()->query($defaultphql));

        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                switch ($param['data']) {
                    case 'type':
                    $condition[] = "type = '{$type}'";
                    break;
                    case 'fullName':
                    case 'cellphone':
                    $condition[] = "S.{$param['data']} LIKE '%{$param['search']['value']}%'";
                    break;
                    case 'setTime':
                    $time = strtotime($param['search']['value']);
                    $condition[] = "S.{$param['data']} >= {$time}";
                    break;
                }
                $withoutfilter = false;
            }
        }
        if( count($condition) > 0 ){
            $phql .= " WHERE ".implode(" AND ",$condition);
        }

        // order
        foreach($_POST['order'] as $order){
            $l = $_POST['columns'][$order['column']]['data'];
            switch($l)
            {
                default:
                $ordset[] = "{$l} {$order['dir']}";
                break;
            }
        }
        if( count($ordset) > 0 ){
            $phql .= " ORDER BY ".implode(",",$ordset);
        }

        // limit
        if( $_POST['start'] != 0 ){
            $phql .= " LIMIT {$_POST['start']}, {$_POST['length']}";
        }else{
            $phql .= " LIMIT {$_POST['length']}";
        }
        $filters = new \Phalcon\Mvc\Model\Resultset\Simple(null, $S , $S->getReadConnection()->query($phql));

        //針對欄位配置顯示數值
        foreach($filters as $row){
            $saler = \Houserich\Models\Saler::findFirst($row->salerId);
            $record = array();
            $record['PeopleId'] = $saler->PeopleId;
            $record['type'] = $saler->type;
            $record['fullName'] = $saler->fullName;
            $record['cellphone'] = $saler->cellphone;
            $record['setTime'] = date('Y-m-d H:i:s',$saler->setTime);

            $lists[] = $record;
        }

        if( !empty($lists) ){
            if( $withoutfilter ){
                $Filtered = count($default);
            }else{
                $Filtered = count($filters);
            }
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsTotal' => count($default),
                          'recordsFiltered' => $Filtered,
                          'data' => $lists );
        }else{
            $JSON = array('draw' => $_REQUEST['draw'],
                          'recordsFiltered' => 0,
                          'data' => array() );
        }
        echo json_encode($JSON);

        return false;
    }


    /**
     * 使用modal載入完整資訊內容
     * */
    public function viewAction()
    {

        return false;
    }
}
