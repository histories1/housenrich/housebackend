<?php

namespace House\People\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class PeopleInformationForm extends \Personalwork\Forms\Form
{

	/**
	 * @Comment("帳號")
	 */
	private function _Account() {
		$element = new \Personalwork\Forms\Elements\Text("account");
		$element->setLabel("帳號")
				->setAttributes(array(
					"class" => "form-control",
					"required" => "required"
				));
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"帳號欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("姓名")
	 */
	private function _Fullname() {
		$element = new \Personalwork\Forms\Elements\Text("fullName");
		$element->setLabel("姓名")
				->setAttributes(array(
					"class" => "form-control",
					"required" => "required"
				));
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		$element->addValidator(new PresenceOf([
			"message"=>"姓名欄位必須填寫。"
		]));
		return $element;
	}

	/**
	 * @Comment("密碼")
	 */
	private function _Password() {
		$element = new \Phalcon\Forms\Element\Password("password");
		$element->setLabel("密碼")
				->setAttributes(array(
					"class" => "form-control",
					"placeholder" => "填入數值將直接設定會員新密碼。"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		return $element;
	}

	/**
	 * @Comment("性別")
	 */
	private function _Gender() {
		$element = new \Personalwork\Forms\Elements\Select("gender");
		$items = array("男"=>"男","女"=>"女");
		$element->setLabel("性別")
				->setAttributes(array(
					"class" => "form-control",
					"required" => "required",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				))
				->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("年齡")
	 */
	private function _Age() {
		$element = new \Personalwork\Forms\Elements\Numeric("age");
		$element->setLabel("年齡")
				->setAttributes(array(
					"class" => "form-control input-smw",
					"required" => "required"
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("身分證字號")
	 */
	private function _Id() {
		$element = new \Personalwork\Forms\Elements\Text("ID");
		$element->setLabel("身分證字號")
				->setAttributes(array(
					"class" => "form-control input-smw"
				));
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("婚姻")
	 */
	private function _Marriage() {
		$element = new \Personalwork\Forms\Elements\Text("marriage");
		$element->setLabel("婚姻")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("職業")
	 */
	private function _Occupation() {
		$element = new \Personalwork\Forms\Elements\Select("occupation");
		$element->setLabel("職業")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
				));
		$occupation = \Houserich\Models\Fieldoptions::findByFieldname("職業");
		$opt = array();
		foreach ($occupation as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("年收入(單位:萬)")
	 */
	private function _Annualincome() {
		$element = new \Personalwork\Forms\Elements\Numeric("annualIncome");
		$element->setLabel("年收入")
				->setAttributes(array(
					"class" => "form-control",
				))
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "(單位:萬)"
				));
		return $element;
	}

	/**
	 * @Comment("完整地址")
	 */
	private function _Address() {
		$element = new \Personalwork\Forms\Elements\Text("address");
		// $element->setLabel("Address");
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("地址(縣市)")
	 */
	private function _Addresscity() {
		$element = new \Personalwork\Forms\Elements\Select("addressCity");
		$element->setAttributes(array(
					"class" => "form-control depend-city",
					"data-target" => "#addressDistrict",
				));
		$this->address_city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($this->address_city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(行政區)")
	 */
	private function _Addressdistrict() {
		$element = new \Personalwork\Forms\Elements\Select("addressDistrict");
		$element->setAttributes(array(
					"class" => "hide form-control",
				));

		$this->address_district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($this->address_district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(路)")
	 */
	private function _Addressroad() {
		$element = new \Personalwork\Forms\Elements\Text("addressRoad");
		$element->setAttributes(array(
					"class" => "form-control input-lgw",
					"placeholder"=> "路/段/道(含段)名"
				));
		$element->addValidator(new StringLength([
			"max" => 20,
			"message"=>"地址路名欄位長度超過15字元限制。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(巷)")
	 */
	private function _Addresslane() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressLane");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("巷");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址巷欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(弄)")
	 */
	private function _Addressalley() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressAlley");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("弄");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址弄欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號)")
	 */
	private function _Addressno() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNo");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("號");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號之)")
	 */
	private function _Addressnoex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNoEx");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓)")
	 */
	private function _Addressfloor() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloor");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("樓");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓之)")
	 */
	private function _Addressfloorex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloorEx");
		$element->setAttributes(array(
					"class" => "form-control input-xsw",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("LINE帳號")
	 */
	private function _Lineid() {
		$element = new \Personalwork\Forms\Elements\Text("lineID");
		$element->setLabel("LINE帳號");
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("最後狀態")
	 */
	private function _Laststatecode() {
		$element = new \Phalcon\Forms\Element\Hidden("lastStatecode");
		return $element;
	}

	/**
	 * @Comment("最後上線時間")
	 */
	private function _Lastlogintime() {
		$element = new \Phalcon\Forms\Element\Hidden("lastLoginTime");
		return $element;
	}


	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("PeopleId");
		$this->add($id);

		$this->add($this->_Account());
		$this->add($this->_Fullname());
		$this->add($this->_Password());
		$this->add($this->_Gender());
		$this->add($this->_Age());
		$this->add($this->_Id());
		$this->add($this->_Marriage());
		$this->add($this->_Occupation());
		$this->add($this->_Annualincome());
		$this->add($this->_Address());
		$this->add($this->_Addresscity());
		$this->add($this->_Addressdistrict());
		$this->add($this->_Addressroad());
		$this->add($this->_Addresslane());
		$this->add($this->_Addressalley());
		$this->add($this->_Addressno());
		$this->add($this->_Addressnoex());
		$this->add($this->_Addressfloor());
		$this->add($this->_Addressfloorex());
		$this->add($this->_Lineid());
		$this->add($this->_Laststatecode());
		$this->add($this->_Lastlogintime());
	}
}
