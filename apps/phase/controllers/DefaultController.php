<?php
/**
 * DefaultController
 *
 * */

namespace House\Phase\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class DefaultController extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     * @View(pageHeader:"Dashboard",pageDesc:"顯示訊息類、統計類小元件區塊與簡易系統操作說明紀錄。",render:"")
     *
     * @Jslibrary("datatable","jgrid")
     * */
    public function indexAction()
    {
        // Javascripts in the header
        $js = $this->assets->collection("headerScript");
        $js->addJs('/misc/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js');

        $this->view->pick("default/index");
    }


    /**
     * 產生demo用jgrid-table資料
     *
     * */
    public function jsonJgridAction()
    {
        $builder = $this->modelsManager->createBuilder()
                        ->from(array('t'=>'\Houserich\Models\Invheader'));
                        // ->columns(['t.*'])
                            // ->where("s.CustomerId={$this->referSale->CustomerId}")
                            // ->andWhere("s.lastrecord=1")
                            // ->andWhere("s.showdate='".$this->referSale->showdate."'")

        // 處理查詢
        $wh = "";
        $searchOn =  $this->request->getQuery('_search', 'string');
        if($searchOn == 'true') {
            // $searchstr = $this->request->getQuery('filters', 'string');
        }

        // 處理排序
        // get index row - i.e. user click to sort
        $sidx = $this->request->getQuery('sidx','string');
        // get the direction
        $sord = $this->request->getQuery('sord','string');
        $builder->orderBy('t.'.$sidx.' '.$sord);

        // 處理分頁個數
        // get the requested page
        $page = $this->request->getQuery('page', 'int', 1);
        // get how many rows we want to have into the grid
        $limit = $this->request->getQuery('rows', 'int', 0);
        // do not put $limit*($page - 1)
        $start = $limit*$page - $limit;
        $builder->limit($limit, $start);


        $rows = $builder->getQuery()
                        ->execute()
                        ->toArray();

        if( count($rows) >0 ) {
            $total_pages = ceil( count($rows)/$limit );
        } else {
            $total_pages = 0;
        }

        // 組成資料結合
        $response = new \stdClass;
        $response->page = $page;
        $response->total = $total_pages;
        $response->records = count($rows);

        foreach($rows as $i => $row) {
            $response->rows[$i]['id'] = $row["invid"];
            $response->rows[$i]['cell'] = array(null,
                                                $row["invid"],
                                                $row["client_id"],
                                                $row["invdate"],
                                                $row["amount"],
                                                $row["tax"],
                                                $row["total"],
                                                $row["closed"],
                                                $row["ship_via"],
                                                $row["note"]);
        }

        echo json_encode($response);
        return false;
    }


    /**
     * 傳入整個紀錄列表直接根據欄位修改！
     * */
    public function editJgridAction()
    {
        $model = \Houserich\Models\Invheader::findFirst( $this->request->getPost('invid') );
        if( $model ){
            foreach ($_POST as $key => $value) {
                if( $key == 'invdate' ){
                    $model->{$key} = date('Y-m-d', strtotime($value));
                }else{
                    $model->{$key} = $value;
                }
            }
            $model->save();
        }
        echo json_encode($_POST);
        return false;
    }

    /**
     * @View(pageHeader:"連結Google Analytics",pageDesc:"直接到Houserich GA畫面。",render:"")
     * */
    public function togaAction()
    {
        $this->view->pick("default/phase");
    }



    /**
     * @View(pageHeader:"通知中心",pageDesc:"根據預定意訊息類型與內容，設定通知條件與是否啟用通知。",render:"")
     * */
    public function notificationAction()
    {
        $this->view->pick("default/phase");
    }

}
